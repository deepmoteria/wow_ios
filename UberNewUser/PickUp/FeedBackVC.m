//  FeedBackVC.m
//  UberNewUser
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"
#import <Social/Social.h>

@interface FeedBackVC ()

@end

@implementation FeedBackVC
@synthesize ratingBar;

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ratingBar initRateBar];
    [self SetLocalization];
    
    if([strForUserType isEqualToString:@"1"])
    {
        [self.btnPayNow setHidden:YES];
        [self.btnSubmit setUserInteractionEnabled:YES];
    }
    
    NSArray *arrName = [self.strFirstName componentsSeparatedByString:@" "];
    
    self.lblDistCost.text = [NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"distance"]floatValue],[dictBillInfo valueForKey:@"unit"]];
    
    self.lblTotal.text = [NSString stringWithFormat:@"%@ %.2f",[dictBillInfo valueForKey:@"currency"],[[dictBillInfo valueForKey:@"grand_total"] floatValue]];
    self.lblRideWith.text = [NSString stringWithFormat:@"Rate Your Ride with %@",[arrName objectAtIndex:0]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)SetLocalization
{
    /*[self.btnFeedBack setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"JUST_WOWED", nil),@"\U0001F60A"] forState:UIControlStateNormal];
    [self.btnFeedBack setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"JUST_WOWED", nil),@"\U0001F60A"] forState:UIControlStateHighlighted];*/
    
    [self.btnFeedBack setTitle:NSLocalizedString(@"JUST_WOWED", nil) forState:UIControlStateNormal];
    [self.btnFeedBack setTitle:NSLocalizedString(@"JUST_WOWED", nil) forState:UIControlStateHighlighted];
    [self.btnSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnSubmit setUserInteractionEnabled:NO];
}

#pragma makr - Action methods

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        RBRatings rating=[ratingBar getcurrentRatings];
        int rate=rating/2.0;
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        else
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:strForUserId forKey:PARAM_ID];
            [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
            [dictParam setObject:[NSString stringWithFormat:@"%d",rate] forKey:PARAM_RATING];
            NSString *commt=self.txtComments.text;
            
            if([commt isEqualToString:NSLocalizedString(@"COMMENT", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];
            }
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:PREF_REQ_ID];
                         isOnTrip=0;
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (IBAction)onClickPayNow:(id)sender
{
    [self.btnSubmit setUserInteractionEnabled:YES];
    [self.btnPayNow setHidden:YES];
}

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
  
    [textField resignFirstResponder];
    return YES;
}

#pragma mark-
#pragma mark- Text View Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
}

#pragma mark - fb sharing

- (IBAction)facebookBtnPressed:(id)sender
{
    fb = [SLComposeViewController
          composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    //check to see if facebook account exists
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        fb=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *FBString=[NSString stringWithFormat:@"please check out this owesome app."];
        
        [fb setInitialText:FBString];
        
        [self presentViewController:fb animated:YES completion:nil];
    }
    else
    {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            mySLComposerSheet = [[SLComposeViewController alloc] init];
            
            //Tell him with what social plattform to use it, e.g. facebook or twitter
            
            mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            //the message you want to post
            
            [mySLComposerSheet setInitialText:@"please check out this owesome app."];
            
            [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
            
            [alert show];
        }
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             NSString *output;
             switch (result)
             {
                 case SLComposeViewControllerResultCancelled:
                     output = @"Post Cancelled";
                     break;
                 case SLComposeViewControllerResultDone:
                     output = @"Post Successfully";
                     break;
                 default:
                     break;
             }
             
             [APPDELEGATE showToastMessage:output];
         }];
    }
}

#pragma mark - twitter sharing

- (IBAction)twitterBtnPressed:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *msg=[NSString stringWithFormat:@"please check out this owesome app."];
        
        [tweet setInitialText:msg];
        
        [self presentViewController:tweet animated:YES completion:Nil];
    }
    else
    {
        //[APPDELEGATE showToastMessage:@"Please Configure twitter in your phone"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
        
        [alert show];
    }
}

#pragma mark - alertview delegate method

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (&UIApplicationOpenSettingsURLString != NULL)
        {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
            //NSURL *url = [NSURL URLWithString:@"prefs:root=TWITTER"];
            //[[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            // Present some dialog telling the user to open the settings app.
        }
    }
}

@end