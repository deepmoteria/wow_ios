//
//  PickUpVC.m
//  UberNewUser
//
//  Created by Elluminati - macbook on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "PickUpVC.h"
#import "SWRevealViewController.h"
#import "AFNHelper.h"
#import "ProviderDetailsVC.h"
#import "CarTypeCell.h"
#import "UIImageView+Download.h"
#import "CarTypeDataModal.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UberStyleGuide.h"
#import "EastimateFareVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import <GoogleMaps/GoogleMaps.h>

@interface PickUpVC ()

{
    NSString *strForUserId,*strForUserToken,*strForLatitude,*strForLongitude,*strForRequestID,*strForDriverLatitude,*strForDriverLongitude,*strForDestinationLat,*strForDestinationLog,*strForTypeid,*strForCurLatitude,*strForCurLongitude,*strMinFare,*strPassCap,*strETA,*Referral,*dist_price,*time_price,*driver_id,*strForPerTime,*strForPerDist,*strForRideLookup,*strForSelectedType,*strForPatternType,*strForOfferRideLookup,*strForOfferRide;
    
    NSString *str_price_per_unit_distance, *str_base_distance,*strPayment_Option, *strForDriverList,*strForSourceLocation,*strForDestinationLocation,*strForSourceLatitude,*strForSourceLongitude,*strCurrentAddress;
    
    NSString *strForOfferRideSourceLatitude,*strForOfferRideDestinationLatitude,*strForOfferRideSourceLongitude,*strForOfferRideDestinationLongitude,*strForOfferRideSourceLocation,*strForOfferRideDestinationLocation;
    
    NSMutableArray *arrForInformation,*arrForApplicationType,*arrForAddress,*arrDriver;
    NSMutableDictionary *driverInfo;
    BOOL is_paymetCard,is_Fare,is_checkForOrganization,isSecond,flag,isTodayDriving,isOfferRideByBoth;
    NSTimer *timerOrga;
    int timeForOrganzation,timeForLookingUp,time,TextField;
    UIDatePicker *datePicker;
    NSTimer *timerForLookingUp,*timerForSecond;
    GMSMarker *markerOwner,*markerSource;
    
    GMSMapView *mapView_;
}

@end

@implementation PickUpVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideGiverTime];
    arrType = [[NSMutableArray alloc]init];
    [self SetLocalization];
    [self customSetup];
    [self.viewForTodayDriving setHidden:YES];
    [self.viewForOfferRide setHidden:YES];
    [self.viewForSideProfile setHidden:YES];
    strForUserType = [[NSUserDefaults standardUserDefaults]objectForKey:PREF_USER_TYPE];
    NSLog(@"User Type = %@",strForUserType);
    [self getOrganization];
    is_checkForOrganization=0;
    timeForOrganzation=0;
    timeForLookingUp=0;
    isSecond=0;
    isOnTrip=0;
    isOfferRideByBoth=0;
    isTodayDriving=0;
    [self.revealButtonItem setTitle:NSLocalizedString(@"NAV_PICK", nil) forState:UIControlStateNormal];
    [self.viewForPicker setHidden:YES];
    isMenuOpen=NO;
    strForRideLookup=@"";
    arrForAddress=[[NSMutableArray alloc]init];
    arrForApplicationType=[[NSMutableArray alloc]init];
    self.tableForCity.hidden=YES;
    self.viewETA.hidden=YES;
    is_Fare=NO;
    driverInfo=[[NSMutableDictionary alloc] init];
    self.viewForDriver.hidden=YES;
    [self.img_driver_profile applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    self.navigationController.navigationBarHidden=NO;
    
    [self updateLocationManagerr];
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:14];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    [self.viewGoogleMap addSubview:mapView_];
    
    coordinate.latitude = [strForCurLatitude doubleValue];
    coordinate.longitude = [strForCurLongitude doubleValue];
    
    markerOwner = [[GMSMarker alloc] init];
    markerOwner.position = coordinate;
    markerOwner.icon = [UIImage imageNamed:@"pin_driver"];
    markerOwner.map = mapView_;
    
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLongitude;
    
    [self getAddress];
    
    [self.view bringSubviewToFront:self.tableForCity];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:PREF_IS_APPROVED]==YES)
    {
        [self.viewForApproval setHidden:YES];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else
    {
        [self.viewForApproval setHidden:NO];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    if(isOnTrip==1)
    {
        [self.timeForAcceptReject invalidate];
        self.timeForAcceptReject=nil;
        [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:nil];
    }
    else
    {
        if(is_checkForOrganization==0)
        {
            self.viewForOrganization.hidden=NO;
            timerOrga = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerForOrganization) userInfo:nil repeats:YES];
        }
        else
        {
            [self timerForOrganization];
        }
    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.viewForPicker setHidden:YES];
    [self.tableForCity setHidden:YES];
    is_Fare=NO;
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.revealButtonItem addTarget:self action:@selector(onclickMenu:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(IBAction)onclickMenu:(id)sender
{
    isMenuOpen=YES;
    [self.view endEditing:YES];
    [self.tableForCity setHidden:YES];
}

-(void)SetLocalization
{
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
}

#pragma mark - 
#pragma mark - general methods

-(void)getOrganization
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:FILE_ORGANIZATION withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableDictionary *dictOrganization = [[NSMutableDictionary alloc]init];
                     NSMutableArray *arrOrganization = [[NSMutableArray alloc]init];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     arrOrganization=[response valueForKey:@"Organization"];
                     NSUInteger randomIndex = (int) arc4random() % [arrOrganization count];
                     dictOrganization=[arrOrganization objectAtIndex:randomIndex];
                     [self.imgForOrganization downloadFromURL:[dictOrganization valueForKey:@"organization_image"] withPlaceholder:nil];
                     self.lblTimerOrganizationName.text=[dictOrganization valueForKey:@"organization_name"];
                     self.textViewForOrganizartion.text=[dictOrganization valueForKey:@"organization_message"];
                     [self.navigationController.navigationBar setHidden:YES];
                 }
             }
         }];
    }
}

-(void)timerForOrganization
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    dict = [pref objectForKey:PREF_LOGIN_OBJECT];
    if(timeForOrganzation<5)
    {
        self.viewForOrganization.hidden=NO;
        timeForOrganzation++;
        [self hideGiverTime];
    }
    else
    {
        [self.navigationController.navigationBar setHidden:NO];
        self.viewForOrganization.hidden=YES;
        [timerOrga invalidate];
        timerOrga=nil;
        is_checkForOrganization=1;
        if(![dict valueForKey:@"is_pattern_filled"])
        {
            [self goToSetting:SEGUE_MILES];
        }
        [self checkApproval];
        if([strForUserType isEqual:@"1"])
        {
            [self hideTakerTime];
            [self.btnOfferRide setHidden:NO];
            [self.viewForRequest setHidden:YES];
            [self.viewForDriver setHidden:YES];
            [timerForCheckReqStatus invalidate];
            [timerForOfferRide invalidate];
            timerForCheckReqStatus=nil;
            timerForOfferRide=nil;
            [self getRequestId];
        }
        else
        {
            if([strForUserType isEqualToString:@"2"])
            {
                [self checkForOfferRide];
                timerForOfferRide = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForOfferRide) userInfo:nil repeats:YES];
            }
            [self hideTakerTime];
            [self.btnOfferRide setHidden:YES];
            [self.timerForUpdateLocation invalidate];
            self.timerForUpdateLocation=nil;
            [self checkForAppStatus];
        }
    }
}

-(void)showViewForDriver
{
    if(timeForLookingUp<5)
    {
        timeForLookingUp++;
        [APPDELEGATE.window addSubview:self.viewForDriver];
        [APPDELEGATE.window bringSubviewToFront:self.viewForDriver];
    }
    else
    {
        [self.revealButtonItem setTitle:NSLocalizedString(@"RIDE_REQUEST", nil) forState:UIControlStateNormal];
        [self.revealButtonItem setTitle:NSLocalizedString(@"RIDE_REQUEST", nil) forState:UIControlStateSelected];
        [timerForLookingUp invalidate];
        timerForLookingUp=nil;
        [self.viewForDriver removeFromSuperview];
        [self.viewForPicker setHidden:YES];
        [self.viewForRequest setHidden:YES];
        [self showGiverTime];
        
        CLLocationCoordinate2D sourceCordinate;
        sourceCordinate.latitude=[strForLatitude doubleValue];
        sourceCordinate.longitude=[strForLongitude doubleValue];
        
        markerSource = [[GMSMarker alloc] init];
        markerSource.position = sourceCordinate;
        markerSource.icon = [UIImage imageNamed:@"pin_source"];
        markerSource.map = mapView_;
    }
}

-(void)hideGiverTime
{
    self.lblTime.hidden=YES;
    self.lblTime.text = @"";
    self.lblMins.hidden=YES;
    self.imgTime.hidden=YES;
    self.btnCancelRequestRide.hidden=YES;
    self.lblConfirmation.hidden=YES;
    self.imgConfirmation.hidden=YES;
}

-(void)showGiverTime
{
    self.lblTime.hidden=NO;
    self.lblMins.hidden=NO;
    self.imgTime.hidden=NO;
    self.btnCancelRequestRide.hidden=NO;
    self.lblConfirmation.hidden=NO;
    self.imgConfirmation.hidden=NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
}

-(void)checkApproval
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
     {
         [[AppDelegate sharedAppDelegate]hideLoadingView];
         if (response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             if([[response valueForKey:@"success"]boolValue])
             {
                 BOOL approved;
                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                 [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                 approved = [[response valueForKey:@"is_approved"] boolValue];
                 if(approved)
                 {
                     [self.viewForApproval setHidden:YES];
                     [pref setBool:YES forKey:PREF_IS_APPROVED];
                     [self hideTakerTime];
                 }
                 else
                 {
                     [self.viewForApproval setHidden:NO];
                     [pref setBool:NO forKey:PREF_IS_APPROVED];
                 }
                 [pref synchronize];
             }
         }
     }];
}

#pragma mark-
#pragma mark- If-Else Methods

-(void)getRequestId
{
    if (strForRequestID!=nil)
    {
        [self checkTakerStatus];
    }
    else
    {
        [self requsetProgress];
    }
}

-(void)getRequestIdSecond
{
    if (strForRequestID!=nil)
    {
        [self checkTakerStatus];
    }
    else
    {
        flag=YES;
        [self getAllRequests];
    }
}

-(void)requestThird
{
    if(strForRequestID!=nil)
    {
        [self respondToRequest];
    }
    else
    {
        flag=NO;
        [self getAllRequests];
        self.timeForAcceptReject = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getAllRequests) userInfo:nil repeats:YES];
    }
}

#pragma mark -
#pragma mark - Taker Methods

-(void)checkTakerStatus
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForRequestID=[pref objectForKey:PREF_REQ_ID];
        
        [dictparam setValue:strForUserId forKey:PARAM_ID];
        [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictparam setValue:strForRequestID forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:FILE_GET_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Check taker status= %@",response);
             
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSMutableDictionary *dictRequest=[response valueForKey:@"request"];
                     
                     is_completed=[[dictRequest valueForKey:@"is_completed"]intValue];
                     is_dog_rated=[[dictRequest valueForKey:@"is_dog_rated"]intValue];
                     is_started=[[dictRequest valueForKey:@"is_walk_started" ]intValue];
                     is_walker_arrived=[[dictRequest valueForKey:@"is_walker_arrived"]intValue];
                     is_walker_started=[[dictRequest valueForKey:@"is_walker_started"]intValue];
                     
                     dictOwner=[dictRequest valueForKey:@"owner"];;//[arrOwner objectAtIndex:0];
                     
                     /*NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     
                     [pref setObject:[dictOwner valueForKey:@"name"] forKey:PREF_USER_NAME];
                     [pref setObject:[dictOwner valueForKey:@"rating"] forKey:PREF_USER_RATING];
                     [pref setObject:[dictOwner valueForKey:@"phone"] forKey:PREF_USER_PHONE];
                     [pref setObject:[dictOwner valueForKey:@"picture"] forKey:PREF_USER_PICTURE];
                     [pref synchronize];*/
                     
                     [mapView_ clear];
                     
                     [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:nil];
                 }
                 else
                 {
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     strForRequestID = [pref objectForKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self getRequestId];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)requsetProgress
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        
        [dictparam setValue:[pref valueForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setValue:[pref valueForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:FILE_PROGRESS withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Request in Progress= %@",response);
             
             if (response)
             {
                 if([[response valueForKey:@"success"]intValue]==1)
                 {
                     if ([[response valueForKey:@"request_id"] intValue]!=-1)
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                         [pref synchronize];
                         strForRequestID=[response valueForKey:@"request_id"];
                     }
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self getRequestIdSecond];
                 }
                 else
                 {
                     /*UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]] message:@"Please Login Agian" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                      [alert show];*/
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)checkForOfferRide
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        [pref synchronize];
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setValue:strForUserId forKey:PARAM_ID];
        [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_PROVIDER_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Offer Ride response = %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     if([[response valueForKey:@"is_approved"]boolValue])
                     {
                         [self.viewForApproval setHidden:YES];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:PREF_IS_APPROVED];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     else
                     {
                         [self.viewForApproval setHidden:NO];
                         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:PREF_IS_APPROVED];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     NSMutableArray *arrForOfferRide=[response valueForKey:@"incoming_requests"];
                     if(arrForOfferRide.count!=0)
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [self.viewForRequest setHidden:YES];
                         [self.viewForDriver setHidden:YES];
                         [self showTakerTime];
                         [self.btnOfferRide setHidden:YES];
                         self.btnAccept.tag=2;
                         self.btnReject.tag=2;
                         
                         NSMutableDictionary *dictRequestData = [arrForOfferRide valueForKey:@"request_data"];
                         
                         strForRequestID = [NSString stringWithFormat:@"%@",[arrForOfferRide valueForKey:@"request_id"]];
                         
                         strForOfferRide = [NSString stringWithFormat:@"%@",[arrForOfferRide valueForKey:@"is_offer_ride"]];
                         
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setValue:strForRequestID forKey:PREF_REQ_ID];
                         [pref setValue:strForOfferRide forKey:PREF_OFFER_RIDE];
                         [pref synchronize];
                         
                         NSMutableArray *arrOwner=[dictRequestData valueForKey:@"owner"];
                         dictOwner=[arrOwner objectAtIndex:0];
                         
                         self.lblRateValue.text = [NSString stringWithFormat:@"%.1f",[[dictOwner valueForKey:@"rating"] floatValue]];
                         
                         self.lblDetailName.text = [ NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"first_name"],[dictOwner valueForKey:@"last_name"]];
                         
                         [self.imgForOwnerProfile downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
                         
                         self.lblDetailNumber.text = [dictOwner valueForKey:@"car_number"];
                         self.lblDetailType.text = [dictOwner valueForKey:@"car_model"];
                         
                         NSString *str = [dictOwner valueForKey:@"login_by"];
                         
                         if([str isEqualToString:@"facebook"])
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"facebook"]];
                         }
                         else if ([str isEqualToString:@"google"])
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"google"]];
                         }
                         else
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"linkedIn"]];
                         }
                         
                         [self.imgDetailPicture downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
                         
                         self.lblDetailSource.text = [dictOwner valueForKey:@"s_location"];
                         self.lblDetailDestination.text = [dictOwner valueForKey:@"d_location"];
                         self.lblDetailStartTime.text = [dictOwner valueForKey:@"start_time"];
                         self.lblOrganizationName.text = [dictOwner valueForKey:@"organization"];
                         
                         if([[dictOwner valueForKey:@"selected_type"]isEqualToString:@"1"])
                         {
                             self.imgDetailType.image = [UIImage imageNamed:@"car_rate_card"];
                             self.lblDetailNumber.text = [[dictOwner valueForKey:@"car_number"] uppercaseString];
                             self.lblDetailType.text = @"Car Registration No";
                         }
                         else
                         {
                             self.imgDetailType.image = [UIImage imageNamed:@"bike_rate_card"];
                             self.lblDetailNumber.text = [[dictOwner valueForKey:@"bike_number"] uppercaseString];
                             self.lblDetailType.text = @"Bike Registration No";
                         }
                         
                         time = [[arrForOfferRide valueForKey:@"time_left_to_respond"]intValue];
                         NSInteger intForProviderTime = time/60;
                         if(time==0)
                         {
                             [self.timeForAcceptReject invalidate];
                             self.timeForAcceptReject=nil;
                             self.lblMins.text = @"Mins";
                             NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                             [pref removeObjectForKey:PREF_REQ_ID];
                             [pref synchronize];
                             [self hideTakerTime];
                         }
                         else if (time<61)
                         {
                             if(isSecond==0)
                             {
                                 timerForSecond = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setSeconds) userInfo:nil repeats:YES];
                             }
                         }
                         else
                             self.lblTime.text = [NSString stringWithFormat:@"%ld",(long)intForProviderTime+1];
                     }
                 }
                 else
                 {
                     /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                     [alert show];*/
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)getAllRequests
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        [pref synchronize];
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setValue:strForUserId forKey:PARAM_ID];
        [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:FILE_PROVIDER_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Get All Request= %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     if([[response valueForKey:@"is_approved"]boolValue])
                     {
                         [self.viewForApproval setHidden:YES];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:PREF_IS_APPROVED];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     else
                     {
                         [self.viewForApproval setHidden:NO];
                         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:PREF_IS_APPROVED];
                         [[NSUserDefaults standardUserDefaults]synchronize];
                     }
                     
                     NSMutableArray *arrRespone=[response valueForKey:@"incoming_requests"];
                     if(arrRespone.count!=0)
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus = nil;
                         [self.viewForRequest setHidden:YES];
                         [self.viewForDriver setHidden:YES];
                         [self showTakerTime];
                         [self.btnOfferRide setHidden:YES];
                         self.btnAccept.tag=1;
                         self.btnReject.tag=1;
                         
                         NSMutableDictionary *dictRequestData=[arrRespone valueForKey:@"request_data"];
                         NSMutableArray *arrOwner=[dictRequestData valueForKey:@"owner"];
                         dictOwner=[arrOwner objectAtIndex:0];
                         
                         self.lblRateValue.text=[NSString stringWithFormat:@"%.1f",[[dictOwner valueForKey:@"rating"] floatValue]];
                         
                         self.lblDetailName.text=[NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"first_name"],[dictOwner valueForKey:@"last_name"]];
                         
                         [self.imgForOwnerProfile downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
                         
                         self.lblDetailNumber.text=[dictOwner valueForKey:@"car_number"];
                         self.lblDetailType.text=[dictOwner valueForKey:@"car_model"];
                         
                         NSString *str = [dictOwner valueForKey:@"login_by"];
                         
                         if([str isEqualToString:@"facebook"])
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"facebook"]];
                         }
                         else if ([str isEqualToString:@"google"])
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"google"]];
                         }
                         else
                         {
                             [self.imgLoginWith setImage:[UIImage imageNamed:@"linkedIn"]];
                         }
                         
                         [self.imgDetailPicture downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
                         
                         self.lblDetailSource.text = [dictOwner valueForKey:@"s_location"];
                         self.lblDetailDestination.text = [dictOwner valueForKey:@"d_location"];
                         self.lblDetailStartTime.text = [dictOwner valueForKey:@"start_time"];
                         self.lblOrganizationName.text = [dictOwner valueForKey:@"organization"];
                         
                         if([[dictOwner valueForKey:@"selected_type"]isEqualToString:@"1"])
                         {
                             self.imgDetailType.image = [UIImage imageNamed:@"car_rate_card"];
                             self.lblDetailNumber.text = [[dictOwner valueForKey:@"car_number"] uppercaseString];
                             self.lblDetailType.text = @"Car Registration No";
                         }
                         else
                         {
                             self.imgDetailType.image = [UIImage imageNamed:@"bike_rate_card"];
                             self.lblDetailNumber.text = [[dictOwner valueForKey:@"bike_number"] uppercaseString];
                             self.lblDetailType.text = @"Bike Registration No";
                         }

                         strForRequestID = [NSString stringWithFormat:@"%@",[arrRespone valueForKey:@"request_id"]];
                         
                         NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                         [pref synchronize];
                         
                         time=[[arrRespone valueForKey:@"time_left_to_respond"]intValue];
                         NSInteger intForProviderTime = time/60;
                         if(time==0)
                         {
                             [self.timeForAcceptReject invalidate];
                             self.timeForAcceptReject = nil;
                             self.lblMins.text = @"Mins";
                             NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
                             [pref removeObjectForKey:PREF_REQ_ID];
                             [pref synchronize];
                             [self hideTakerTime];
                             [self.btnOfferRide setHidden:NO];
                         }
                         else if (time<61)
                         {
                             if(isSecond==0)
                             {
                                 timerForSecond = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setSeconds) userInfo:nil repeats:YES];
                             }
                         }
                         else
                             self.lblTime.text = [NSString stringWithFormat:@"%ld",(long)intForProviderTime+1];
                     }
                     else if (flag==YES)
                     {
                         [self requestThird];
                     }
                 }
                 else
                 {
                     /**UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                     [alert show];*/
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)respondToRequest
{
    if(isTodayDriving==1)
    {
        if(strForSelectedType.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_PATTERN", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForRequestID=[pref objectForKey:PREF_REQ_ID];
            
            if (strForRequestID!=nil)
            {
                NSMutableDictionary *dictparam = [[NSMutableDictionary alloc]init];
                
                [dictparam setValue:strForUserId forKey:PARAM_ID];
                [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictparam setValue:strForRequestID forKey:PARAM_REQUEST_ID];
                [dictparam setValue:@"1" forKey:PARAM_ACCEPTED];
                if(isTodayDriving==1)
                    [dictparam setValue:strForSelectedType forKey:PARAM_SELECTED_TYPE];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getProviderDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
                 {
                     NSLog(@"Respond to Request= %@",response);
                     [APPDELEGATE hideLoadingView];
                     if (response)
                     {
                         if([[response valueForKey:@"success"] intValue]==1)
                         {
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_ACCEPTED", nil)];
                             NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                             
                             [pref setValue:[dictOwner valueForKey:@"name"] forKey:PREF_USER_NAME];
                             [pref setValue:[dictOwner valueForKey:@"rating"] forKey:PREF_USER_RATING];
                             [pref setValue:[dictOwner valueForKey:@"phone"] forKey:PREF_USER_PHONE];
                             [pref setValue:[dictOwner valueForKey:@"picture"] forKey:PREF_USER_PICTURE];
                             [pref synchronize];
                             
                             [self.timeForAcceptReject invalidate];
                             self.timeForAcceptReject=nil;
                             
                             [mapView_ clear];
                             [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:nil];
                         }
                     }
                 }];
            }
            else
            {
                
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

-(void)respondToOfferRideRequest
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForRequestID=[pref objectForKey:PREF_REQ_ID];
        
        if (strForRequestID!=nil)
        {
            NSMutableDictionary *dictparam = [[NSMutableDictionary alloc]init];
            
            [dictparam setValue:strForUserId forKey:PARAM_ID];
            [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictparam setValue:strForRequestID forKey:PARAM_REQUEST_ID];
            [dictparam setValue:@"1" forKey:PARAM_ACCEPTED];
                        
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"Respond to Request= %@",response);
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         
                         [pref setValue:[dictOwner valueForKey:@"name"] forKey:PREF_USER_NAME];
                         [pref setValue:[dictOwner valueForKey:@"rating"] forKey:PREF_USER_RATING];
                         [pref setValue:[dictOwner valueForKey:@"phone"] forKey:PREF_USER_PHONE];
                         [pref setValue:[dictOwner valueForKey:@"picture"] forKey:PREF_USER_PICTURE];
                         [pref synchronize];
                         
                         [timerForOfferRide invalidate];
                         timerForOfferRide = nil;
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus = nil;
                         
                         [mapView_ clear];
                         [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:nil];
                     }
                 }
             }];
        }
        else
        {
            
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)hideTakerTime
{
    self.lblTime.hidden=YES;
    self.lblTime.text = @"";
    self.lblMins.hidden=YES;
    self.imgTime.hidden=YES;
    [self.btnAccept setHidden:YES];
    [self.btnReject setHidden:YES];
    [self.btnSideProfile setHidden:YES];
}

-(void)showTakerTime
{
    self.lblTime.hidden=NO;
    self.lblMins.hidden=NO;
    self.imgTime.hidden=NO;
    [self.btnAccept setHidden:NO];
    [self.btnReject setHidden:NO];
    [self.btnSideProfile setHidden:NO];
}

#pragma mark -
#pragma mark - Location Delegate

-(CLLocationCoordinate2D) getLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

-(void)updateLocationManagerr
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
    {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    strForLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
    strForLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    /*if (arrDriver.count>0) {
        [self getETA:[arrDriver objectAtIndex:0]];
    }
    
        //[self getAddress];
        [self getProviders];*/
}

-(void)getAddress
{
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"end_address"];
    
    if (getAddress.count!=0)
    {
        NSArray *arrComma=[[[getAddress objectAtIndex:0] objectAtIndex:0] componentsSeparatedByString:@","];
        
        for (int j=0; j<arrComma.count-2; j++)
        {
            NSString *strComma = [NSString stringWithFormat:@"%@",[arrComma objectAtIndex:j]];
            if(j==0)
                strCurrentAddress = strComma;
            else
                strCurrentAddress = [strCurrentAddress stringByAppendingString:strComma];
        }
    }
}

#pragma mark -
#pragma mark - Mapview Delegate

-(void)showMapCurrentLocatinn
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:14];
    [mapView_ animateWithCameraUpdate:updatedCamera];
}

/*-(void)updateLocation
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLatitude doubleValue]==0.00)&&([strForLongitude doubleValue]==0)))
            {
                
            }
            else
            {
                NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                strForUserId=[pref objectForKey:PREF_USER_ID];
                strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                [pref synchronize];
                
                NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
                [dictparam setValue:strForUserId forKey:PARAM_ID];
                [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictparam setValue:strForLatitude forKey:PARAM_LONGITUDE];
                [dictparam setValue:strForLongitude forKey:PARAM_LATITUDE];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getProviderDataFromPath:FILE_PROVIDER_LOCATION withParamData:dictparam withBlock:^(id response, NSError *error)
                 {
                     NSLog(@"Update Location = %@",response);
                     if (response)
                     {
                         if([[response valueForKey:@"success"] intValue]==1)
                         {
                            
                         }
                     }
                     
                 }];
                
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow Driver -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
}*/

#pragma mark- Searching Method

- (IBAction)Searching:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    NSString *str;
    if (TextField==1)
    {
        str=self.txtAddress.text;
        CGRect frameRelativeTomainview = [self.txtAddress convertRect:self.txtAddress.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(TextField==2)
    {
        str=self.txtDestAddress.text;
        CGRect frameRelativeTomainview = [self.txtDestAddress convertRect:self.txtDestAddress.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(TextField==3)
    {
        str=self.txtOfferRideSource.text;
        CGRect frameRelativeTomainview = [self.txtOfferRideSource convertRect:self.txtOfferRideSource.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(TextField==4)
    {
        str=self.txtOfferRideDestination.text;
        CGRect frameRelativeTomainview = [self.txtOfferRideDestination convertRect:self.txtOfferRideDestination.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height+5,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             if(sender == self.txtAddress)
             {
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isSource"];
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                 
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                 
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }
             else if(sender == self.txtOfferRideSource)
             {
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isOfferRideSource"];
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                     
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }

         }
     }];
}

- (IBAction)SearchingDestination:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    self.tableForCity.frame = CGRectMake(self.txtDestAddress.frame.origin.x, self.txtDestAddress.frame.size.height+self.txtDestAddress.frame.origin.y+80, self.tableForCity.frame.size.width, self.tableForCity.frame.size.height);
    
    NSString *str=self.txtDestAddress.text;
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             if(sender == self.txtDestAddress)
             {
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDestination"];
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                 
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }
             else if(sender == self.txtOfferRideDestination)
             {
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isOfferRideDestination"];
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                     
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }

         }
     }];
}

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
    
    cell.textLabel.numberOfLines=0;
    cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
    cell.textLabel.text=formatedAddress;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    [self setNewPlaceData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)setNewPlaceData
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isSource"]==YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isSource"];
        self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtAddress];
        [self getLocationFromString:self.txtAddress.text];
    }
    else if([[NSUserDefaults standardUserDefaults] boolForKey:@"isDestination"]==YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isDestination"];
        self.txtDestAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtDestAddress];
        [self getDestinationLocationFromString:self.txtDestAddress.text];
    }
    else if([[NSUserDefaults standardUserDefaults] boolForKey:@"isOfferRideSource"]==YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isOfferRideSource"];
        self.txtDestAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtOfferRideSource];
        [self getLocationFromString:self.txtOfferRideSource.text];
    }
    else if([[NSUserDefaults standardUserDefaults] boolForKey:@"isOfferRideDestination"]==YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isOfferRideDestination"];
        self.txtDestAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtOfferRideDestination];
        [self getDestinationLocationFromString:self.txtOfferRideDestination.text];
    }
}


#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_TO_ACCEPT])
    {
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strCurrentClientAddress = strCurrentAddress;
        obj.strForSourceLatitude=strForSourceLatitude;
        obj.strForSourceLatitude=strForSourceLongitude;
        obj.strForDestinationLatitude=strForDestinationLat;
        obj.strForDestinationLatitude=strForDestinationLog;
    }
    else if ([segue.identifier isEqualToString:@"segueToEastimate"])
    {
        EastimateFareVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strMinFare=strMinFare;
        obj.str_base_distance = str_base_distance;
        obj.str_price_per_unit_distance = str_price_per_unit_distance;
    }
}

-(void)goToSetting:(NSString *)str
{
    [self performSegueWithIdentifier:str sender:self];
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)cancelReqBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         self.txtAddress.text = @"";
                         self.txtDestAddress.text = @"";
                         self.lblPickedDate.text = @"Start Time";
                         [self.btnCar setSelected:NO];
                         [self.btnBike setSelected:NO];
                         [self.btnBoth setSelected:NO];
                         [self.viewForDriver setHidden:YES];
                         [self.viewForRequest setHidden:NO];
                         [self hideGiverTime];
                         [timerForCheckReqStatus invalidate];
                         [timerForLookingUp invalidate];
                         timerForCheckReqStatus=nil;
                         timerForLookingUp=nil;
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref removeObjectForKey:PREF_REQ_ID];
                         [pref synchronize];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                     }
                     else
                     {}
                 }
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Wow Client -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
         alertLocation.tag=100;
         [alertLocation show];
    }
}

- (IBAction)myLocationPressed:(id)sender
{
    if ([CLLocationManager locationServicesEnabled])
    {
        CLLocationCoordinate2D coor;
        coor.latitude=[strForCurLatitude doubleValue];
        coor.longitude=[strForCurLongitude doubleValue];
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
        [mapView_ animateWithCameraUpdate:updatedCamera];
        
        CLLocationCoordinate2D coordinate = [self getLocation];
        strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];

        coordinate.latitude = [strForCurLatitude doubleValue];
        coordinate.longitude = [strForCurLongitude doubleValue];
        
        markerOwner = [[GMSMarker alloc] init];
        markerOwner.position = coordinate;
        markerOwner.icon = [UIImage imageNamed:@"pin_driver"];
        markerOwner.map = mapView_;
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Wow Client -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
}

-(void)setTimerToCheckDriverStatus
{
    if (timerForCheckReqStatus)
    {
        [timerForCheckReqStatus invalidate];
        timerForCheckReqStatus = nil;
    }
     timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
}

-(void)checkForAppStatus
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    if(strReqId!=nil)
    {
        if([strReqId integerValue]==-1)
        {
            [self RequestInProgress];
        }
        else
        {
            [self checkForRequestStatus];
            timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
            [self.viewForRequest setHidden:YES];
            [self.viewForDriver setHidden:YES];
        }
    }
    else
    {
        [self.viewForRequest setHidden:NO];
        [self RequestInProgress];
    }
}

-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];

        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     if(strCheck)
                     {
                         self.viewForDriver.hidden=YES;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         
                         strForDriverLatitude = [dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude = [dictWalker valueForKey:@"longitude"];
                         
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             return;
                         }
                         ProviderDetailsVC *vcFeed = nil;
                         for (int j=0; j<self.navigationController.viewControllers.count; j++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:j];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             [self.timeForAcceptReject invalidate];
                             self.timeForAcceptReject=nil;
                             [timerForLookingUp invalidate];
                             timerForCheckReqStatus=nil;
                             timerForLookingUp=nil;
                             [self hideGiverTime];
                             [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                             [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                         }
                         else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                     
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1 && [[response valueForKey:@"success"]boolValue])
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     [APPDELEGATE hideLoadingView];
                     self.viewForDriver.hidden=YES;
                 }
                 else
                 {
                     driverInfo=[response valueForKey:@"walker"];
                     [self showGiverTime];
                     if([[NSUserDefaults standardUserDefaults]valueForKey:PREF_REQ_ID])
                     {
                         NSString *strTime = [NSString stringWithFormat:@"%@",[response valueForKey:@"time_left_to_respond"]];
                         NSInteger intForTime = [strTime intValue]/60;
                         if([strTime intValue]==0)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             self.lblMins.text = @"Mins";
                             if([strForUserType isEqual:@"1"])
                             {
                                 [self.viewForRequest setHidden:YES];
                                 [self.btnOfferRide setHidden:NO];
                             }
                             else
                             {
                                 [self.viewForDriver setHidden:YES];
                                 [self.viewForRequest setHidden:NO];
                             }
                             [self hideGiverTime];
                         }
                         else if ([strTime intValue]<61)
                         {
                             time=[strTime intValue];
                             if(isSecond==0)
                             {
                                 timerForSecond = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setSeconds) userInfo:nil repeats:YES];
                             }
                         }
                         else
                             self.lblTime.text = [NSString stringWithFormat:@"%ld",(long)intForTime+1];
                     }
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)RequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     BOOL isApproved;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     isApproved = [[response valueForKey:@"is_approved"] boolValue];
                     if(isApproved)
                     {
                         [self.viewForApproval setHidden:YES];
                         [pref setBool:YES forKey:PREF_IS_APPROVED];
                         [self hideTakerTime];
                         [self.btnOfferRide setHidden:YES];
                         [self.viewForRequest setHidden:NO];
                     }
                     else
                     {
                         [self.viewForApproval setHidden:NO];
                         [pref setBool:NO forKey:PREF_IS_APPROVED];
                     }
                     [pref synchronize];
                     
                     if([strForUserType isEqual:@"1"])
                     {
                         [self.btnOfferRide setHidden:NO];
                         [self.viewForRequest setHidden:YES];
                         [self.viewForDriver setHidden:YES];
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                     }
                     else
                     {
                         [self hideTakerTime];
                         [self.btnOfferRide setHidden:YES];
                     }
                 }
                 else
                 {
                     if([[response valueForKey:@"error_code"]intValue]==406)
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                         [alert show];
                     }
                     else
                     {
                         [self.viewForApproval setHidden:NO];
                     }
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getPagesData
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];

    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGE,PARAM_ID,strForUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];

             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 arrPage=[response valueForKey:@"informations"];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     
                 }
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getETA:(NSDictionary *)dict
{
    CLLocationCoordinate2D scorr=CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    CLLocationCoordinate2D dcorr=CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue], [[dict valueForKey:@"longitude"]doubleValue]);
    [self calculateRoutesFrom:scorr to:dcorr];
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] || [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
        
    }
    else
    {
        NSDictionary *getRoutes = [json valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        //NSArray *getAddress = [getLegs valueForKey:@"duration"];
        NSArray *getDistance = [getLegs valueForKey:@"distance"];
        NSString *strDistance = [[[getDistance objectAtIndex:0]objectAtIndex:0] valueForKey:@"text"];
        NSArray *timeRoute=[strDistance  componentsSeparatedByString:@" "];
        NSString *strDistanceValue = [timeRoute objectAtIndex:0];
        NSString *strDistanceUnit = [timeRoute objectAtIndex:1];
        
        if([strDistanceUnit isEqualToString:@"mi"])
        {
            strForPerDist=[NSString stringWithFormat:@"%.2f",[strDistanceValue floatValue]*1.6];
        }
        else if ([strDistanceUnit isEqualToString:@"m"])
        {
            strForPerDist=[NSString stringWithFormat:@"%.2f",[strDistanceValue floatValue]/1000];
        }
        else if ([strDistanceUnit isEqualToString:@"km"])
        {
            strForPerDist=[NSString stringWithFormat:@"%.2f",[strDistanceValue floatValue]];
        }
        
        [[AppDelegate sharedAppDelegate]hideLoadingView];
        NSString *strForCar = [NSString stringWithFormat:@"%@",[[arrType valueForKey:@"base_price"] objectAtIndex:0]];
        NSString *strForBike = [NSString stringWithFormat:@"%@",[[arrType valueForKey:@"base_price"] objectAtIndex:1]];
        
        self.lblEstiDistance.text=[NSString stringWithFormat:@"%@ %@",strDistanceUnit,strForPerDist];
        self.lblEstiAmountOfCar.text = [NSString stringWithFormat:@"%@ %d",[[arrType valueForKey:@"currency"] objectAtIndex:0],([strForPerDist intValue]*[strForCar intValue])];
        self.lblEstiAmountOfBike.text = [NSString stringWithFormat:@"%@ %d",[[arrType valueForKey:@"currency"] objectAtIndex:1],([strForPerDist intValue]*[strForBike intValue])];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    return nil;
}

-(void)setSeconds
{
    self.lblTime.text=[NSString stringWithFormat:@"%d",time];
    isSecond=1;
    self.lblMins.text=@"Sec";
    time--;
    if(time<=0)
    {
        self.lblMins.text=@"Min";
        self.txtAddress.text = @"";
        self.txtDestAddress.text = @"";
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        [pref removeObjectForKey:PREF_REQ_ID];
        [pref synchronize];
        if([strForUserType isEqual:@"1"])
        {
            [self.timeForAcceptReject invalidate];
            self.timeForAcceptReject=nil;
            [self.viewForRequest setHidden:YES];
            [self hideTakerTime];
            [self.btnOfferRide setHidden:NO];
            self.timeForAcceptReject = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getAllRequests) userInfo:nil repeats:YES];
        }
        else
        {
            [timerForCheckReqStatus invalidate];
            [timerForSecond invalidate];
            timerForCheckReqStatus=nil;
            timerForSecond=nil;
            [self.viewForDriver setHidden:YES];
            [self.viewForRequest setHidden:NO];
            [self hideGiverTime];
        }
    }
}

#pragma mark
#pragma mark - UITextfield Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(self.txtAddress==textField)
    {
        if(arrForAddress.count==1)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x,86+134, self.tableView.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+78, self.tableView.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+34, self.tableView.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableView.hidden=YES;
        
        [self.tableView reloadData];
    }
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(isMenuOpen==NO)
    {
        if(textField==self.txtAddress)
        {
            self.txtAddress.text=@"";
        }
        if(textField==self.txtDestAddress)
        {
            self.txtDestAddress.text=@"";
        }
        if(textField==self.txtOfferRideSource)
        {
            self.txtOfferRideSource.text = @"";
        }
        if(textField==self.txtOfferRideDestination)
        {
            self.txtOfferRideDestination.text = @"";
        }
        return YES;
    }
    else
        return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        TextField=1;
        CGRect frameRelativeTomainview = [self.txtAddress convertRect:self.txtAddress.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(textField==self.txtDestAddress)
    {
        TextField=2;
        CGRect frameRelativeTomainview = [self.txtDestAddress convertRect:self.txtDestAddress.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(textField==self.txtOfferRideSource)
    {
        TextField=3;
        CGRect frameRelativeTomainview = [self.txtOfferRideSource convertRect:self.txtOfferRideSource.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(self.tableForCity.frame.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(textField==self.txtOfferRideDestination)
    {
        TextField=4;
        CGRect frameRelativeTomainview = [self.txtOfferRideDestination convertRect:self.txtOfferRideDestination.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
      [self getLocationFromString:self.txtAddress.text];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.tableView.hidden=YES;
    self.tableForCity.hidden=YES;
    [textField resignFirstResponder];
    return YES;
}

-(void)getLocationFromString:(NSString *)str
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             
             if([[NSUserDefaults standardUserDefaults] boolForKey:@"isSource"]==NO)
             {
                 if ([arrAddress count] > 0)
                 {
                     self.lblEstiSource.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     
                     self.lblSourceAddress.text = self.lblEstiSource.text;
                     strForSourceLocation = self.lblSourceAddress.text;
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForSourceLatitude=[dictLocation valueForKey:@"lat"];
                     strForSourceLongitude=[dictLocation valueForKey:@"lng"];
                 }
             }
             else if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isOfferRideSource"]==NO)
             {
                 if ([arrAddress count] > 0)
                 {
                     self.lblSourceAddress.text = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     strForOfferRideSourceLocation = self.lblSourceAddress.text;
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForOfferRideSourceLatitude=[dictLocation valueForKey:@"lat"];
                     strForOfferRideSourceLongitude=[dictLocation valueForKey:@"lng"];
                 }
             }
         }
     }];
}

-(void)getDestinationLocationFromString:(NSString *)str
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             
             if([[NSUserDefaults standardUserDefaults]boolForKey:@"isDestination"]==NO)
             {
                 if ([arrAddress count] > 0)
                 {
                     self.lblEstiDesti.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     
                     self.lblDestinationAddress.text = self.lblEstiDesti.text;
                     strForDestinationLocation = self.lblDestinationAddress.text;
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForDestinationLat=[dictLocation valueForKey:@"lat"];
                     strForDestinationLog=[dictLocation valueForKey:@"lng"];
                 }
             }
             else if([[NSUserDefaults standardUserDefaults]boolForKey:@"isOfferRideDestination"]==NO)
             {
                 if ([arrAddress count] > 0)
                 {
                     self.lblDestinationAddress.text = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     strForOfferRideDestinationLocation = self.lblDestinationAddress.text;
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForOfferRideDestinationLatitude=[dictLocation valueForKey:@"lat"];
                     strForOfferRideDestinationLongitude=[dictLocation valueForKey:@"lng"];
                }
             }
         }
     }];
}

#pragma mark -
#pragma mark - Ride Taker Action Methods

- (IBAction)acceptBtnPressed:(id)sender
{
    if(self.btnAccept.tag==1)
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
        
        if([[dictInfo valueForKey:@"type"]isEqualToString:@"1,2"] || [[dictInfo valueForKey:@"type"]isEqualToString:@"2,1"])
        {
            isTodayDriving=1;
            [self.viewForTodayDriving setHidden:NO];
        }
        else
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_ADMIN_APPROVE", nil)];
            [self respondToRequest];
        }
    }
    else if(self.btnAccept.tag==2)
    {
        [self respondToOfferRideRequest];
    }
}

- (IBAction)rejectBtnPressed:(id)sender
{
    if(self.btnReject.tag==1)
    {
        if ([[AppDelegate sharedAppDelegate]connected])
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForRequestID=[pref objectForKey:PREF_REQ_ID];
            
            if (strForRequestID!=nil)
            {
                NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
                
                [dictparam setValue:strForRequestID forKey:PARAM_REQUEST_ID];
                [dictparam setValue:strForUserId forKey:PARAM_ID];
                [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictparam setValue:@"0" forKey:PARAM_ACCEPTED];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getProviderDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
                 {
                     NSLog(@"Reject ride = %@",response);
                     [APPDELEGATE hideLoadingView];
                     if (response)
                     {
                         response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                         if([[response valueForKey:@"success"] boolValue])
                         {
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_REJECTED", nil)];
                         }
                     }
                 }];
            }
            else
            {
                
            }
        }
    }
    else if (self.btnReject.tag==2)
    {
        if ([[AppDelegate sharedAppDelegate]connected])
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForRequestID=[pref objectForKey:PREF_REQ_ID];
            
            if (strForRequestID!=nil)
            {
                NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
                
                [dictparam setValue:strForRequestID forKey:PARAM_REQUEST_ID];
                [dictparam setValue:strForUserId forKey:PARAM_ID];
                [dictparam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictparam setValue:@"0" forKey:PARAM_ACCEPTED];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
                 {
                     NSLog(@"Reject ride = %@",response);
                     [APPDELEGATE hideLoadingView];
                     if (response)
                     {
                         response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                         if([[response valueForKey:@"success"] boolValue])
                         {
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_REJECTED", nil)];
                         }
                     }
                 }];
            }
            else
            {
                
            }
        }
    }
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref removeObjectForKey:PREF_REQ_ID];
    strForRequestID=[pref valueForKey:PREF_REQ_ID];
    
    [mapView_ clear];
    
    CLLocationCoordinate2D current;
    current.latitude=[strForLatitude doubleValue];
    current.longitude=[strForLongitude doubleValue];
    
    GMSMarker *markerTaker;
    
    markerTaker = [[GMSMarker alloc] init];
    markerTaker.position = current;
    markerTaker.icon = [UIImage imageNamed:@"pin_driver"];
    markerTaker.map = mapView_;
    
    if([strForUserType isEqual:@"1"])
    {
        [self hideTakerTime];
        [self.btnOfferRide setHidden:NO];
        [self.viewForRequest setHidden:YES];
        [self.viewForDriver setHidden:YES];
        self.timeForAcceptReject = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getAllRequests) userInfo:nil repeats:YES];
    }
    else
    {
        if([strForUserType isEqualToString:@"2"])
        {
            timerForOfferRide = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForOfferRide) userInfo:nil repeats:YES];
        }
        [self hideTakerTime];
        [self.btnOfferRide setHidden:YES];
        [self checkForAppStatus];
    }
}

- (IBAction)okApprovalBtn:(id)sender
{
    exit(0);
}

#pragma mark -
#pragma mark - Ride Giver Action Methods

- (IBAction)onClickDropForTime:(id)sender
{
    self.viewForPicker.hidden=NO;
    [self.datePickerNew setHidden:NO];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [self.datePickerNew setLocale:locale];
    [self SetRequestTime];
}

-(void)SetRequestTime
{
    [self.datePickerNew setDatePickerMode:UIDatePickerModeTime];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [self.datePickerNew setLocale:locale];
}

- (IBAction)onClickRideEstimate:(id)sender
{
    if([self.txtAddress.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_SOURCE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([self.txtDestAddress.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_DESTINATION", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait while calculatting ETA"];
        self.lblEstiSource.text=self.txtAddress.text;
        self.lblEstiDesti.text=self.txtDestAddress.text;
        self.lblSourceAddress.text=self.txtAddress.text;
        self.lblDestinationAddress.text=self.txtDestAddress.text;
        self.viewETA.hidden=NO;
        CLLocationCoordinate2D scorr=CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
        CLLocationCoordinate2D dcorr=CLLocationCoordinate2DMake([strForDestinationLat doubleValue], [strForDestinationLog doubleValue]);
        [self calculateRoutesFrom:scorr to:dcorr];
    }
}

- (IBAction)onClickRequestButton:(id)sender
{
    [self.timeForAcceptReject invalidate];
    self.timeForAcceptReject=nil;
    
    if([self.txtAddress.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_SOURCE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([self.txtDestAddress.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_DESTINATION", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([strForRideLookup isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_RIDE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if(([[AppDelegate sharedAppDelegate]connected]))
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATE_REQUEST", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strForSourceLatitude forKey:PARAM_LATITUDE];
            [dictParam setValue:strForSourceLongitude  forKey:PARAM_LONGITUDE];
            [dictParam setValue:strForDestinationLat  forKey:PARAM_DESTI_LATITUDE];
            [dictParam setValue:strForDestinationLog  forKey:PARAM_DESTI_LONGITUDE];
            [dictParam setValue:strForRideLookup forKey:PARAM_REQUEST_TYPE];
            [dictParam setValue:strForSourceLocation forKey:@"s_location"];
            [dictParam setValue:strForDestinationLocation forKey:@"d_location"];
            [dictParam setValue:@"1" forKey:@"payment_mode"];
            if([self.lblPickedDate.text isEqualToString:@"Start Time"])
            {
                self.lblPickedDate.text=@"";
                [dictParam setValue:self.lblPickedDate.text forKey:PARAM_START_TIME];
            }
            else
                [dictParam setValue:[NSString stringWithFormat:@"%@:00",self.lblPickedDate.text] forKey:PARAM_START_TIME];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSLog(@"pick up......%@",response);
                         if([[response valueForKey:@"success"]boolValue])
                         {
                             NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                             strForRequestID=[response objectForKey:@"request_id"];
                             [pref setValue:strForRequestID forKey:PREF_REQ_ID];
                             [pref synchronize];
                             [self.viewForDriver setHidden:NO];
                             [self showViewForDriver];
                             timerForLookingUp = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showViewForDriver) userInfo:nil repeats:YES];
                             [self setTimerToCheckDriverStatus];
                         }
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                         
                     }
                 }
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
}

/*- (IBAction)onClickRequestButton:(id)sender
{
    [self.viewForDriver setHidden:NO];
    timerForLookingUp = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showViewForDriver) userInfo:nil repeats:YES];
}*/

- (IBAction)onClickGotIt:(id)sender
{
    self.viewETA.hidden=YES;
}

- (IBAction)onClickServiceSelect:(id)sender
{
    if(sender==self.btnCar)
    {
        self.imgForCarService.image=[UIImage imageNamed:@"car_rate_card"];
        self.lblCarService.textColor=[UIColor purpleColor];
        self.imgForBikeService.image=[UIImage imageNamed:@"bike_blank"];
        self.lblBikeService.textColor=[UIColor blackColor];
        self.imgForEither.image=[UIImage imageNamed:@"both"];
        self.lblEitherService.textColor=[UIColor blackColor];
        strForRideLookup=@"1";
        NSLog(@"%@",strForRideLookup);
    }
    else if (sender==self.btnBike)
    {
        self.imgForBikeService.image=[UIImage imageNamed:@"bike_rate_card"];
        self.lblBikeService.textColor=[UIColor purpleColor];
        self.imgForEither.image=[UIImage imageNamed:@"both"];
        self.lblEitherService.textColor=[UIColor blackColor];
        self.imgForCarService.image=[UIImage imageNamed:@"car_blank"];
        self.lblCarService.textColor=[UIColor blackColor];
        strForRideLookup=@"2";
        NSLog(@"%@",strForRideLookup);
    }
    else
    {
        self.imgForEither.image=[UIImage imageNamed:@"both_select"];
        self.lblEitherService.textColor=[UIColor purpleColor];
        self.imgForBikeService.image=[UIImage imageNamed:@"bike_blank"];
        self.lblBikeService.textColor=[UIColor blackColor];
        self.imgForCarService.image=[UIImage imageNamed:@"car_blank"];
        self.lblCarService.textColor=[UIColor blackColor];
        strForRideLookup=@"1,2";
        NSLog(@"%@",strForRideLookup);
    }
}
- (IBAction)onClickCancelPicker:(id)sender
{
    [self.viewForPicker setHidden:YES];
}

- (IBAction)onClickDonePicker:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedDateString;
    formattedDateString = [dateFormatter stringFromDate:self.datePickerNew.date];
    self.lblPickedDate.text = formattedDateString;
    [self.viewForPicker setHidden:YES];
}

- (IBAction)todayDrivingTypeBtn:(id)sender
{
    if(sender==self.btnDrivingCar)
    {
        [self.btnDrivingCar setSelected:YES];
        [self.btnDrivingBike setSelected:NO];
        strForSelectedType=@"1";
    }
    else
    {
        [self.btnDrivingBike setSelected:YES];
        [self.btnDrivingCar setSelected:NO];
        strForSelectedType=@"2";
    }
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_ADMIN_APPROVE", nil)];
    [self respondToRequest];
}

- (IBAction)offerRideBtnPressed:(id)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    
    if([[dictInfo valueForKey:@"type"]isEqualToString:@"1,2"] || [[dictInfo valueForKey:@"type"]isEqualToString:@"2,1"])
    {
        isOfferRideByBoth=1;
        self.lblSelectVehicle.hidden=NO;
        [self.btnOfferRideBike setHidden:NO];
        [self.btnOfferRideCar setHidden:NO];
    }
    else
    {
        self.lblSelectVehicle.hidden=YES;
        [self.btnOfferRideBike setHidden:YES];
        [self.btnOfferRideCar setHidden:YES];
        self.viewForOfferRide.frame = CGRectMake(self.viewForOfferRide.frame.origin.x, self.viewForOfferRide.frame.origin.y, self.viewForOfferRide.frame.size.width, self.lblOfferRideStartTime.frame.size.height+self.lblOfferRideStartTime.frame.origin.y);
    }
    
    [self.viewForOfferRide setHidden:NO];
}

- (IBAction)usuallyFromGoBtn:(id)sender
{
    if(sender==self.btnUsuallyFrom)
    {
        [self.btnUsuallyFrom setSelected:YES];
        [self.btnUsuallyGo setSelected:NO];
        self.txtOfferRideSource.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"SourceLocationFrom"];
        self.txtOfferRideDestination.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"DestinationLocationFrom"];
        strForPatternType=@"1";
    }
    else
    {
        [self.btnUsuallyGo setSelected:YES];
        [self.btnUsuallyFrom setSelected:NO];
        self.txtOfferRideSource.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"SourceLocationGo"];
        self.txtOfferRideDestination.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"DestinationLocationGo"];
        strForPatternType=@"2";
    }
}
- (IBAction)offerRideVehicle:(id)sender
{
    if(sender==self.btnOfferRideCar)
    {
        [self.btnOfferRideCar setSelected:YES];
        [self.btnOfferRideBike setSelected:NO];
        strForOfferRideLookup=@"1";
    }
    else
    {
        [self.btnOfferRideBike setSelected:YES];
        [self.btnOfferRideCar setSelected:NO];
        strForOfferRideLookup=@"2";
    }
}

- (IBAction)finalOfferARide:(id)sender
{
    if(strForPatternType.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_PATTERN", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (isOfferRideByBoth==1)
    {
        if(strForOfferRideLookup.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_RIDE_LOOKUP", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        if(([[AppDelegate sharedAppDelegate]connected]))
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CREATE_REQUEST", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strForPatternType forKey:PARAM_PATTERN_TYPE];
            if(isOfferRideByBoth==1)
            {
                [dictParam setValue:strForOfferRideLookup forKey:PARAM_REQUEST_TYPE];
            }
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getProviderDataFromPath:FILE_OFFER_RIDE withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         
                     }
                 }
             }];
        }
    }
}

- (IBAction)closeOfferRideBtn:(id)sender
{
    [self.viewForOfferRide setHidden:YES];
    self.txtOfferRideSource.text = @"";
    self.txtOfferRideDestination.text = @"";
    [self.btnUsuallyFrom setSelected:NO];
    [self.btnUsuallyGo setSelected:NO];
    [self.tableForCity setHidden:YES];
}

- (IBAction)onClickSideProfile:(id)sender
{
    if(self.btnSideProfile.selected==YES)
    {
        self.btnSideProfile.selected=NO;
        [self.viewForSideProfile setHidden:YES];
    }
    else
    {
        self.btnSideProfile.selected=YES;
        [self.viewForSideProfile setHidden:NO];
    }
}

@end