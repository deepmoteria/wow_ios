//
//  FeedBackVC.h
//  UberNewUser
//
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"
#import "SWRevealViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>

@interface FeedBackVC : BaseVC<UITextViewDelegate,FBSDKSharingDelegate,MFMailComposeViewControllerDelegate>
{
    SLComposeViewController *fb,*mySLComposerSheet;
}

@property (weak, nonatomic) IBOutlet RatingBar *ratingBar;

@property (weak, nonatomic) IBOutlet UITextView *txtComments;

@property (nonatomic,strong) NSMutableDictionary *dictWalkInfo;

@property (nonatomic,strong) NSString *strUserImg;
@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic,strong) NSString *strLastName;

@property (weak, nonatomic) IBOutlet UILabel *lblDistCost;
@property (weak, nonatomic) IBOutlet UILabel *lblRideWith;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lComment;

@property (weak, nonatomic) IBOutlet UIButton *btnPayNow;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnFeedBack;


- (IBAction)onClickPayNow:(id)sender;
- (IBAction)facebookBtnPressed:(id)sender;
- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)submitBtnPressed:(id)sender;

@end