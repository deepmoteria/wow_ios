//
//  ProviderDetailsVC.m
//  UberNewUser
//
//  Created by Deep Gami on 29/10/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ProviderDetailsVC.h"
#import "SWRevealViewController.h"
#import "sbMapAnnotation.h"
#import "UIImageView+Download.h"
#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "RateView.h"
#import "UIView+Utils.h"
#import "UberStyleGuide.h"

@interface ProviderDetailsVC ()

{
    NSDate *dateForwalkStartedTime;
    BOOL isTimerStaredForMin,isWalkInStarted,pathDraw;
    NSMutableArray *arrPath;
    GMSMutablePath *pathUpdates;
    NSString *strUSerImage,*strLastName,*strNearestLocation;
    NSString *strProviderPhone,*strTime,*strDistance,*strForDestLat,*strForDestLong,*strForProviderLat,*strForProviderLong;
    GMSMapView *mapView_;
    GMSMarker *client_marker,*source_marker,*destination_marker;
    GMSMutablePath *pathpoliline;
    NSDictionary *dictCard;
    BOOL iscash,isFirst,isPinDest;
    float totalDist;
}

@end

@implementation ProviderDetailsVC
@synthesize strForCurrentLongitude,strForCurrentLatitude,strForSourceLatitude,strForSourceLongitude,strForDestinationLatitude,strForDestinationLongitude,timerForTimeAndDistance,timerForCheckReqStatuss;

#pragma mark -
#pragma mark - View DidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    isOnTrip=1;
    self.btnSos.hidden=YES;
    [self.viewForDetails setHidden:YES];
    APPDELEGATE.vcProvider=self;
    [self customSetup];
    [self updateLocationManager];
    arrPath=[[NSMutableArray alloc]init];
    pathUpdates = [GMSMutablePath path];
    pathUpdates = [[GMSMutablePath alloc]init];
    isTimerStaredForMin=NO;
    pathDraw=YES;
    isFirst=NO;
    isPinDest = NO;
    totalDist=0;
    CLLocationCoordinate2D current = [self getLocation];
    strForCurrentLatitude = [NSString stringWithFormat:@"%f", current.latitude];
    strForCurrentLongitude = [NSString stringWithFormat:@"%f", current.longitude];
    
    NSLog(@"current location = %@",self.strCurrentClientAddress);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurrentLatitude doubleValue] longitude:[strForCurrentLongitude doubleValue]zoom:14];
    mapView_=[GMSMapView mapWithFrame:CGRectMake(0, 0,self.viewForMap.frame.size.width,self.viewForMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    [self.viewForMap addSubview:mapView_];
    mapView_.delegate=self;

    client_marker = [[GMSMarker alloc] init];
    client_marker.position = CLLocationCoordinate2DMake([strForCurrentLatitude doubleValue], [strForCurrentLongitude doubleValue]);
    client_marker.icon=[UIImage imageNamed:@"pin_you"];
    client_marker.map = mapView_;
    
    source_marker = [[GMSMarker alloc] init];
    source_marker.position = CLLocationCoordinate2DMake([strForSourceLatitude doubleValue], [strForSourceLongitude doubleValue]);
    source_marker.icon=[UIImage imageNamed:@"pin_source"];
    source_marker.map = mapView_;
    
    destination_marker = [[GMSMarker alloc] init];
    destination_marker.position = CLLocationCoordinate2DMake([strForDestinationLatitude doubleValue], [strForDestinationLongitude doubleValue]);
    destination_marker.icon=[UIImage imageNamed:@"pin_destination"];
    destination_marker.map = mapView_;
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    isWalkInStarted=[pref boolForKey:PREF_IS_WALK_STARTED];
    
    isWalkInStarted=NO;
    
    if(isWalkInStarted)
    {
        [self requestPath];
    }
    
    if([strForUserType isEqualToString:@"1"])
    {
        [self.btnCancel setHidden:YES];
        [self.btnRideStart setHidden:NO];
        [self.btnRideEnd setHidden:YES];
        [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_ACCEPTED", nil) forState:UIControlStateNormal];
        [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_ACCEPTED", nil) forState:UIControlStateSelected];
        [self checkStatus];
        [self setTakerData];
    }
    else
    {
        [self checkForTripStatus];
        
        if([[pref valueForKey:PREF_OFFER_RIDE] boolValue]==YES)
            [self.btnCancel setHidden:YES];
        else
            [self.btnCancel setHidden:NO];
        
        [self.btnRideStart setHidden:YES];
        [self.btnRideEnd setHidden:YES];
        timerForCheckReqStatuss = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForTripStatus) userInfo:nil repeats:YES];
    }
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeView)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self viewForDetails] addGestureRecognizer:swipe];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [self.ratingView initRateBar];
    [self.ratingView setUserInteractionEnabled:NO];
    [self.imgForDriverProfile applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.imgDetailPicture applyRoundedCornersFullWithColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)SetLocalization
{
    [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_CONFIRMED", nil) forState:UIControlStateNormal];
    [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_CONFIRMED", nil) forState:UIControlStateSelected];
    [self.btnCall setTitle:NSLocalizedString(@"CALL", nil) forState:UIControlStateNormal];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.revealBtnItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

#pragma mark - swipe method

-(void)SwipeView
{
    if(UISwipeGestureRecognizerDirectionRight)
    {
        [self.viewForDetails setHidden:NO];
    }
    else
    {
        [self.viewForDetails setHidden:YES];
    }
}

#pragma mark - Custom Methods

-(void)checkStatus
{
    if (is_started==1)
    {
        [self updateLocationManager];
        self.timeForUpdateWalkLoc = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(updateWalkLocation) userInfo:nil repeats:YES];
        if (is_completed==1)
        {
            if (is_dog_rated==1)
            {
                
            }
            else
            {
                [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:nil];
            }
        }
        else
        {
            [self.btnRideEnd setHidden:NO];
            [self.btnRideStart setHidden:YES];
            self.btnSos.frame = CGRectMake(self.btnCall.frame.size.width+self.btnCall.frame.size.width/2, self.btnSos.frame.origin.y, self.btnSos.frame.size.width, self.btnSos.frame.size.height);
            [self.btnSos setHidden:NO];
        }
    }
    else
    {
        [self.btnRideEnd setHidden:YES];
        [self.btnRideStart setHidden:NO];
        [self.btnSos setHidden:YES];
    }
}

-(void)setTakerData
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref valueForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableDictionary *dictRideGiver = [[NSMutableDictionary alloc]init];
                     
                     dictRideGiver = [[response valueForKey:@"request"] valueForKey:@"owner"];
                     
                     self.lblRateValue.text=[NSString stringWithFormat:@"%.1f",[[dictOwner valueForKey:@"rating"] floatValue]];
                     
                     RBRatings rate=([[dictRideGiver valueForKey:@"rating"]floatValue]*2);
                     [self.ratingView setRatings:rate];
                     
                     self.lblDriverName.text=[NSString stringWithFormat:@"%@",[dictRideGiver valueForKey:@"name"]];
                     
                     self.lblDriverDetail.text=[dictRideGiver valueForKey:@"phone"];
                     strProviderPhone=[NSString stringWithFormat:@"%@",[dictRideGiver valueForKey:@"phone"]];
                     [self.imgForDriverProfile downloadFromURL:[dictRideGiver valueForKey:@"picture"] withPlaceholder:nil];
                     strUSerImage=[dictRideGiver valueForKey:@"picture"];
                     
                     NSString *str = [dictRideGiver valueForKey:@"login_by"];
                     
                     if([str isEqualToString:@"facebook"])
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"facebook"]];
                     }
                     else if ([str isEqualToString:@"google"])
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"google"]];
                     }
                     else
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"linkedIn"]];
                     }
                     
                     [self.imgDetailPicture downloadFromURL:[dictRideGiver valueForKey:@"picture"] withPlaceholder:nil];
                     self.lblDetailName.text = [NSString stringWithFormat:@"%@",[dictRideGiver valueForKey:@"name"]];
                     self.lblDetailSource.text = [dictRideGiver valueForKey:@"s_location"];
                     self.lblDetailDestination.text = [dictRideGiver valueForKey:@"d_location"];
                     self.lblDetailStartTime.text = [dictRideGiver valueForKey:@"start_time"];
                     self.lblOrganizationName.text = [dictRideGiver valueForKey:@"organization"];
                     
                     if([[dictOwner valueForKey:@"selected_type"]isEqualToString:@"1"])
                     {
                         self.imgDetailType.image = [UIImage imageNamed:@"car_rate_card"];
                         self.lblDetailNumber.text = [[dictRideGiver valueForKey:@"car_number"] uppercaseString];
                         self.lblDetailType.text = @"Car Registration No";
                     }
                     else
                     {
                         self.imgDetailType.image = [UIImage imageNamed:@"bike_rate_card"];
                         self.lblDetailNumber.text = [[dictRideGiver valueForKey:@"bike_number"] uppercaseString];
                         self.lblDetailType.text = @"Bike Registration No";
                     }
                     
                     client_marker.position = CLLocationCoordinate2DMake([[dictRideGiver valueForKey:@"latitude"] doubleValue], [[dictRideGiver valueForKey:@"longitude"] doubleValue]);
                     client_marker.icon=[UIImage imageNamed:@"pin_you"];
                     client_marker.map = mapView_;
                     
                     source_marker.position = CLLocationCoordinate2DMake([[dictRideGiver valueForKey:@"s_latitude"] doubleValue], [[dictRideGiver valueForKey:@"s_longitude"] doubleValue]);
                     source_marker.icon=[UIImage imageNamed:@"pin_source"];
                     source_marker.map = mapView_;
                     
                     destination_marker.position = CLLocationCoordinate2DMake([[dictRideGiver valueForKey:@"d_latitude"] doubleValue], [[dictRideGiver valueForKey:@"d_longitude"] doubleValue]);
                     destination_marker.icon=[UIImage imageNamed:@"pin_destination"];
                     destination_marker.map = mapView_;
                     
                     [self centerMapFirst:client_marker.position two:source_marker.position third:destination_marker.position];
                     
                     is_started=[[[response valueForKey:@"request"] valueForKey:@"is_walk_started"] intValue];
                     is_completed=[[[response valueForKey:@"request"] valueForKey:@"is_completed"] intValue];
                     is_dog_rated=[[[response valueForKey:@"request"] valueForKey:@"is_walker_rated"] intValue];
                     
                     if(is_completed==1)
                     {
                         totalDist=[[response valueForKey:@"distance"]floatValue];
                     }
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }];
    }
}


-(void)checkForTripStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref valueForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"GET REQ--->%@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableDictionary *dictOwner=[response valueForKey:@"owner"];
                     NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                     
                     self.lblRateValue.text=[NSString stringWithFormat:@"%.1f",[[dictWalker valueForKey:@"rating"] floatValue]];
                     
                     RBRatings rate=([[dictWalker valueForKey:@"rating"]floatValue]*2);
                     [self.ratingView setRatings:rate];
                     
                     strLastName=[dictWalker valueForKey:@"last_name"];
                     self.lblDriverName.text=[NSString stringWithFormat:@"%@ %@",[dictWalker valueForKey:@"first_name"],strLastName];
                     
                     self.lblDriverDetail.text=[dictWalker valueForKey:@"phone"];
                     strProviderPhone=[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"phone"]];
                     [self.imgForDriverProfile downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:nil];
                     strUSerImage=[dictWalker valueForKey:@"picture"];
                     
                     dictCard=[response valueForKey:@"card_details"];
                     
                     NSString *str = [dictWalker valueForKey:@"login_by"];
                     
                     if([str isEqualToString:@"facebook"])
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"facebook"]];
                     }
                     else if ([str isEqualToString:@"google"])
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"btn_google"]];
                     }
                     else
                     {
                         [self.imgLoginWith setImage:[UIImage imageNamed:@"btn_linked_in"]];
                     }
                     
                     [self.imgDetailPicture downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:nil];
                     self.lblDetailName.text = [NSString stringWithFormat:@"%@ %@",[dictWalker valueForKey:@"first_name"],[dictWalker valueForKey:@"last_name"]];
                     self.lblDetailSource.text = [dictWalker valueForKey:@"s_location"];
                     self.lblDetailDestination.text = [dictWalker valueForKey:@"d_location"];
                     self.lblDetailStartTime.text = [dictWalker valueForKey:@"start_time"];
                     self.lblOrganizationName.text = [dictWalker valueForKey:@"organization"];
                     
                     if([[dictWalker valueForKey:@"selected_type"]isEqualToString:@"1"])
                     {
                         self.imgDetailType.image = [UIImage imageNamed:@"car_rate_card"];
                         self.lblDetailNumber.text = [[dictWalker valueForKey:@"car_number"] uppercaseString];
                         self.lblDetailType.text = @"Car Registration No";
                     }
                     else
                     {
                         self.imgDetailType.image = [UIImage imageNamed:@"bike_rate_card"];
                         self.lblDetailNumber.text = [[dictWalker valueForKey:@"bike_number"] uppercaseString];
                         self.lblDetailType.text = @"Bike Registration No";
                     }
                     
                     client_marker.position = CLLocationCoordinate2DMake([[dictOwner valueForKey:@"owner_lat"] doubleValue], [[dictOwner valueForKey:@"owner_long"] doubleValue]);
                     client_marker.icon=[UIImage imageNamed:@"pin_you"];
                     client_marker.map = mapView_;
                     
                     source_marker.position = CLLocationCoordinate2DMake([[dictWalker valueForKey:@"latitude"] doubleValue], [[dictWalker valueForKey:@"longitude"] doubleValue]);
                     source_marker.icon=[UIImage imageNamed:@"pin_source"];
                     source_marker.map = mapView_;
                     
                     destination_marker.position = CLLocationCoordinate2DMake([[dictWalker valueForKey:@"d_latitude"] doubleValue], [[dictWalker valueForKey:@"d_longitude"] doubleValue]);
                     destination_marker.icon=[UIImage imageNamed:@"pin_destination"];
                     destination_marker.map = mapView_;
                     
                     [self centerMapFirst:client_marker.position two:source_marker.position third:destination_marker.position];
                     
                     is_started=[[response valueForKey:@"is_walk_started"] intValue];
                     is_completed=[[response valueForKey:@"is_completed"] intValue];
                     is_dog_rated=[[response valueForKey:@"is_walker_rated"] intValue];
                     strDistance=[NSString stringWithFormat:@"%.2f %@",[[response valueForKey:@"distance"] floatValue],[response valueForKey:@"unit"]];
                     
                     [self checkDriverStatus];
                     
                     if(is_completed==1)
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
                         
                         dictBillInfo=[response valueForKey:@"bill"];
                         
                         FeedBackVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[FeedBackVC class]])
                             {
                                 vcFeed = (FeedBackVC *)vc;
                             }
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatuss invalidate];
                             [timerForTimeAndDistance invalidate];
                             timerForTimeAndDistance=nil;
                             timerForCheckReqStatuss=nil;
                             [self.timerforpathDraw invalidate];
                             [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                             [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
                         }
                         else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                     
                     else if(is_started==1)
                     {
                         [self.btnSos setHidden:NO];
                         [self.btnCancel setHidden:YES];
                         [locationManager startUpdatingLocation];
                         
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
                         
                         //strForSourceLatitude=[dictWalker valueForKey:@"latitude"];
                         //strForSourceLongitude=[dictWalker valueForKey:@"longitude"];
                     }
                 }
                 else
                 {
                     
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
-(void)requestPath
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
    NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_REQUEST_PATH,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
     {
         
         NSLog(@"Page Data= %@",response);
         if (response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [arrPath removeAllObjects];
                 arrPath=[response valueForKey:@"locationdata"];
                 //[self drawPath];
             }
         }
         
     }];
}

-(int)checkDriverStatus
{
    if(is_started==1)
    {
        [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_PROGRESS", nil) forState:UIControlStateNormal];
        [self.revealBtnItem setTitle:NSLocalizedString(@"RIDE_PROGRESS", nil) forState:UIControlStateSelected];
        [self.viewWithKms setHidden:NO];
        [self.viewWithoutKms setHidden:YES];
        isWalkInStarted=YES;
    }
    else
    {
        [self.viewWithKms setHidden:YES];
        [self.viewWithoutKms setHidden:NO];
    }
    
    if(is_completed==1)
    {
        isWalkInStarted=NO;
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
    }
    return 5;
}

#pragma mark -
#pragma mark - Location Delegate

-(CLLocationCoordinate2D) getLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

-(void)updateLocationManager
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil)
    {
        if (newLocation.coordinate.latitude == oldLocation.coordinate.latitude && newLocation.coordinate.longitude == oldLocation.coordinate.longitude)
        {
            
        }
        else
        {
            //[mapView_  clear];
            //[pathUpdates addCoordinate:newLocation.coordinate];
            
            GMSMarker *markerOwner1 = [[GMSMarker alloc] init];
            markerOwner1.position = CLLocationCoordinate2DMake([strForCurrentLatitude doubleValue], [strForCurrentLongitude doubleValue]);
            markerOwner1.icon = [UIImage imageNamed:@"pin_you"];
            markerOwner1.map = mapView_;
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

-(void)showMapCurrentLocatin
{
    if([CLLocationManager locationServicesEnabled])
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurrentLatitude doubleValue] longitude:[strForCurrentLongitude doubleValue]zoom:14];
        mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 416) camera:camera];
        mapView_.myLocationEnabled = NO;
        [self.viewForMap addSubview:mapView_];
        client_marker = [[GMSMarker alloc] init];
        client_marker.position = CLLocationCoordinate2DMake([strForCurrentLatitude doubleValue], [strForCurrentLongitude doubleValue]);
        client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
        client_marker.map = mapView_;
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Wow Client -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
}

-(void)updateWalkLocation
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([APPDELEGATE connected])
        {
            if(((strForCurrentLatitude==nil)&&(strForCurrentLongitude==nil))
               ||(([strForCurrentLatitude doubleValue]==0.00)&&([strForCurrentLongitude doubleValue]==0)))
            {
                
            }
            else
            {
                NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                NSString *strUserId = [pref valueForKey:PREF_USER_ID];
                NSString *strUserToken = [pref valueForKey:PREF_USER_TOKEN];
                NSString *strRequsetId = [pref valueForKey:PREF_REQ_ID];
                
                NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
                [dictparam setObject:strUserId forKey:PARAM_ID];
                [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
                [dictparam setObject:strForCurrentLatitude forKey:PARAM_LATITUDE];
                [dictparam setObject:strForCurrentLongitude forKey:PARAM_LONGITUDE];
                [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];

                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getOnlyDataFromPath:FILE_WALK_LOCATION withParamData:dictparam withBlock:^(id response, NSError *error)
                 {
                     NSLog(@"Update Walk Location = %@",response);
                     if (response)
                     {
                        response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                        if([[response valueForKey:@"success"] intValue]==1)
                         {
                             totalDist=[[response valueForKey:@"distance"]floatValue];
                             
                             if ([[response valueForKey:@"is_cancelled"] intValue]==1)
                             {
                                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                 [pref removeObjectForKey:PREF_REQ_ID];
                                 [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCELLED", nil) ];
                                 is_completed=0;
                                 is_walker_started=0;
                                 is_walker_arrived=0;
                                 is_started=0;
                                 [self.navigationController popToRootViewControllerAnimated:YES];
                            }
                         }
                         else
                         {
                             NSLog(@"error in walklocation");
                         }
                     }
                 }];
            }
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow Driver -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

        }
    }
    if(alertView.tag==200)
    {
        if(buttonIndex == 1)
        {
            [self cancelRequest];
        }
    }
    
}

#pragma mark - 
#pragma mark - center map method

-(void)centerMapFirst:(CLLocationCoordinate2D)pos1 two:(CLLocationCoordinate2D)pos2 third:(CLLocationCoordinate2D)pos3
{
    GMSCoordinateBounds* bounds =
    [[GMSCoordinateBounds alloc]initWithCoordinate:pos1 coordinate:pos2];
    bounds=[bounds includingCoordinate:pos3];
    CLLocationCoordinate2D location1 = bounds.southWest;
    CLLocationCoordinate2D location2 = bounds.northEast;
    
    float mapViewWidth = mapView_.frame.size.width;
    float mapViewHeight = mapView_.frame.size.height;
    
    MKMapPoint point1 = MKMapPointForCoordinate(location1);
    MKMapPoint point2 = MKMapPointForCoordinate(location2);
    
    MKMapPoint centrePoint = MKMapPointMake(
                                            (point1.x + point2.x) / 2,
                                            (point1.y + point2.y) / 2);
    CLLocationCoordinate2D centreLocation = MKCoordinateForMapPoint(centrePoint);
    
    double mapScaleWidth = mapViewWidth / fabs(point2.x - point1.x);
    double mapScaleHeight = mapViewHeight / fabs(point2.y - point1.y);
    double mapScale = MIN(mapScaleWidth, mapScaleHeight);
    
    double zoomLevel = 19.5 + log2(mapScale);
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:centreLocation zoom: zoomLevel];
    [mapView_ animateWithCameraUpdate:updatedCamera];
}

#pragma mark - Action methods

- (IBAction)onClickRevelButton:(id)sender
{
    
}

- (IBAction)cancelBtnPressed:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CANCEL_RIDE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    alert.tag=200;
    [alert show];
}
 
- (IBAction)onClickSosBtn:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref valueForKey:PREF_REQ_ID];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:strForUserId forKey:PARAM_ID];
        [dict setValue:strForUserToken forKey:PARAM_TOKEN];
        [dict setValue:strReqId forKey:PARAM_REQUEST_ID];
        [dict setValue:self.strCurrentClientAddress forKey:@"nearest_location"];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_SOS withParamData:nil withBlock:^(id response, NSError *error)
        {
            if(response)
            {
                response=[[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                if([[response valueForKey:@"success"] boolValue])
                {
                    
                }
            }
        }];
    }
}

- (IBAction)contactProviderBtnPressed:(id)sender
{
    NSString *call=[NSString stringWithFormat:@"tel://%@",strProviderPhone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}

-(void)cancelRequest
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]hideLoadingView];
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [timerForCheckReqStatuss invalidate];
                     timerForCheckReqStatuss=nil;
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                     is_walker_arrived=0;
                     is_walker_started=0;
                     is_completed=0;
                     is_started=0;
                     is_dog_rated=0;
                     isOnTrip=0;
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }

}

- (IBAction)sideArrowClicked:(id)sender
{
    if(self.btnDetails.selected==YES)
    {
        self.btnDetails.selected=NO;
        [self.viewForDetails setHidden:YES];
    }
    else
    {
        self.btnDetails.selected=YES;
        [self.viewForDetails setHidden:NO];
    }
}

- (IBAction)RideStartedBtn:(id)sender
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_RIDE_START", nil)];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strUserId=[pref objectForKey:PREF_USER_ID];
    NSString *strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    NSString *strRequsetId=[pref objectForKey:PREF_REQ_ID];
    
    if (strRequsetId!=nil)
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strForCurrentLatitude forKey:PARAM_LATITUDE];
        [dictparam setObject:strForCurrentLatitude forKey:PARAM_LONGITUDE];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getProviderDataFromPath:FILE_RIDE_STARTED withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     is_started=1;
                     self.timeForUpdateWalkLoc = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(updateWalkLocation) userInfo:nil repeats:YES];
                     [self.btnRideStart setHidden:YES];
                     [self.btnRideEnd setHidden:NO];
                     self.btnSos.frame = CGRectMake(self.btnCall.frame.size.width+self.btnCall.frame.size.width/2, self.btnSos.frame.origin.y, self.btnSos.frame.size.width, self.btnSos.frame.size.height);
                     [self.btnSos setHidden:NO];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                      [alert show];
                 }
             }
             
         }];
    }
}

- (IBAction)RideEndedBtn:(id)sender
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_RIDE_COMPLETE", nil)];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strUserId=[pref objectForKey:PREF_USER_ID];
    NSString *strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    NSString *strRequsetId=[pref objectForKey:PREF_REQ_ID];
    
    if (strRequsetId!=nil)
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strForCurrentLatitude forKey:PARAM_LATITUDE];
        [dictparam setObject:strForCurrentLongitude forKey:PARAM_LONGITUDE];
        [dictparam setObject:[NSString stringWithFormat:@"%.2f",totalDist] forKey:PARAM_DISTANCE];

        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getProviderDataFromPath:FILE_RIDE_COMPLETED withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"ride ended response %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [self.timeForUpdateWalkLoc invalidate];
                     dictBillInfo=[response valueForKey:@"bill"];
                     [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSMutableDictionary *dictWalkInfo=[[NSMutableDictionary alloc]init];
    NSString *distance= strDistance;
    
    NSArray *arrDistace=[distance componentsSeparatedByString:@" "];
    float dist;
    dist=[[arrDistace objectAtIndex:0]floatValue];
    if (arrDistace.count>1)
    {
        
        if ([[arrDistace objectAtIndex:1] isEqualToString:@"kms"])
        {
            dist=dist*0.621371;
            
        }
    }
    [dictWalkInfo setObject:[NSString stringWithFormat:@"%f",dist] forKey:@"distance"];
    //[dictWalkInfo setObject:strTime forKey:@"time"];
    if([segue.identifier isEqualToString:SEGUE_TO_FEEDBACK])
    {
        FeedBackVC *obj=[segue destinationViewController];
        obj.dictWalkInfo=dictWalkInfo;
        // obj.dictBillInfo=dictBillInfo;
        obj.strUserImg=strUSerImage;
        obj.strFirstName=self.lblDriverName.text;
    }
}

@end