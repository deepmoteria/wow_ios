//
//  PickUpVC.h
//  UberNewUser
//
//  Created by Elluminati - macbook on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>


@interface PickUpVC : BaseVC<MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,GMSMapViewDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    NSTimer *timerForCheckReqStatus,*timerForOfferRide;;
    CLLocationManager *locationManager;
    NSDictionary* aPlacemark;
    NSMutableArray *placeMarkArr;
}

@property(nonatomic,weak)IBOutlet MKMapView *map;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerNew;

@property (weak, nonatomic) IBOutlet UITextView *textViewForOrganizartion;

@property (weak, nonatomic) IBOutlet UITableView *tableForCity;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSTimer *timerForUpdateLocation;
@property(nonatomic, strong) NSTimer *timeForAcceptReject;

@property (weak, nonatomic) IBOutlet UIView *viewForOfferRide;
@property (weak, nonatomic) IBOutlet UIView *viewGoogleMap;
@property (weak, nonatomic) IBOutlet UIView *viewForSideProfile;
@property (weak, nonatomic) IBOutlet UIView *viewForTodayDriving;
@property (weak, nonatomic) IBOutlet UIView *viewETA;
@property (weak, nonatomic) IBOutlet UIView *viewForApproval;
@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIView *viewForOrganization;
@property (weak, nonatomic) IBOutlet UIView *viewForRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForDriver;

@property (weak, nonatomic) IBOutlet UIImageView *imgForOwnerProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetailPicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgLoginWith;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetailType;
@property (weak, nonatomic) IBOutlet UIImageView *imgConfirmation;
@property (weak, nonatomic) IBOutlet UIImageView *imgTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgForCarService;
@property (weak, nonatomic) IBOutlet UIImageView *imgForBikeService;
@property (weak, nonatomic) IBOutlet UIImageView *imgForEither;
@property (weak, nonatomic) IBOutlet UIImageView *imgForOrganization;
@property (weak, nonatomic) IBOutlet UIImageView *img_driver_profile;

@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtDestAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtOfferRideSource;
@property (weak, nonatomic) IBOutlet UITextField *txtOfferRideDestination;

@property (weak, nonatomic) IBOutlet UILabel *lblTimerOrganizationName;
@property (weak, nonatomic) IBOutlet UILabel *lblRateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailName;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *lblSideOrganizationName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailSource;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailDestination;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailType;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPickedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectVehicle;
@property (weak, nonatomic) IBOutlet UILabel *lblUsuallyGo;
@property (weak, nonatomic) IBOutlet UILabel *lblUsuallyFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblMins;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferRideStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCarService;
@property (weak, nonatomic) IBOutlet UILabel *lblBikeService;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiAmountOfBike;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblEitherService;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiSource;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiDesti;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizationName;
@property (weak, nonatomic) IBOutlet UILabel *lblEstiAmountOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverRate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_Carname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_CarNumber;

@property (nonatomic) IBOutlet UIButton* revealButtonItem;

@property (weak, nonatomic) IBOutlet UIButton *btnSideProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnDrivingCar;
@property (weak, nonatomic) IBOutlet UIButton *btnDrivingBike;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferRide;
@property (weak, nonatomic) IBOutlet UIButton *btnUsuallyGo;
@property (weak, nonatomic) IBOutlet UIButton *btnUsuallyFrom;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferRideCar;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferRideBike;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnETA;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnCar;
@property (weak, nonatomic) IBOutlet UIButton *btnBike;
@property (weak, nonatomic) IBOutlet UIButton *btnBoth;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequestRide;

-(void)goToSetting:(NSString *)str;

- (IBAction)onClickSideProfile:(id)sender;
- (IBAction)todayDrivingTypeBtn:(id)sender;
- (IBAction)offerRideBtnPressed:(id)sender;
- (IBAction)usuallyFromGoBtn:(id)sender;
- (IBAction)offerRideVehicle:(id)sender;
- (IBAction)finalOfferARide:(id)sender;
- (IBAction)closeOfferRideBtn:(id)sender;
- (IBAction)onClickDropForTime:(id)sender;
- (IBAction)cancelReqBtnPressed:(id)sender;
- (IBAction)myLocationPressed:(id)sender;
- (IBAction)ETABtnPressed:(id)sender;
- (IBAction)acceptBtnPressed:(id)sender;
- (IBAction)rejectBtnPressed:(id)sender;
- (IBAction)okApprovalBtn:(id)sender;
- (IBAction)onClickRideEstimate:(id)sender;
- (IBAction)onClickRequestButton:(id)sender;
- (IBAction)onClickGotIt:(id)sender;
- (IBAction)onClickServiceSelect:(id)sender;
- (IBAction)onClickCancelPicker:(id)sender;
- (IBAction)onClickDonePicker:(id)sender;


@end