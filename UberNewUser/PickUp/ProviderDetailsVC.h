//
//  ProviderDetailsVC.h
//  UberNewUser
//
//  Created by Deep Gami on 29/10/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "MapView.h"
#import "Place.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CrumbPath.h"
#import "CrumbPathView.h"
#import <GoogleMaps/GoogleMaps.h>

@class RatingBar;

@interface ProviderDetailsVC : BaseVC<MKMapViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    UIImageView* routeView;
    NSArray* routes;
    UIColor* lineColor;
    NSDictionary* aPlacemark;
    NSMutableArray *placeMarkArr;
    GMSMarker *markerOwner;
    GMSMarker *markerDriver;
    CLLocationManager *locationManager;
}

@property (nonatomic,strong) MKPolyline *polyline;

@property(nonatomic,strong) CrumbPath *crumbs;
@property (nonatomic,strong) CrumbPathView *crumbView;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (strong , nonatomic) NSTimer *timerForCheckReqStatuss;
@property (strong , nonatomic) NSTimer *timerForTimeAndDistance;
@property(nonatomic, strong) NSTimer *timeForUpdateWalkLoc;
@property (nonatomic,strong) NSTimer *timerforpathDraw;

@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *longitude;

@property (nonatomic,strong) NSString *strCurrentClientAddress;
@property (nonatomic,strong) NSString *strForCurrentLatitude;
@property (nonatomic,strong) NSString *strForCurrentLongitude;
@property (nonatomic,strong) NSString *strForSourceLatitude;
@property (nonatomic,strong) NSString *strForSourceLongitude;
@property (nonatomic,strong) NSString *strForDestinationLatitude;
@property (nonatomic,strong) NSString *strForDestinationLongitude;

@property (weak, nonatomic) IBOutlet UIImageView *imgKms;
@property (weak, nonatomic) IBOutlet UIImageView *imgDestiDot;
@property (weak, nonatomic) IBOutlet UIImageView *imgYouDot;
@property (weak, nonatomic) IBOutlet UIImageView *imgForDriverProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetailType;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetailPicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgLoginWith;

@property (weak, nonatomic) IBOutlet UIView *viewWithoutKms;
@property (weak, nonatomic) IBOutlet UIView *viewForDriver;
@property (weak, nonatomic) IBOutlet UIView *viewWithKms;
@property (weak, nonatomic) IBOutlet UIView *viewForMap;
@property (weak, nonatomic) IBOutlet UIView *viewForDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnSos;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnDetails;
@property (weak, nonatomic) IBOutlet UIButton *revealBtnItem;
@property (weak, nonatomic) IBOutlet UIButton *btnRideStart;
@property (weak, nonatomic) IBOutlet UIButton *btnRideEnd;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnMin;

@property (weak, nonatomic) IBOutlet UILabel *lblDestinationDot;
@property (weak, nonatomic) IBOutlet UILabel *lblYouDot;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailName;
@property (weak, nonatomic) IBOutlet UILabel *lblRateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblOrganizationName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailSource;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailDestination;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailType;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailNumber;


-(int)checkDriverStatus;

- (IBAction)onClickRevelButton:(id)sender;
- (IBAction)sideArrowClicked:(id)sender;
- (IBAction)RideStartedBtn:(id)sender;
- (IBAction)RideEndedBtn:(id)sender;
- (IBAction)onClickSosBtn:(id)sender;
- (IBAction)contactProviderBtnPressed:(id)sender;

@end