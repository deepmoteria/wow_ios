//
//  SupportVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "HistoryVC.h"
#import "HistoryCell.h"
#import "Constants.h"
#import "UIImageView+Download.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "UberStyleGuide.h"

@interface HistoryVC ()
{
    NSMutableArray *arrHistory;
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection;
}

@end

@implementation HistoryVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    arrHistory=[[NSMutableArray alloc]init];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tableView.hidden=NO;
    self.viewForBill.hidden=YES;
    self.lblnoHistory.hidden=YES;
    self.imgNoDisplay.hidden=YES;
    self.navigationController.navigationBarHidden=NO;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserType = [pref objectForKey:PREF_USER_TYPE];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"GETTING HISTORY", nil)];
    if([strForUserType isEqualToString:@"0"])
    {
        [self getHistory];
        self.btnRideTook.hidden=YES;
        self.btnRideGiven.hidden=YES;
        self.btnRideGivenTook.hidden=NO;
        [self.btnRideGivenTook setTitle:@"Rides Taken" forState:UIControlStateNormal];
        [self.btnRideGivenTook setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.imgForRideTook.hidden=YES;
        self.imgForRideGiven.hidden=YES;
        self.imgForRideGivenTook.hidden=NO;
    }
    else if([strForUserType isEqualToString:@"1"])
    {
        [self getProviderHistory];
        self.btnRideTook.hidden=YES;
        self.btnRideGiven.hidden=YES;
        self.btnRideGivenTook.hidden=NO;
        [self.btnRideGivenTook setTitle:@"Rides Given" forState:UIControlStateNormal];
        [self.btnRideGivenTook setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.imgForRideTook.hidden=YES;
        self.imgForRideGiven.hidden=YES;
        self.imgForRideGivenTook.hidden=NO;
    }
    else if ([strForUserType isEqualToString:@"2"])
    {
        [self getHistory];
        self.btnRideTook.hidden=NO;
        self.btnRideGiven.hidden=NO;
        self.btnRideGivenTook.hidden=YES;
        [self.btnRideGiven setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnRideTook setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.imgForRideTook.hidden=NO;
        self.imgForRideGiven.hidden=NO;
        self.imgForRideGivenTook.hidden=YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_HISTORY", nil) forState:UIControlStateNormal];
}

-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lBasePrice.text=NSLocalizedString(@"BASE PRICE", nil);
    self.lDistanceCost.text=NSLocalizedString(@"DISTANCE COST", nil);
    self.lTimeCost.text=NSLocalizedString(@"TIME COST", nil);
    self.lPromoBonus.text=NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lreferalBonus.text=NSLocalizedString(@"REFERRAL BOUNCE", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    [self.btnClose setTitle:NSLocalizedString(@"Close",nil) forState:UIControlStateNormal];
}

#pragma mark - Custom Methods

-(void)getHistory
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_HISTORY,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"History Data= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE hideLoadingView];
                     
                     arrHistory=[response valueForKey:@"requests"];
                     NSLog(@"History count = %lu",(unsigned long)arrHistory.count);
                     if (arrHistory.count==0 || arrHistory==nil)
                     {
                         self.tableView.hidden=YES;
                         self.lblnoHistory.hidden=NO;
                         self.imgNoDisplay.hidden=NO;
                     }
                     else
                     {
                         self.tableView.hidden=NO;
                         self.lblnoHistory.hidden=YES;
                         self.imgNoDisplay.hidden=YES;
                         [self makeSection];
                         [self.tableView reloadData];
                     }
                     
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)getProviderHistory
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_HISTORY,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getProviderDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"History Data= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE hideLoadingView];
                     
                     arrHistory=[response valueForKey:@"requests"];
                     NSLog(@"History count = %lu",(unsigned long)arrHistory.count);
                     if (arrHistory.count==0 || arrHistory==nil)
                     {
                         self.tableView.hidden=YES;
                         self.lblnoHistory.hidden=NO;
                         self.imgNoDisplay.hidden=NO;
                     }
                     else
                     {
                         self.tableView.hidden=NO;
                         self.lblnoHistory.hidden=YES;
                         self.imgNoDisplay.hidden=YES;
                         [self makeSection];
                         [self.tableView reloadData];
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - Table view data source

-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrHistory];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO
                                                                            selector:@selector(localizedStandardCompare:)];
    
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
        
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return arrForSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[arrForSection objectAtIndex:section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)];
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 300, 15)];
    NSString *strDate=[arrForDate objectAtIndex:section];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
    
    if([strDate isEqualToString:current])
    {
        lblDate.text=@"Today";
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        lblDate.text=@"Yesterday";
    }
    else
    {
        NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
        NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];//2nd Jan 2015
        lblDate.text=text;
    }
    
    lblDate.textColor=[UIColor blackColor];
    headerView.backgroundColor=[UIColor lightGrayColor];
    [headerView addSubview:lblDate];
    return headerView;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arrForDate objectAtIndex:section];
}*/

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIImageView *imgFooter=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"history_footer"]];
    return imgFooter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"historycell";
    
    HistoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    if([strForUserType isEqualToString:@"1"])
    {
        NSMutableDictionary *dictOwner=[pastDict valueForKey:@"walker"];
        
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"first_name"],[dictOwner valueForKey:@"last_name"]];
        cell.lblType.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
        cell.lblPrice.text=[NSString stringWithFormat:@"%@ %.2f",[pastDict valueForKey:@"currency"],[[pastDict valueForKey:@"total"] floatValue]];
        cell.lblDistance.text=[NSString stringWithFormat:@"%@ %@",[pastDict valueForKey:@"unit"],[pastDict valueForKey:@"distance"]];
        
        NSDate *dateTemp=[[UtilityClass sharedObject]stringToDate:[pastDict valueForKey:@"date"]];
        NSString *strDate=[[UtilityClass sharedObject]DateToString:dateTemp withFormate:@"hh:mm a"];
        
        cell.lblTime.text=[NSString stringWithFormat:@"%@",strDate];
        [cell.imageView downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
        return cell;
    }
    else
    {
        NSMutableDictionary *dictOwner=[pastDict valueForKey:@"owner"];
        
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"first_name"],[dictOwner valueForKey:@"last_name"]];
        cell.lblType.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
        cell.lblPrice.text=[NSString stringWithFormat:@"%@ %.2f",[pastDict valueForKey:@"currency"],[[pastDict valueForKey:@"total"] floatValue]];
        cell.lblDistance.text=[NSString stringWithFormat:@"%@ %@",[pastDict valueForKey:@"unit"],[pastDict valueForKey:@"distance"]];
        
        NSDate *dateTemp=[[UtilityClass sharedObject]stringToDate:[pastDict valueForKey:@"date"]];
        NSString *strDate=[[UtilityClass sharedObject]DateToString:dateTemp withFormate:@"hh:mm a"];
        
        cell.lblTime.text=[NSString stringWithFormat:@"%@",strDate];
        [cell.imageView downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Action Methods
        
- (IBAction)closeBtnPressed:(id)sender
{
    self.navigationController.navigationBarHidden=NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.viewForBill.hidden=YES;
    } completion:^(BOOL finished)
     {
     }];
}

- (IBAction)onClickNavigationBtn:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onClickRideGiven:(id)sender
{
    [self.tableView reloadData];
}
- (IBAction)onClickRideTook:(id)sender
{
    [self.tableView reloadData];
}
- (IBAction)onClickrideGivenTook:(id)sender
{
    [self.tableView reloadData];
}

@end