//
//  RegisterVC.h
//  Uber
//
//  Created by Elluminati - macbook on 23/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "BaseVC.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <linkedin-sdk/LISDK.h>

@interface RegisterVC : BaseVC<UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, retain) GTMOAuth2Authentication *auth;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtOtp;
@property (weak, nonatomic) IBOutlet UITextField *txtOfficeEmailId;

@property (weak, nonatomic) IBOutlet UIView *viewForLogin;

@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrFb;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrgoogle;

@property (weak, nonatomic) IBOutlet UILabel *lblLikeTo;
@property (weak, nonatomic) IBOutlet UILabel *lblCar;
@property (weak, nonatomic) IBOutlet UILabel *lblBike;
@property (weak, nonatomic) IBOutlet UILabel *lblCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblGuidline;
@property (weak, nonatomic) IBOutlet UILabel *lblRead;

@property (weak, nonatomic) IBOutlet UIButton *btnRideTaker;
@property (weak, nonatomic) IBOutlet UIButton *btnRideGiver;
@property (weak, nonatomic) IBOutlet UIButton *btnRiderBoth;
@property (weak, nonatomic) IBOutlet UIButton *btnNav_Register;
@property (weak, nonatomic) IBOutlet UIButton *btnGuid;
@property (weak, nonatomic) IBOutlet UIButton *btnBike;
@property (weak, nonatomic) IBOutlet UIButton *btnCar;
@property (weak, nonatomic) IBOutlet UIButton *btnGuidLines;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btngoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnLinkedIn;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;

- (IBAction)onClickRadioButton:(id)sender;
- (IBAction)onClickGuid:(id)sender;
- (IBAction)onClickbtnCar:(id)sender;
- (IBAction)onClickbtnBike:(id)sender;
- (IBAction)fbbtnPressed:(id)sender;
- (IBAction)linkedInBtnPressed:(id)sender;
- (IBAction)onClickLogin:(id)sender;
- (IBAction)googleBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
- (IBAction)onClickNav_SignUp:(id)sender;
- (IBAction)checkBoxBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)onClickGuidlines:(id)sender;
- (IBAction)onClickRideRadioBtn:(id)sender;

@end