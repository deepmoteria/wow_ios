//
//  TermsVC.m
//  SG Taxi
//
//  Created by My Mac on 12/5/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "TermsVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UtilityClass.h"

@interface TermsVC ()

@end

@implementation TermsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    [super setBackBarItem];
    [self.viewForEmail setHidden:YES];
    
    NSURL *websiteUrl = [NSURL URLWithString:PRIVACY_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webViewTerms loadRequest:urlRequest];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureTerms:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.viewForEmail addGestureRecognizer:singleTapGestureRecognizer];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btnTerm setTitle:NSLocalizedString(@"Terms & Conditions", nil) forState:UIControlStateNormal];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  tap gesture

-(void)handleSingleTapGestureTerms:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.viewForEmail setHidden:YES];
    [self.view endEditing:YES];
}

#pragma mark -  action methods

- (IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickDownload:(id)sender
{
    NSString *fileURL = [NSString stringWithFormat:@"%@",PRIVACY_URL];
    NSURL  *url = [NSURL URLWithString:fileURL];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if (urlData)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"filename.text"];
        [urlData writeToFile:filePath atomically:YES];
        [APPDELEGATE showToastMessage:@"File has been downloaded successfully"];
    }
    else
    {
        [APPDELEGATE showToastMessage:@"Something wrong with download. Try again"];
    }
}

- (IBAction)onClickEmail:(id)sender
{
    [self.viewForEmail setHidden:NO];
}

- (IBAction)onClickSendEmail:(id)sender
{
    if(self.txtEmail.text.length<1)
    {
        [APPDELEGATE showToastMessage:@"Please enter email id first"];
    }
    else
    {
        if([APPDELEGATE connected])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait......"];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:self.txtEmail.text forKey:@"email"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_SUBMIT_EMAIL withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 NSLog(@"Response %@",response);
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [self.viewForEmail setHidden:YES];
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                 }
             }];
        }
    }
}

#pragma mark - text field delegate method

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end