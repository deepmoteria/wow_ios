//
//  LoginVC.m
//  Uber
//
//  Created by Elluminati - macbook on 21/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "LoginVC.h"
#import "FacebookUtility.h"
#import <GooglePlus/GooglePlus.h>
#import "AppDelegate.h"
#import "GooglePlusUtility.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "UtilityClass.h"
#import "UberStyleGuide.h"
#import <GoogleOpenSource/GoogleOpenSource.h>

static NSString *const kKeychainItemName = @"Google OAuth2 For gglplustest";
static NSString *const kClientID = @"807626072017-1l9d73dqf38iar2d4i9e00igbc0j6b9b.apps.googleusercontent.com";
static NSString *const kClientSecret = @"aUerqYEidSMauUa1hCPVUi9A";

@interface LoginVC ()
{
    NSString *strForSocialId,*strLoginType,*strForEmail;
    AppDelegate *appDelegate;
    UtilityClass *utilClass;
    int reTrive;
}

@end

@implementation LoginVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
    strLoginType=@"manual";
    reTrive=0;
    self.btnSignUp=[APPDELEGATE setBoldFontDiscriptor:self.btnSignUp];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[FacebookUtility sharedObject] logOutFromFacebook];
    [[GPPSignIn sharedInstance] signOut];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.btnSignUp setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
}

-(void)setLocalization
{
    [self.btnSignUp setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
}

#pragma mark - Linked In Integration

- (IBAction)onClickLinkedIn:(id)sender
{
    strLoginType=@"linkedin";
    [self updateControlsWithResponseLabel:NO];
    NSLog(@"linked in pressed");
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil]state:nil showGoToAppStoreDialog:YES
                                  successBlock:^(NSString *returnState)
     {
         NSLog(@"%s","success called!");
         LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
         NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
         NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
         [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
         NSLog(@"Response label text %@",text);
         [self updateControlsWithResponseLabel:YES];
     }
                                    errorBlock:^(NSError *error)
     {
         NSLog(@"%s %@","error called! ", [error description]);
         [self updateControlsWithResponseLabel:YES];
     }
     ];
}

- (void)updateControlsWithResponseLabel:(BOOL)updateResponseLabel
{
    NSString *url = @"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,phonetic-last-name,location:(country:(code)),industry,distance,current-status,current-share,network,skills,phone-numbers,date-of-birth,main-address,positions:(title),educations:(school-name,field-of-study,start-date,end-date,degree,activities))";
    
    if ([LISDKSessionManager hasValidSession])
    {
        [[LISDKAPIHelper sharedInstance] getRequest:url
                                            success:^(LISDKAPIResponse *response)
         {
             NSLog(@"%@",response.data);
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Linked in Data" message:[NSString stringWithFormat:@"%@",response.data] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alert show];
         }
                                              error:^(LISDKAPIError *apiError)
         {
             
         }];
    }
    else
    {
        NSError *error;
        if (error!= nil)
        {
            NSLog(@"%@",error);
        }
        else
        {
            
        }
    }
}

#pragma mark - google plus Integration

- (IBAction)onClickGooglePlus:(id)sender
{
    NSString *scope = kGTLAuthScopePlusLogin;
    GTMOAuth2Authentication *authLogin = [GTMOAuth2ViewControllerTouch
                                      authForGoogleFromKeychainForName:kKeychainItemName
                                      clientID:kClientID
                                      clientSecret:kClientSecret];
    GTMOAuth2ViewControllerTouch *authLoginController;
    authLoginController = [[GTMOAuth2ViewControllerTouch alloc]
                      initWithScope:scope
                      clientID:kClientID
                      clientSecret:kClientSecret
                      keychainItemName:kClientSecret
                      delegate:self
                      finishedSelector:@selector(LoginviewController:finishedWithAuth:error:)];
    [[self navigationController] pushViewController:authLoginController animated:YES];
    [authLogin beginTokenFetchWithDelegate:self didFinishSelector:@selector(authLogin:finishedRefreshWithFetcher:error:)];
}

- (void)authLogin:(GTMOAuth2Authentication *)authLogin finishedRefreshWithFetcher:(GTMHTTPFetcher *)fetcher error:(NSError *)error
{
    [self LoginviewController:nil finishedWithAuth:authLogin error:error];
    if (error != nil)
    {
        NSLog(@"self .auth :%@",self.authForLogin);
        
        NSLog(@"Authentication Error %@", error.localizedDescription);
        
        self.authForLogin=nil;
        return;
    }
    self.authForLogin=authLogin;
}

- (void)LoginviewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)authLog error:(NSError *)error
{
    if (error != nil)
    {
        //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        NSLog(@"Authentication Error %@", error.localizedDescription);
        self.authForLogin=nil;
        [[GPPSignIn sharedInstance] signOut];
        [[GPPSignIn sharedInstance] disconnect];
        return;
    }
    else
    {
        self.authForLogin=authLog;
        authLog.shouldAuthorizeAllRequests = YES;
        NSLog(@"login in");
        [self ForLoginRetrive];
    }
}

-(void)ForLoginRetrive
{
    GTLServicePlus *plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:self.authForLogin];//!!!here use our authentication object!!!
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error)
     {
         if (error)
         {
             GTMLoggerError(@"Error: %@", error);
         }
         else
         {
             reTrive++;
             NSString *description = [NSString stringWithFormat:
                                      @"%@\n%@\n%@\n%@ Birthdate :%@ %@ %@", person.displayName,
                                      person.aboutMe,person.emails,person.birthday,person.image,person.name,person.gender];
             NSLog(@"response :%@",description);
             NSDictionary *dict=person.JSON;
             NSLog(@"Dict :%@",[dict valueForKey:@"emails"]);
             
             NSMutableArray *arr=[[NSMutableArray alloc]init];
             arr=[dict valueForKey:@"emails"];
             NSDictionary *dictMain=[arr objectAtIndex:0];
             strForSocialId=[dict valueForKey:@"id"];
             strLoginType = @"google";
             NSLog(@"array  :%@",[dictMain valueForKey:@"value"]);
             strForEmail = [dictMain valueForKey:@"value"];
             NSLog(@"log for self auth :%@",self.authForLogin);
             //[self.imgProPic downloadFromURL:[NSString stringWithFormat:@"%@",person.image.url] withPlaceholder:nil];
             NSLog(@"new image :%@",person.image.url);
             if(reTrive==2)
             {
                 [appDelegate showLoadingWithTitle:NSLocalizedString(@"ALREADY_LOGIN", nil)];
                 [self onClickLogin:nil];
             }
         }
     }];
}

#pragma mark - facebook Integration

- (IBAction)onClickFacebook:(id)sender
{
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
    
    strLoginType=@"facebook";
    
    if (![[FacebookUtility sharedObject]isLogin])
    {
        [[FacebookUtility sharedObject]loginInFacebook:^(BOOL success, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (success)
             {
                 NSLog(@"Success");
                 appDelegate = [UIApplication sharedApplication].delegate;
                 [appDelegate userLoggedIn];
                 [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         NSLog(@"fb responce = %@",response);
                         strForSocialId=[response valueForKey:@"id"];
                         strForEmail=[response valueForKey:@"email"];
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         [self onClickLogin:nil];
                     }
                 }];
             }
         }];
    }
    else
    {
        NSLog(@"User Login Click");
        appDelegate = [UIApplication sharedApplication].delegate;
        [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error)
        {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                NSLog(@"fb responce = %@",response);
                strForSocialId=[response valueForKey:@"id"];
                strForEmail=[response valueForKey:@"email"];
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                [self onClickLogin:nil];
            }
        }];
        [appDelegate userLoggedIn];
    }
}

#pragma mark - action methods

- (IBAction)onClickSignInUsing:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onClickLogin:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strDeviceId=[pref objectForKey:PREF_DEVICE_TOKEN];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:@"ios" forKey:PARAM_DEVICE_TYPE];
        [dictParam setValue:strDeviceId forKey:PARAM_DEVICE_TOKEN];
        
        /*if([strLoginType isEqualToString:@"manual"])
            [dictParam setValue:strForEmail forKey:PARAM_EMAIL];
        else
            [dictParam setValue:strForEmail forKey:PARAM_EMAIL];*/
        
        [dictParam setValue:strLoginType forKey:PARAM_LOGIN_BY];
        
        if([strLoginType isEqualToString:@"facebook"])
            [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
        else if ([strLoginType isEqualToString:@"google"])
            [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
        /*else
            [dictParam setValue:self.txtPsw.text forKey:PARAM_PASSWORD];*/
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_LOGIN withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             
             NSLog(@"Login Response ---> %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSString *strLog=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"LOGIN_SUCCESS", nil),[response valueForKey:@"first_name"]];
                     [APPDELEGATE showToastMessage:strLog];
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                     [pref setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                     [pref setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                     [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                     [pref setValue:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                     [pref setBool:YES forKey:PREF_IS_LOGIN];
                     [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                     [pref setValue:[response valueForKey:@"user_type"] forKey:PREF_USER_TYPE];
                     [pref synchronize];
                     [self performSegueWithIdentifier:SEGUE_SUCCESS_LOGIN sender:self];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
