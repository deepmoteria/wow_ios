//
//  GuidLinesVc.h
//  Wow Client
//
//  Created by My Mac on 10/24/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface GuidLinesVc : BaseVC

@property (weak, nonatomic) IBOutlet UIWebView *webViewForGuid;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onclickOk:(id)sender;
@end
