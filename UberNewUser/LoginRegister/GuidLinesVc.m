//
//  GuidLinesVc.m
//  Wow Client
//
//  Created by My Mac on 10/24/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "GuidLinesVc.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface GuidLinesVc ()

@end

@implementation GuidLinesVc

- (void)viewDidLoad
{
    [super viewDidLoad];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    [super setBackBarItem];
    
    NSURL *websiteUrl = [NSURL URLWithString:GUIDLINE_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webViewForGuid loadRequest:urlRequest];
}

-(void)viewDidAppear:(BOOL)animated
{
    [APPDELEGATE hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE hideLoadingView];
}

#pragma action method

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onclickOk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end