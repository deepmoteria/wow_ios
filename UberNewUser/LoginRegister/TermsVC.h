//
//  TermsVC.h
//  SG Taxi
//
//  Created by My Mac on 12/5/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface TermsVC : BaseVC <UIWebViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webViewTerms;

@property (weak, nonatomic) IBOutlet UIView *viewForEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnTerm;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)onClickDownload:(id)sender;
- (IBAction)onClickEmail:(id)sender;
- (IBAction)onClickSendEmail:(id)sender;

@end
