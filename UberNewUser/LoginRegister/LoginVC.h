//
//  LoginVC.h
//  Uber
//
//  Created by Elluminati - macbook on 21/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "BaseVC.h"
#import "Reachability.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <linkedin-sdk/LISDK.h>

@interface LoginVC : BaseVC<UITextFieldDelegate,UIGestureRecognizerDelegate>


@property NetworkStatus internetConnectionStatus;
@property (nonatomic, retain) GTMOAuth2Authentication *authForLogin;

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

- (IBAction)onClickLinkedIn:(id)sender;
- (IBAction)onClickGooglePlus:(id)sender;
- (IBAction)onClickFacebook:(id)sender;
- (IBAction)onClickSignInUsing:(id)sender;

@end
