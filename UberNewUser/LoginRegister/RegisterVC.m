//
//  RegisterVC.m
//  Uber
//
//  Created by Elluminati - macbook on 23/06/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "RegisterVC.h"
#import "MyThingsVC.h"
#import "FacebookUtility.h"
#import <GooglePlus/GooglePlus.h>
#import "AppDelegate.h"
#import "GooglePlusUtility.h"
#import "UIImageView+Download.h"
#import "AFNHelper.h"
#import "Base64.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVBase.h>
#import <AVFoundation/AVFoundation.h>
#import "UtilityClass.h"
#import "MyThingsVC.h"
#import "Constants.h"
#import "UIView+Utils.h"
#import "UberStyleGuide.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "Constants.h"

static NSString *const kKeychainItemName = @"Google OAuth2 For gglplustest";
static NSString *const kClientID = @"807626072017-1l9d73dqf38iar2d4i9e00igbc0j6b9b.apps.googleusercontent.com";
static NSString *const kClientSecret = @"aUerqYEidSMauUa1hCPVUi9A";

@interface RegisterVC ()

{
    AppDelegate *appDelegate;
    NSMutableArray *arrForCountry,*arrForSubTypeId;
    NSString *strImageData,*strForRegistrationType,*strForSocialId,*strForToken,*strForID,*strService,*strGender,*strForRide,*strAvailId;
    BOOL isPicAdded,igGoogleLogin,isframe;
    GPPSignIn *signIn;
}

@end

@implementation RegisterVC

@synthesize btnMale,btnFemale,btnRideGiver,btnRideTaker,btnRiderBoth,btnCar,btnBike;

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setNavBarTitle:TITLE_REGISTER];
    [self SetLocalization];
    [self getSubType];
    arrForCountry=[[NSMutableArray alloc]init];
    arrForSubTypeId=[[NSMutableArray alloc]init];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 500)];
    appDelegate=[AppDelegate sharedAppDelegate];
    igGoogleLogin=0;
    isframe=0;
    [self customFont];
    [self.scrollView setHidden:NO];
    [self.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"cb_glossy_off.png"] forState:UIControlStateNormal];
    [self.imgProPic applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    self.btnRegister.hidden=YES;
    self.btnRegister.enabled=FALSE;
    isPicAdded=NO;
    
    btnMale.tag=1;
    btnFemale.tag=2;
    btnRideTaker.tag=3;
    btnRideGiver.tag=4;
    btnRiderBoth.tag=5;
    btnCar.tag=6;
    btnBike.tag=7;
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureRegister:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.scrollView addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[FacebookUtility sharedObject] logOutFromFacebook];
    [[GPPSignIn sharedInstance] signOut];
    [[GPPSignIn sharedInstance] disconnect];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.btnNav_Register setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
}

-(void)SetLocalization
{
    self.txtFirstName.placeholder=NSLocalizedString(@"FIRST NAME*", nil);
    self.txtLastName.placeholder=NSLocalizedString(@"LAST NAME*", nil);
    self.txtEmail.placeholder=NSLocalizedString(@"EMAIL*", nil);
    self.txtNumber.placeholder=NSLocalizedString(@"NUMBER*", nil);
    self.txtOtp.placeholder=NSLocalizedString(@"OTP*",nil);
    self.txtOfficeEmailId.placeholder=NSLocalizedString(@"OFFICE_MAIL*",nil);
    [self.btnTerm setTitle:NSLocalizedString(@"I agree to the terms and conditions", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"Ready to WOW !", nil) forState:UIControlStateNormal];
}

#pragma mark - custom methods

-(void)getSubType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getOtp
{
    [APPDELEGATE showLoadingWithTitle:@"Please Wait while getting Otp"];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *number=[NSString stringWithFormat:@"+91%@",[pref objectForKey:PARAM_PHONE]];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:number forKey:@"mobile_number"];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_OTP withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Get otp= %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }

}

#pragma mark - hide show methods

-(void)showRegisterSerive
{
    self.lblLikeTo.hidden=NO;
    btnCar.hidden=NO;
    btnBike.hidden=NO;
    self.lblCar.hidden=NO;
    self.lblBike.hidden=NO;
}

-(void)hideRegisterSerive
{
    self.lblLikeTo.hidden=YES;
    btnCar.hidden=YES;
    btnBike.hidden=YES;
    self.lblCar.hidden=YES;
    self.lblBike.hidden=YES;
}

#pragma mark - set frames

-(void)setFrameRideTaken
{
    if(isframe==0)
    {
        isframe=1;
        self.btnTerm.frame = CGRectMake(self.btnTerm.frame.origin.x, self.btnTerm.frame.origin.y-40,self.btnTerm.frame.size.width, self.btnTerm.frame.size.height);
        
        self.btnCheckBox.frame = CGRectMake(self.btnCheckBox.frame.origin.x, self.btnCheckBox.frame.origin.y-40,self.btnCheckBox.frame.size.width, self.btnCheckBox.frame.size.height);
        
        self.lblCondition.frame = CGRectMake(self.lblCondition.frame.origin.x, self.lblCondition.frame.origin.y-40,self.lblCondition.frame.size.width, self.lblCondition.frame.size.height);
        
        self.lblGuidline.frame = CGRectMake(self.lblGuidline.frame.origin.x, self.lblGuidline.frame.origin.y-40,self.lblGuidline.frame.size.width, self.lblGuidline.frame.size.height);
        
        self.btnGuid.frame = CGRectMake(self.btnGuid.frame.origin.x, self.btnGuid.frame.origin.y-40,self.btnGuid.frame.size.width, self.btnGuid.frame.size.height);
        
        self.btnGuidLines.frame = CGRectMake(self.btnGuidLines.frame.origin.x, self.btnGuidLines.frame.origin.y-40,self.btnGuidLines.frame.size.width, self.btnGuidLines.frame.size.height);
        
        self.lblRead.frame = CGRectMake(self.lblRead.frame.origin.x, self.lblRead.frame.origin.y-40,self.lblRead.frame.size.width, self.lblRead.frame.size.height);
        
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.lblRead.frame.size.height+self.lblRead.frame.origin.y+15)];
    }
}

-(void)setOtherFrames
{
    if(isframe==1)
    {
        isframe=0;
        self.btnTerm.frame = CGRectMake(self.btnTerm.frame.origin.x, self.btnTerm.frame.origin.y+40,self.btnTerm.frame.size.width, self.btnTerm.frame.size.height);
        
        self.btnCheckBox.frame = CGRectMake(self.btnCheckBox.frame.origin.x, self.btnCheckBox.frame.origin.y+40,self.btnCheckBox.frame.size.width, self.btnCheckBox.frame.size.height);
        
        self.lblCondition.frame = CGRectMake(self.lblCondition.frame.origin.x, self.lblCondition.frame.origin.y+40,self.lblCondition.frame.size.width, self.lblCondition.frame.size.height);
        
        self.lblGuidline.frame = CGRectMake(self.lblGuidline.frame.origin.x, self.lblGuidline.frame.origin.y+40,self.lblGuidline.frame.size.width, self.lblGuidline.frame.size.height);
        
        self.btnGuid.frame = CGRectMake(self.btnGuid.frame.origin.x, self.btnGuid.frame.origin.y+40,self.btnGuid.frame.size.width, self.btnGuid.frame.size.height);
        
        self.btnGuidLines.frame = CGRectMake(self.btnGuidLines.frame.origin.x, self.btnGuidLines.frame.origin.y+40,self.btnGuidLines.frame.size.width, self.btnGuidLines.frame.size.height);
        
        self.lblRead.frame = CGRectMake(self.lblRead.frame.origin.x, self.lblRead.frame.origin.y+40,self.lblRead.frame.size.width, self.lblRead.frame.size.height);
        
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.lblRead.frame.size.height+self.lblRead.frame.origin.y+15)];
    }
}


#pragma mark-
#pragma mark- Custom Font & Localization

-(void)customFont
{
    self.txtFirstName.font=[UberStyleGuide fontRegular];
    self.txtLastName.font=[UberStyleGuide fontRegular];
    self.txtEmail.font=[UberStyleGuide fontRegular];
    self.txtOtp.font=[UberStyleGuide fontRegular];
    self.txtOfficeEmailId.font=[UberStyleGuide fontRegular];
    self.txtNumber.font=[UberStyleGuide fontRegular];
    self.btnNav_Register=[APPDELEGATE setBoldFontDiscriptor:self.btnNav_Register];
    self.btnRegister=[APPDELEGATE setBoldFontDiscriptor:self.btnRegister];
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UITextField Delegate

-(void)handleSingleTapGestureRegister:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtEmail resignFirstResponder];
    [self.txtOfficeEmailId resignFirstResponder];
    [self.txtNumber resignFirstResponder];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtEmail resignFirstResponder];
    [self.txtOfficeEmailId resignFirstResponder];
    [self.txtNumber resignFirstResponder];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.txtNumber)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self.txtOtp)
    {
        offset=CGPointMake(0, 20);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    if(textField==self.txtOfficeEmailId)
    {
        offset=CGPointMake(0, 40);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    /*if(textField==self.txtEmail)
    {
        offset=CGPointMake(0, 110);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    else if(textField==self.txtNumber)
    {
        offset=CGPointMake(0, 150);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    else if(textField==self.txtOfficeEmailId)
    {
        offset=CGPointMake(0, 180);
        [self.scrollView setContentOffset:offset animated:YES];
    }*/
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.scrollView setContentOffset:offset animated:YES];
    
    if(textField==self.txtFirstName)
        [self.txtLastName becomeFirstResponder];
    else if(textField==self.txtLastName)
        [self.txtEmail becomeFirstResponder];
    else if(textField==self.txtEmail)
        [self.txtNumber becomeFirstResponder];
    else if(textField==self.txtNumber)
    {
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        [pref setValue:self.txtNumber.text forKey:PARAM_PHONE];
        [pref synchronize];
        [textField becomeFirstResponder];
        [self getOtp];
    }
    else if (textField==self.txtOtp)
        [self.txtOfficeEmailId becomeFirstResponder];
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - facebook integration

- (IBAction)fbbtnPressed:(id)sender
{
    strForRegistrationType=@"facebook";
    
    if (![[FacebookUtility sharedObject]isLogin])
    {
        [[FacebookUtility sharedObject]loginInFacebook:^(BOOL success, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (success)
             {
                 [self.scrollView setHidden:NO];
                 [self.viewForLogin setHidden:YES];
                 self.btnRegister.hidden=NO;
                 NSLog(@"Success");
                 appDelegate = [UIApplication sharedApplication].delegate;
                 [appDelegate userLoggedIn];
                 [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         NSLog(@"FB Response ->%@",response);
                         strForSocialId=[response valueForKey:@"id"];
                         self.txtEmail.text=[response valueForKey:@"email"];
                         NSArray *arr=[[response valueForKey:@"name"] componentsSeparatedByString:@" "];
                         self.txtFirstName.text=[arr objectAtIndex:0];
                         self.txtLastName.text=[arr objectAtIndex:1];
                         
                         NSString *strForGender = [response valueForKey:@"gender"];
                         
                         if([strForGender isEqualToString:@"male"])
                         {
                             [self.btnMale setSelected:YES];
                             strGender=@"male";
                         }
                         else
                         {
                             [self.btnFemale setSelected:YES];
                             strGender=@"female";
                         }
                         
                         [self.imgProPic downloadFromURL:[response valueForKey:@"link"] withPlaceholder:nil];
                         NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[response objectForKey:@"id"]];
                         [self.imgProPic downloadFromURL:userImageURL withPlaceholder:[UIImage imageNamed:@"user.png"]];
                         if(self.imgProPic.image==[UIImage imageNamed:@"nil"])
                             isPicAdded=NO;
                         else
                             isPicAdded=YES;
                    }
                 }];
             }
         }];
    }
    else
    {
        [self.scrollView setHidden:NO];
        [self.viewForLogin setHidden:YES];
        self.btnRegister.hidden=NO;
        NSLog(@"User Login Click");
        appDelegate = [UIApplication sharedApplication].delegate;
        [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error)
        {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                NSLog(@"FB Response ->%@ ",response);
                strForSocialId=[response valueForKey:@"id"];
                self.txtEmail.text=[response valueForKey:@"email"];
                NSArray *arr=[[response valueForKey:@"name"] componentsSeparatedByString:@" "];
                self.txtFirstName.text=[arr objectAtIndex:0];
                self.txtLastName.text=[arr objectAtIndex:1];
                NSString *strForGender = [response valueForKey:@"gender"];
                
                if([strForGender isEqualToString:@"male"])
                {
                   [self.btnMale setSelected:YES];
                    strGender=@"male";
                }
                else
                {
                    [self.btnFemale setSelected:NO];
                    strGender=@"female";
                }
                
                [self.imgProPic downloadFromURL:[response valueForKey:@"link"] withPlaceholder:nil];
                NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [response objectForKey:@"id"]];
                [self.imgProPic downloadFromURL:userImageURL withPlaceholder:nil];
                isPicAdded=YES;
            }
        }];
        [appDelegate userLoggedIn];
    }
}

#pragma mark - 
#pragma mark - google plus integration

- (IBAction)googleBtnPressed:(id)sender
{
    strForRegistrationType=@"google";
    
    NSString *scope = kGTLAuthScopePlusLogin;
    GTMOAuth2Authentication * auth = [GTMOAuth2ViewControllerTouch
                                      authForGoogleFromKeychainForName:kKeychainItemName
                                      clientID:kClientID
                                      clientSecret:kClientSecret];
    GTMOAuth2ViewControllerTouch *authController;
    authController = [[GTMOAuth2ViewControllerTouch alloc]
                      initWithScope:scope
                      clientID:kClientID
                      clientSecret:kClientSecret
                      keychainItemName:kKeychainItemName
                      delegate:self
                      finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    [[self navigationController] pushViewController:authController animated:YES];
    [auth beginTokenFetchWithDelegate:self didFinishSelector:@selector(auth:finishedRefreshWithFetcher:error:)];
}

- (void)auth:(GTMOAuth2Authentication *)auth finishedRefreshWithFetcher:(GTMHTTPFetcher *)fetcher error:(NSError *)error
{
    [self viewController:nil finishedWithAuth:auth error:error];
    if (error != nil)
    {
        NSLog(@"self .auth :%@",self.auth);
        
        NSLog(@"Authentication Error %@", error.localizedDescription);
        
        self.auth=nil;
        return;
    }
    self.auth=auth;
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
    if (error != nil)
    {
        //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        NSLog(@"Authentication Error %@", error.localizedDescription);
        self.auth=nil;
        [self.scrollView setHidden:YES];
        [self.viewForLogin setHidden:NO];
        [[GPPSignIn sharedInstance] signOut];
        [[GPPSignIn sharedInstance] disconnect];
        return;
    }
    else
    {
        [self.scrollView setHidden:NO];
        self.auth=auth;
        [self.viewForLogin setHidden:YES];
        self.btnRegister.hidden=NO;
        auth.shouldAuthorizeAllRequests = YES;
        NSLog(@"login in");
        [self ForRetrive];
    }
}

-(void)ForRetrive
{
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:self.auth];
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error)
    {
        if (error)
        {
            GTMLoggerError(@"Error: %@", error);
        }
        else
        {
            igGoogleLogin=1;
            NSString *description = [NSString stringWithFormat:
                                     @"%@\n%@\n%@\n%@ Birthdate :%@ %@ %@", person.displayName,
                                     person.aboutMe,person.emails,person.birthday,person.image,person.name,person.gender];
            
            NSLog(@"response :%@",description);
            NSDictionary *dict=person.JSON;
            NSLog(@"Dict :%@",[dict valueForKey:@"emails"]);
            
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            arr=[dict valueForKey:@"emails"];
            NSDictionary *dictMain=[arr objectAtIndex:0];
            strForSocialId=[dict valueForKey:@"id"];
            NSLog(@"array  :%@",[dictMain valueForKey:@"value"]);
            self.txtEmail.text=[dictMain valueForKey:@"value"];
            self.txtFirstName.text=[[dict valueForKey:@"name"] valueForKey:@"givenName"];
            self.txtLastName.text=[[dict valueForKey:@"name"] valueForKey:@"familyName"];
            
            if([[dict valueForKey:@"gender"]isEqualToString:@"male"])
            {
                [self.btnMale setSelected:YES];
                strGender=@"male";
            }
            else
            {
                [self.btnFemale setSelected:YES];
                strGender=@"female";
            }
            
            NSLog(@"log for self auth :%@",self.auth);
            [self.imgProPic downloadFromURL:[NSString stringWithFormat:@"%@",person.image.url] withPlaceholder:nil];
            NSLog(@"new image :%@",person.image.url);
        }
    }];
}

#pragma mark -
#pragma mark - Linkedin integration

- (IBAction)linkedInBtnPressed:(id)sender;
{
    [self updateControlsWithResponseLabel:NO];
    NSLog(@"linked in pressed");
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil]state:nil showGoToAppStoreDialog:YES
                                  successBlock:^(NSString *returnState)
    {
        NSLog(@"%s","success called!");
        LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
        NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
        NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
        [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
        NSLog(@"Response label text %@",text);
        [self updateControlsWithResponseLabel:YES];
    }
    errorBlock:^(NSError *error)
    {
        NSLog(@"%s %@","error called! ", [error description]);
        [self updateControlsWithResponseLabel:YES];
    }
    ];
}

- (void)updateControlsWithResponseLabel:(BOOL)updateResponseLabel
{
    NSString *url = @"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,phonetic-last-name,location:(country:(code)),industry,distance,current-status,current-share,network,skills,phone-numbers,date-of-birth,main-address,positions:(title),educations:(school-name,field-of-study,start-date,end-date,degree,activities))";
    
    if ([LISDKSessionManager hasValidSession])
    {
        [[LISDKAPIHelper sharedInstance] getRequest:url
                                            success:^(LISDKAPIResponse *response)
         {
             NSLog(@"%@",response.data);
         }
                                              error:^(LISDKAPIError *apiError)
         {
             
         }];
    }
    else
    {
        NSError *error;
        if (error!= nil)
        {
            NSLog(@"%@",error);
        }
        else
        {
            
        }
    }
}

#pragma mark
#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self selectPhotos];
        }
            break;
        case 1:
        {
            [self takePhoto];
        }
            break;
        default:
            break;
    }
}

#pragma mark
#pragma mark - Action to Share

- (void)selectPhotos
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

-(void)takePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing=YES;
        [self presentViewController:imagePickerController animated:YES completion:^{
    }];

    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }
}

#pragma mark
#pragma mark - ImagePickerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if([info valueForKey:UIImagePickerControllerEditedImage]==nil)
    {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:[info valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset)
        {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc(rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
            UIImage *img=[UIImage imageWithData:data];
            [self setImage:img];
        }
        failureBlock:^(NSError *err)
        {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
    else
    {
        [self setImage:[info valueForKey:UIImagePickerControllerEditedImage]];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)setImage:(UIImage *)image
{
    self.imgProPic.image=image;
    isPicAdded=YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark - Action Methods

- (IBAction)nextBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if(self.txtFirstName.text.length<1 || self.txtLastName.text.length<1 || self.txtEmail.text.length<1 || self.txtNumber.text.length<1 || self.txtOfficeEmailId.text.length<1 || [strGender isEqualToString:@""] || arrForSubTypeId.count<1)
        {
            if(self.txtFirstName.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_FIRST_NAME", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if(self.txtLastName.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_LAST_NAME", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if(self.txtEmail.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if(self.txtNumber.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_NUMBER", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if (self.txtOfficeEmailId.text.length<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_OFFICE_ID", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if ([strGender isEqualToString:@""])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_GENDER", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else if (arrForSubTypeId.count<1)
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_TYPE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            /*else if(isPicAdded==NO)
             {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please Select Profile Picture", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
             [alert show];
             }*/
        }
        else
        {
            if([[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
            {
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"Registering", nil)];
                
                strAvailId = @"";
                for (int i=0; i<arrForSubTypeId.count; i++)
                {
                    NSString *strTemp;
                    if (i!=(arrForSubTypeId.count-1))
                    {
                        strTemp=[NSString stringWithFormat:@"%@,",[arrForSubTypeId objectAtIndex:i]];
                    }
                    else
                    {
                        strTemp=[NSString stringWithFormat:@"%@",[arrForSubTypeId objectAtIndex:i]];
                    }
                    
                    strAvailId = [strAvailId stringByAppendingString:strTemp];
                }
                
                NSTimeZone *timeZone = [NSTimeZone localTimeZone];
                NSString *tzName = [timeZone name];
                
                NSString *strnumber=[NSString stringWithFormat:@"+91%@",self.txtNumber.text];
                NSString *strRide=[NSString stringWithFormat:@"%d",ride];
                
                NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                NSString *strDeviceId=[pref objectForKey:PREF_DEVICE_TOKEN];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setValue:self.txtEmail.text forKey:PARAM_EMAIL];
                [dictParam setValue:self.txtOfficeEmailId.text forKey:PARAM_OFFCIAL_EMAIL];
                [dictParam setValue:self.txtFirstName.text forKey:PARAM_FIRST_NAME];
                [dictParam setValue:self.txtLastName.text forKey:PARAM_LAST_NAME];
                [dictParam setValue:strnumber forKey:PARAM_PHONE];
                [dictParam setValue:self.txtOtp.text forKey:PARAM_OTP];
                [dictParam setValue:strDeviceId forKey:PARAM_DEVICE_TOKEN];
                [dictParam setValue:@"ios" forKey:PARAM_DEVICE_TYPE];
                [dictParam setValue:strAvailId forKey:PARAM_TYPE];
                [dictParam setValue:tzName forKey:PARAM_TIMEZONE];
                [dictParam setValue:strGender forKey:PARAM_GENDER];
                [dictParam setValue:strRide forKey:PARAM_RIDE];
                [dictParam setValue:strForRegistrationType forKey:PARAM_LOGIN_BY];
                
                if([strForRegistrationType isEqualToString:@"facebook"])
                    [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
                else if ([strForRegistrationType isEqualToString:@"google"])
                    [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
                /*else
                 [dictParam setValue:self.txtPassword.text forKey:PARAM_PASSWORD];*/
                
                if(isPicAdded==YES)
                {
                    UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.imgProPic.image];
                    
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_REGISTER withParamDataImage:dictParam andImage:imgUpload withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                             if([[response valueForKey:@"success"] boolValue])
                             {
                                 [APPDELEGATE showToastMessage:NSLocalizedString(@"REGISTER_SUCCESS", nil)];
                                 strForID=[response valueForKey:@"id"];
                                 strForToken=[response valueForKey:@"token"];
                                 [pref setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                                 [pref setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                                 [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                                 [pref setObject:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                                 [pref setBool:YES forKey:PREF_IS_LOGIN];
                                 [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                                 [pref setValue:[response valueForKey:@"user_type"] forKey:PREF_USER_TYPE];
                                 [pref synchronize];
                                 [self performSegueWithIdentifier:SEGUE_SUCCESS_LOGIN sender:self];
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                         NSLog(@"REGISTER RESPONSE --> %@",response);
                     }];
                    
                }
                else
                {
                    NSLog(@"not profile");
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_REGISTER withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                             if([[response valueForKey:@"success"] boolValue])
                             {
                                 [APPDELEGATE showToastMessage:NSLocalizedString(@"REGISTER_SUCCESS", nil)];
                                 strForID=[response valueForKey:@"id"];
                                 strForToken=[response valueForKey:@"token"];
                                 [pref setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                                 [pref setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                                 [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                                 [pref setBool:YES forKey:PREF_IS_LOGIN];
                                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
                                 [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                                 [pref synchronize];
                                 [self performSegueWithIdentifier:SEGUE_SUCCESS_LOGIN sender:self];
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                             
                         }
                         NSLog(@"REGISTER RESPONSE --> %@",response);
                     }];
                }
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (IBAction)checkBoxBtnPressed:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=1;
        [btn setSelected:YES];
        if(self.btnGuidLines.isSelected==YES)
            self.btnRegister.enabled=TRUE;
        else
            self.btnRegister.enabled=FALSE;
    }
    else
    {
        btn.tag=0;
        [btn setSelected:NO];
        self.btnRegister.enabled=FALSE;
    }
}

- (IBAction)termsBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"pushToTerms" sender:self];
}

- (IBAction)onClickRadioButton:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn==self.btnMale)
    {
        btnMale.selected=YES;
        btnFemale.selected=NO;
        strGender=@"male";
    }
    else if (btn==self.btnFemale)
    {
        btnMale.selected=NO;
        btnFemale.selected=YES;
        strGender=@"female";
    }
}

- (IBAction)onClickGuidlines:(id)sender
{
    [self performSegueWithIdentifier:@"pushToGuid" sender:self];
}
- (IBAction)onClickRideRadioBtn:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn==self.btnRideTaker)
    {
        btnRideGiver.selected=NO;
        btnRiderBoth.selected=NO;
        btnRideTaker.selected=YES;
        [self hideRegisterSerive];
        [self setFrameRideTaken];
        ride=0;
    }
    else if (btn==self.btnRideGiver)
    {
        btnRideGiver.selected=YES;
        btnRiderBoth.selected=NO;
        btnRideTaker.selected=NO;
        [self showRegisterSerive];
        [self setOtherFrames];
        ride=1;
    }
    else if (btn==self.btnRiderBoth)
    {
        btnRideGiver.selected=NO;
        btnRiderBoth.selected=YES;
        btnRideTaker.selected=NO;
        [self showRegisterSerive];
        [self setOtherFrames];
        ride=2;
    }
}

- (IBAction)onClickGuid:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=1;
        [btn setSelected:YES];
        if(self.btnCheckBox.isSelected==YES)
            self.btnRegister.enabled=TRUE;
        else
            self.btnRegister.enabled=FALSE;
        
    }
    else
    {
        btn.tag=0;
        [btn setSelected:NO];
        self.btnRegister.enabled=FALSE;
    }

}

- (IBAction)onClickbtnCar:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=1;
        //[btn setBackgroundImage:[UIImage imageNamed:@".png"] forState:UIControlStateNormal];
        [btn setSelected:NO];
        [arrForSubTypeId removeObject:@"1"];
    }
    else
    {
        btn.tag=0;
        //[btn setBackgroundImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        [btn setSelected:YES];
        [arrForSubTypeId addObject:@"1"];
    }
}

- (IBAction)onClickbtnBike:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=1;
        //[btn setBackgroundImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
        [btn setSelected:NO];
        [arrForSubTypeId removeObject:@"2"];
    }
    else
    {
        btn.tag=0;
        //[btn setBackgroundImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        [btn setSelected:YES];
        [arrForSubTypeId addObject:@"2"];
    }
}

- (IBAction)onClickLogin:(id)sender
{
    [self performSegueWithIdentifier:@"segueToLogin" sender:nil];
}

- (IBAction)onClickNav_SignUp:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end