#import "BaseVC.h"

@interface MyMilesVC : BaseVC<UITextFieldDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewMiles;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIView *viewForRideGiver;
@property (weak, nonatomic) IBOutlet UIView *viewForRideTaker;
@property (weak, nonatomic) IBOutlet UIView *viewForOrganization;

@property (weak, nonatomic) IBOutlet UIButton *btnChoose;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@property (weak, nonatomic) IBOutlet UILabel *lblCostContribution;
@property (weak, nonatomic) IBOutlet UILabel *lblPartOfOrga;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentageSign;
@property (weak, nonatomic) IBOutlet UILabel *lblMySmiles;

@property (weak, nonatomic) IBOutlet UITextField *txtSelectPercentage;
@property (weak, nonatomic) IBOutlet UITextField *txtCarNo;
@property (weak, nonatomic) IBOutlet UITextField *txtBikeNo;
@property (weak, nonatomic) IBOutlet UITextField *txtGoFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtGoTo;
@property (weak, nonatomic) IBOutlet UITextField *txtCommingFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtCommingTo;
@property (weak, nonatomic) IBOutlet UITextField *txtGoStartBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtCommingStartBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtOrganization;

@property (weak, nonatomic) IBOutlet UIImageView *imgCar;
@property (weak, nonatomic) IBOutlet UIImageView *imgBike;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarLine;
@property (weak, nonatomic) IBOutlet UIImageView *imgBikeLine;

@property (weak, nonatomic) IBOutlet UITableView *tableForOrganization;
@property (weak, nonatomic) IBOutlet UITableView *tableForCity;
@property (weak, nonatomic) IBOutlet UITableView *tableForOrgaList;

- (IBAction)onClickOk:(id)sender;
- (IBAction)onClickMenu:(id)sender;
- (IBAction)onClickCancelPicker:(id)sender;
- (IBAction)onClickDonePicker:(id)sender;
- (IBAction)onClickFeelGoodBtn:(id)sender;
- (IBAction)searchingCity:(id)sender;

@end