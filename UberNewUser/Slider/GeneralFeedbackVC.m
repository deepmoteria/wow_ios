//  GeneralFeedbackVC.m
//  Wow Client
//
//  Created by My Mac on 10/31/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "GeneralFeedbackVC.h"
#import "RatingBar.h"
@interface GeneralFeedbackVC ()

@end

@implementation GeneralFeedbackVC
{
    NSString *strForUserId,*strForUserToken,*strForUserType;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_GENERAL_FEEDBACK", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesturefeedback:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
    [self.ratingView initRateBar];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictIfo = [pref objectForKey:PREF_LOGIN_OBJECT];
    strForUserType = [NSString stringWithFormat:@"%@",[dictIfo valueForKey:@"user_type"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tap gesture

-(void)handleSingleTapGesturefeedback:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtFeedback resignFirstResponder];
}


#pragma mark - 
#pragma mark - Action methods

- (IBAction)onClickNavigationBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickSubmit:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait..."];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:@"user_id"];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strForUserType forKey:PARAM_RIDE];
        [dictParam setValue:self.txtFeedback.text forKey:@"comment"];
    
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GENERAL_FEEDBACK withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [[AppDelegate sharedAppDelegate]showToastMessage:@"Thank You For Feedback"];
                     [self.navigationController popToRootViewControllerAnimated:YES];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
             NSLog(@"FEEDBACK --> %@",response);
         }];
    }
    
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

#pragma mark
#pragma mark - textView delegates

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView==self.txtFeedback)
    {
        self.txtFeedback.text = @"";
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView==self.txtFeedback)
    {
        if(self.txtFeedback.text.length < 1)
        {
            self.txtFeedback.text = @"We are All Ears!!!";
        }
    }
}
@end
