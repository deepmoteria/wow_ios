//
//  InviteFriendsVC.m
//  Wow Client
//
//  Created by My Mac on 12/19/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "InviteFriendsVC.h"
#import "InviteFriendsCell.h"
#import "GooglePlusUtility.h"

@interface InviteFriendsVC ()
{
    NSMutableArray *arrForContacts,*arrForNumber;
}

@end

@implementation InviteFriendsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForContacts = [[NSMutableArray alloc]init];
    arrForNumber = [[NSMutableArray alloc]init];
    [self.tableForContacts setHidden:YES];
    [self.btnInviteSelected setHidden:YES];
    [self getAllContacts];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    NSString *strForLoginBy = [dictInfo valueForKey:@"login_by"];
    
    if([strForLoginBy isEqualToString:@"facebook"])
    {
        [self.btnSocialShare setTitle:@"Using Facebook" forState:UIControlStateNormal];
        self.btnSocialShare.tag=1;
    }
    else if ([strForLoginBy isEqualToString:@"google"])
    {
        [self.btnSocialShare setTitle:@"Using Google+" forState:UIControlStateNormal];
        self.btnSocialShare.tag=2;
    }
    else
    {
        [self.btnSocialShare setTitle:@"Using LinkedIn" forState:UIControlStateNormal];
        self.btnSocialShare.tag=3;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"INVITE", nil) forState:UIControlStateNormal];
}

#pragma mark - 
#pragma mark - custom methods

-(void)getAllContacts
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    __block BOOL accessGranted = NO;
    if (addressBook != NULL)
    {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     accessGranted = granted;
                                                     dispatch_semaphore_signal(sema);
                                                 });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else
        accessGranted = YES;
    
    if (accessGranted)
    {
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        
        //CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonPhoneProperty);
        
        //CFIndex nPeople =(CFIndex) ABAddressBookGetPersonCount(allPeople);
        
        CFIndex nPeople =(CFIndex) ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonPhoneProperty);
        
        for (int i=0; i<(CFIndex)allPeople;i++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            ABRecordRef person1 = CFArrayGetValueAtIndex((CFArrayRef)nPeople, i);
            
            if(person==person1)
            {
                ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
                
                NSString *firstName = [NSString stringWithFormat:@"%@",ABRecordCopyValue(person, kABPersonFirstNameProperty)];
                
                NSString *lastName = [NSString stringWithFormat:@"%@",ABRecordCopyValue(person, kABPersonLastNameProperty)];
                
                NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(phones, 0);
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                
                if(![firstName isEqualToString:@"(null)"] || [firstName isKindOfClass:[[NSNull null] class]])
                    [dict setValue:firstName forKey:@"firstname"];
                if(![lastName isEqualToString:@"(null)"] || [lastName isKindOfClass:[[NSNull null] class]])
                    [dict setValue:lastName forKey:@"lastname"];
                if(![phoneNumber isEqualToString:@"(null)"] || [phoneNumber isKindOfClass:[[NSNull null] class]])
                    [dict setValue:phoneNumber forKey:@"number"];
                
                [arrForContacts addObject:dict];
                [self.tableForContacts reloadData];
            }
            else
            {
                break;
            }
        }
    }
    else
    {
        #ifdef DEBUG
        NSLog(@"Cannot fetch Contacts");
        #endif
    }
}

#pragma mark-
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForContacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Invite";
    
    InviteFriendsCell *cell = [self.tableForContacts dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[InviteFriendsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrForContacts objectAtIndex:indexPath.row];
    NSString *strFirstName = [dict valueForKey:@"firstname"];
    NSString *strLastName = [dict valueForKey:@"lastname"];
    cell.imgeCheck.image = [UIImage imageNamed:@"uncheck_icon"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(strFirstName)
        cell.lblName.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"firstname"]];
    
    if(strLastName)
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"firstname"],[dict valueForKey:@"lastname"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrForContacts objectAtIndex:indexPath.row];
    
    InviteFriendsCell *Cell = (InviteFriendsCell*)[self.tableForContacts cellForRowAtIndexPath:indexPath];
    if([Cell.imgeCheck.image isEqual:[UIImage imageNamed:@"uncheck_icon"]])
    {
        [arrForNumber addObject:[dict valueForKey:@"number"]];
        Cell.imgeCheck.image=[UIImage imageNamed:@"check_icon"];
    }
    else
    {
        Cell.imgeCheck.image=[UIImage imageNamed:@"uncheck_icon"];
        [arrForNumber removeObject:[dict valueForKey:@"number"]];
    }
    
    NSLog(@"numbers = %@",arrForNumber);

}

#pragma mark -
#pragma mark - Action Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (&UIApplicationOpenSettingsURLString != NULL)
        {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
            //NSURL *url = [NSURL URLWithString:@"prefs:root=TWITTER"];
            //[[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            // Present some dialog telling the user to open the settings app.
        }
    }
}

- (IBAction)onClickConatact:(id)sender
{
    [self.tableForContacts setHidden:NO];
    [self.btnInviteSelected setHidden:NO];
}

- (IBAction)onClickSocialBtnPressed:(id)sender
{
    if(self.btnSocialShare.tag==1)
    {
        fb = [SLComposeViewController
              composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            fb=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            
            NSString *FBString=[NSString stringWithFormat:@"please check out this app."];
            
            [fb setInitialText:FBString];
            
            [self presentViewController:fb animated:YES completion:nil];
        }
        else
        {
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                mySLComposerSheet = [[SLComposeViewController alloc] init];
                
                mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                
                [mySLComposerSheet setInitialText:@"please check out this app."];
                
                [self presentViewController:mySLComposerSheet animated:YES completion:nil];
            
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                    
                [alert show];
            }
            
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
             {
                 NSString *output;
                 switch (result)
                 {
                     case SLComposeViewControllerResultCancelled:
                         output = @"Post Cancelled";
                         break;
                     case SLComposeViewControllerResultDone:
                         output = @"Post Successfully";
                         break;
                     default:
                         break;
                 }
                 
                 [APPDELEGATE showToastMessage:output];
             }];
        }
    }
    else if (self.btnSocialShare.tag==2)
    {
        if ([[GooglePlusUtility sharedObject]isLogin])
        {
            id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
            
            [shareBuilder setPrefillText:@"please check out this app."];
            
            [shareBuilder open];
        }
        else
        {
            [[GooglePlusUtility sharedObject]loginWithBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
                     
                     [shareBuilder setPrefillText:@"please check out this app."];
                     
                     [shareBuilder open];
                 }
             }];
        }
    }
    else if(self.btnSocialShare.tag==3)
    {
        NSString *url = @"https://api.linkedin.com/v1/people/~/shares";
        
        NSString *payload = @"please check out this app.";
                             
        if ([LISDKSessionManager hasValidSession])
        {
            [[LISDKAPIHelper sharedInstance] postRequest:url stringBody:payload
                success:^(LISDKAPIResponse *response)
            {
                
            }
                error:^(LISDKAPIError *apiError)
            {
                
            }];
        };
    }
}

- (IBAction)onClickBackBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickInviteSelected:(id)sender
{
    if(arrForNumber.count==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_CONTACT", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSString *strforNumber = @"";
        for (int i=0; i<arrForNumber.count; i++)
        {
            NSString *strTemp;
            if (i!=(arrForNumber.count-1))
            {
                strTemp=[NSString stringWithFormat:@"%@,",[arrForNumber objectAtIndex:i]];
            }
            else
            {
                strTemp=[NSString stringWithFormat:@"%@",[arrForNumber objectAtIndex:i]];
            }
            strforNumber = [strforNumber stringByAppendingString:strTemp];
        }

        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait while invitting your friends"];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strforNumber forKey:@"mobile_number"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_INVITE withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     {
                         if([[response valueForKey:@"success"]integerValue]==1)
                         {
                             [[AppDelegate sharedAppDelegate]showToastMessage:@"You have successfully invite your friends"];
                             [self.navigationController popToRootViewControllerAnimated:YES];
                         }
                         else
                         {
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             [alert show];
                         }
                     }
                 }
             }];
        }

    }
}
@end
