//
//  GeneralFeedbackVC.h
//  Wow Client
//
//  Created by My Mac on 10/31/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"

@interface GeneralFeedbackVC : BaseVC<UIGestureRecognizerDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UITextView *txtFeedback;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

- (IBAction)onClickNavigationBtn:(id)sender;
- (IBAction)onClickSubmit:(id)sender;

@end
