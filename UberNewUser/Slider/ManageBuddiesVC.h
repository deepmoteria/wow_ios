//
//  ManageBuddiesVC.h
//  Wow Client
//
//  Created by My Mac on 11/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ManageBuddiesVC : BaseVC <ABPeoplePickerNavigationControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewForManage;

@property (weak, nonatomic) IBOutlet UITableView *tableForContacts;

@property (weak, nonatomic) IBOutlet UIView *viewForFirstBuddy;
@property (weak, nonatomic) IBOutlet UIView *viewForExpandFirstBuddy;
@property (weak, nonatomic) IBOutlet UIView *viewForSecondBuddy;
@property (weak, nonatomic) IBOutlet UIView *viewForThirdBuddy;
@property (weak, nonatomic) IBOutlet UIView *viewForExpandSecondBuddy;
@property (weak, nonatomic) IBOutlet UIView *viewForExpandThirdBuddy;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstBuddy;
@property (weak, nonatomic) IBOutlet UITextField *txtSecondBuddy;
@property (weak, nonatomic) IBOutlet UITextField *txtThirdBuddy;

@property (weak, nonatomic) IBOutlet UISwitch *switchForFirstRide;
@property (weak, nonatomic) IBOutlet UISwitch *switchForFirstSos;
@property (weak, nonatomic) IBOutlet UISwitch *switchForSecondRide;
@property (weak, nonatomic) IBOutlet UISwitch *switchForSecondSos;
@property (weak, nonatomic) IBOutlet UISwitch *switchForThirdRide;
@property (weak, nonatomic) IBOutlet UISwitch *switchForThirdSos;

@property (weak, nonatomic) IBOutlet UILabel *lblFirstRide;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstSos;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondRide;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondSos;
@property (weak, nonatomic) IBOutlet UILabel *lblThirdRide;
@property (weak, nonatomic) IBOutlet UILabel *lblThirdSos;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnEditFirstBuddy;
@property (weak, nonatomic) IBOutlet UIButton *btnEditSecondBuddy;
@property (weak, nonatomic) IBOutlet UIButton *btnEditThirdBuddy;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFirst;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondBuddy;
@property (weak, nonatomic) IBOutlet UIButton *btnThirdBuddy;


- (IBAction)onClickSaveBtn:(id)sender;
- (IBAction)onClickClosebtn:(id)sender;
- (IBAction)onClickAddBuddy:(id)sender;
- (IBAction)onClickRideSwitch:(id)sender;
- (IBAction)onClickSos:(id)sender;
- (IBAction)onClickEditBuddy:(id)sender;
- (IBAction)onClickNavigationBtn:(id)sender;

@end