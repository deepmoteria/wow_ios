//
//  RateCardVC.m
//  Wow Client
//
//  Created by My Mac on 10/31/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "RateCardVC.h"
#import "Constants.h"
#import "UIImageView+Download.h"

@interface RateCardVC ()

@end

@implementation RateCardVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableDictionary *dictTypes = [[NSMutableDictionary alloc]init];
    for(int i=0;i<arrType.count;i++)
    {
        dictTypes=[arrType objectAtIndex:i];
        if(i==0)
        {
            [self.imgForCar downloadFromURL:[dictTypes valueForKey:@"icon"] withPlaceholder:nil];
            self.lblCarFare.text=[NSString stringWithFormat:@"%@ %@/- per km",[dictTypes valueForKey:@"currency"],[dictTypes valueForKey:@"base_price"]];
        }
        else
        {
            [self.imgForbike downloadFromURL:[dictTypes valueForKey:@"icon"] withPlaceholder:nil];
            self.lblBikeFare.text=[NSString stringWithFormat:@"%@ %@/- per km",[dictTypes valueForKey:@"currency"],[dictTypes valueForKey:@"base_price"]];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_RATE_CARD", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - action methods

- (IBAction)onClickNavigationBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
