#import "MyMilesVC.h"
#import "MilesCell.h"

@interface MyMilesVC ()

{
    CGRect MainFrame;
    NSMutableArray *arrOrganization,*arrForPercentage,*arrForId,*placeMarkMiles,*arrForSelectedOrga;
    UITextField *txtField;
    UILabel *label;
    int textTag,moreOrganization,isTextField,isCar;
    CGRect newFrameTextField,newFrameLabel,newframe;
    NSInteger intForTag,intForOrga;
    NSDictionary *milesPlacemark;
    NSString *goFrom_latitude,*goFrom_longitude,*goTo_latitude,*goTo_longitude,*commingFrom_latitude,*commingFrom_longitude,*commingTo_latitude,*commingTo_longitude,*sLocationGoFrom,*dLocationGoTo,*sLocationCommingFrom,*dLocationCommingTo;
    BOOL dragging;
    float oldX, oldY;
    NSString *strForHyperLink;
}

@end

@implementation MyMilesVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.viewForPicker setHidden:YES];
    [self.viewForOrganization setHidden:YES];
    [self.tableForCity setHidden:YES];
    
    arrForPercentage = [[NSMutableArray alloc]initWithObjects:@"10",@"20",@"30",@"40",@"50",@"60",@"70",@"80",@"90",@"100", nil];
    arrForId = [[NSMutableArray alloc]init];
    arrForSelectedOrga = [[NSMutableArray alloc]init];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureMiles:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.scrollViewMiles addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    arrOrganization = [[NSMutableArray alloc]init];
    moreOrganization=1;
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait while getting your miles"];
    [self getMyMiles];
}

#pragma mark - Memoery management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - ScroolViewDelegateMethod

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [txtField resignFirstResponder];
    [self.view endEditing:YES];
    [self.viewForPicker setHidden:YES];
    [self.tableForCity setHidden:YES];
    [self.viewForOrganization setHidden:YES];
}

#pragma mark - TapGesture

-(void)handleSingleTapGestureMiles:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [txtField resignFirstResponder];
    [self.view endEditing:YES];
    [self.viewForPicker setHidden:YES];
    [self.tableForCity setHidden:YES];
    [self.viewForOrganization setHidden:YES];
}

#pragma mark - Custom Methods

-(void)getMyMiles
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strForId = [pref valueForKey:PREF_USER_ID];
    NSString *strForToken = [pref valueForKey:PREF_USER_TOKEN];
    
    NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_PATTERNS,PARAM_ID,strForId,PARAM_TOKEN,strForToken];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getProviderDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if([[response valueForKey:@"success"]boolValue])
             {
                 self.txtGoFrom.text = [response valueForKey:@"pattern1_s_location"];
                 sLocationGoFrom = [response valueForKey:@"pattern1_s_location"];
                 self.txtGoTo.text = [response valueForKey:@"pattern1_d_location"];
                 dLocationGoTo = [response valueForKey:@"pattern1_d_location"];
                 self.txtCommingStartBtn.text = [response valueForKey:@"pattern1_start_time"];
                 self.txtCommingFrom.text = [response valueForKey:@"pattern2_s_location"];
                 sLocationCommingFrom = [response valueForKey:@"pattern2_s_location"];
                 self.txtCommingTo.text = [response valueForKey:@"pattern2_d_location"];
                 dLocationCommingTo = [response valueForKey:@"pattern2_d_location"];
                 self.txtGoStartBtn.text = [response valueForKey:@"pattern2_start_time"];
                 self.txtSelectPercentage.text = [response valueForKey:@"pledge_percentage"];
                 self.txtBikeNo.text = [response valueForKey:@"bike_number"];
                 self.txtCarNo.text = [response valueForKey:@"car_number"];
                 arrOrganization=[response valueForKey:@"organizations"];
                 
                 [[NSUserDefaults standardUserDefaults]setValue:sLocationGoFrom forKey:@"SourceLocationGo"];
                 [[NSUserDefaults standardUserDefaults]setValue:dLocationGoTo forKey:@"DestinationLocationGo"];
                 [[NSUserDefaults standardUserDefaults]setValue:sLocationCommingFrom forKey:@"SourceLocationFrom"];
                 [[NSUserDefaults standardUserDefaults]setValue:dLocationCommingTo forKey:@"DestinationLocationFrom"];
                 
                 NSLog(@"Number of organization = %lu",(unsigned long)arrOrganization.count);
                 NSMutableArray *arr = [[NSMutableArray alloc]init];
                 arr = [response valueForKey:@"organizations"];
                 for (int i=0; i<arr.count; i++)
                 {
                     if([[[arr objectAtIndex:i] valueForKey:@"organization_selected"]boolValue])
                         [arrForSelectedOrga addObject:[arr objectAtIndex:i]];
                 }
                 
                 [self setOrganizations:10:30];
                 [self.tableForOrgaList reloadData];
             }
         }
     }];
}

-(void)setOrganizations:(NSInteger)frameY :(NSInteger)frameH;
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    NSString *strType = [dictInfo valueForKey:@"type"];
    
    if([strForUserType isEqualToString:@"0"])
    {
        [self.viewForRideGiver setHidden:YES];
        [self.viewForRideTaker setHidden:NO];
        [self hideCarBike];
        newframe = self.lblPartOfOrga.frame;
        for(int i=0;i<arrOrganization.count;i++)
        {
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(newframe.origin.x, newframe.origin.y+newframe.size.height+frameY, self.viewForRideTaker.frame.size.width, frameH)];
            lbl.text = [[arrOrganization valueForKey:@"organization_name"] objectAtIndex:i];
            lbl.textColor = [UIColor darkGrayColor];
            lbl.font = [UberStyleGuide fontRegular:17.0f];
            newframe = lbl.frame;
            [self.viewForRideTaker addSubview:lbl];
            [self.viewForRideTaker setFrame:CGRectMake(self.viewForRideTaker.frame.origin.x, self.viewForRideTaker.frame.origin.y, self.viewForRideTaker.frame.size.width, self.viewForRideTaker.frame.size.height+lbl.frame.size.height)];
            [self.scrollViewMiles setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForRideTaker.frame.size.height+self.viewForRideTaker.frame.origin.y)];
        }
    }
    else
    {
        [self.viewForRideGiver setHidden:NO];
        [self.viewForRideTaker setHidden:YES];
        [self showCarBike:strType];
        newFrameTextField = self.txtOrganization.frame;
        if(isCar==1)
        {
            self.lblMySmiles.frame = CGRectMake(0, self.imgCarLine.frame.size.height+self.imgCarLine.frame.origin.y+frameY, self.lblMySmiles.frame.size.width, self.lblMySmiles.frame.size.height);
            self.lblCostContribution.frame = CGRectMake(0, self.lblMySmiles.frame.size.height+self.lblMySmiles.frame.origin.y+frameY, self.lblCostContribution.frame.size.width, self.lblCostContribution.frame.size.height);
            self.viewForRideGiver.frame = CGRectMake(self.imgBike.frame.origin.x, self.lblCostContribution.frame.size.height+self.lblCostContribution.frame.origin.y+frameY, self.viewForRideGiver.frame.size.width, self.viewForRideGiver.frame.size.height);
        }
        else
        {
            self.lblMySmiles.frame = CGRectMake(0, self.imgBikeLine.frame.size.height+self.imgBikeLine.frame.origin.y+frameY, self.lblMySmiles.frame.size.width, self.lblMySmiles.frame.size.height);
            self.lblCostContribution.frame = CGRectMake(0, self.lblMySmiles.frame.size.height+self.lblMySmiles.frame.origin.y+frameY, self.lblCostContribution.frame.size.width, self.lblCostContribution.frame.size.height);
            self.viewForRideGiver.frame = CGRectMake(self.imgBike.frame.origin.x, self.lblCostContribution.frame.size.height+self.lblCostContribution.frame.origin.y+frameY, self.viewForRideGiver.frame.size.width, self.viewForRideGiver.frame.size.height);
        }
        
        [self.scrollViewMiles setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForRideGiver.frame.size.height+self.viewForRideGiver.frame.origin.y-frameH)];
    }
}

-(void)setSelectedOrganizationListFrame
{
    self.tableForOrgaList.frame = CGRectMake(self.btnChoose.frame.origin.x+10, self.btnChoose.frame.origin.y+self.btnChoose.frame.size.height+10, self.txtOrganization.frame.size.width, (35.0f*([arrForSelectedOrga count])));
    
    [self.lblPercentageSign setFrame:CGRectMake(self.lblPercentageSign.frame.origin.x, self.tableForOrgaList.frame.origin.y+self.tableForOrgaList.frame.size.height+10, self.lblPercentageSign.frame.size.width, self.lblPercentageSign.frame.size.height)];
    
    [self.txtSelectPercentage setFrame:CGRectMake(self.txtSelectPercentage.frame.origin.x, self.lblPercentageSign.frame.origin.y+self.lblPercentageSign.frame.size.height+10, self.txtSelectPercentage.frame.size.width, self.txtSelectPercentage.frame.size.height)];
    
    [self.viewForRideGiver setFrame:CGRectMake(self.viewForRideGiver.frame.origin.x, self.viewForRideGiver.frame.origin.y, self.viewForRideGiver.frame.size.width, self.txtSelectPercentage.frame.size.height+self.txtSelectPercentage.frame.origin.y)];
    
    [self.scrollViewMiles setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForRideGiver.frame.size.height+self.viewForRideGiver.frame.origin.y+10)];
}

-(void)SetStartTime
{
    [self.datePicker setDatePickerMode:UIDatePickerModeTime];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [self.datePicker setLocale:locale];
}

#pragma mark - Hide Show Methods

-(void)showCarBike:(NSString *)str
{
    if([str isEqualToString:@"1"])
    {
        isCar=1;
        self.imgCar.hidden=NO;
        self.imgCarLine.hidden=NO;
        self.txtCarNo.hidden=NO;
        self.imgBike.hidden=YES;
        self.imgBikeLine.hidden=YES;
        self.txtBikeNo.hidden=YES;
    }
    else if ([str isEqualToString:@"2"])
    {
        isCar=2;
        self.imgBike.hidden=NO;
        self.imgBikeLine.hidden=NO;
        self.txtBikeNo.hidden=NO;
        self.imgCar.hidden=YES;
        self.imgCarLine.hidden=YES;
        self.txtCarNo.hidden=YES;
        self.imgBike.frame = self.imgCar.frame;
        self.imgBikeLine.frame = self.imgCarLine.frame;
        self.txtBikeNo.frame = self.txtCarNo.frame;
    }
    else
    {
        isCar=3;
        self.imgCar.hidden=NO;
        self.imgCarLine.hidden=NO;
        self.txtCarNo.hidden=NO;
        self.imgBike.hidden=NO;
        self.imgBikeLine.hidden=NO;
        self.txtBikeNo.hidden=NO;
    }
}

-(void)hideCarBike
{
    self.imgBike.hidden=YES;
    self.imgCar.hidden=YES;
    self.imgBikeLine.hidden=YES;
    self.imgCarLine.hidden=YES;
    self.txtBikeNo.hidden=YES;
    self.txtCarNo.hidden=YES;
}

#pragma mark - Tableview Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tableForCity)
        return placeMarkMiles.count;
    else if(tableView==self.tableForOrganization)
    {
        if(textTag==4)
            return arrForPercentage.count;
        else
            return arrOrganization.count;
    }
    else
        return arrForSelectedOrga.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableForCity)
    {
        return 45.0f;
    }
    else if(tableView==self.tableForOrganization)
    {
        return 42.0f;
    }
    else
    {
        return 35.0f;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableForCity)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if(cell == nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        cell.textLabel.font = [UberStyleGuide fontRegular:14];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode=NSLineBreakByWordWrapping;
        if (placeMarkMiles.count>0)
        {
            NSString *formatedAddress=[[placeMarkMiles objectAtIndex:indexPath.row] valueForKey:@"description"];
            cell.textLabel.text=formatedAddress;
        }
        return cell;
    }
    else if(tableView==self.tableForOrganization)
    {
        MilesCell *cell=(MilesCell *)[self.tableForOrganization dequeueReusableCellWithIdentifier:@"orgaIdentifier"];
        if (cell==nil)
        {
            cell=[[MilesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orgaIdentifier"];
        }
        if(textTag==4)
        {
            self.viewForOrganization.frame = CGRectMake(self.viewForOrganization.frame.origin.x, self.viewForOrganization.frame.origin.y, self.txtSelectPercentage.frame.size.width-10, (42.0f*([arrForPercentage count])));
             
            self.tableForOrganization.frame = CGRectMake(self.tableForOrganization.frame.origin.x, self.tableForOrganization.frame.origin.y, self.txtSelectPercentage.frame.size.width-10, (42.0f*([arrForPercentage count])));
            
            cell.lblOrganizationName.text = [arrForPercentage objectAtIndex:indexPath.row];
            [self.btnOk setHidden:YES];
            [cell.imgCheck setHidden:YES];
        }
        else if(textTag==3 || textTag==5)
        {
            self.viewForOrganization.frame = CGRectMake(self.viewForOrganization.frame.origin.x, self.viewForOrganization.frame.origin.y, self.txtOrganization.frame.size.width-10, (44.0f*([arrOrganization count]))+self.btnOk.frame.size.height+10);
            
            self.tableForOrganization.frame = CGRectMake(self.tableForOrganization.frame.origin.x, self.tableForOrganization.frame.origin.y, self.txtOrganization.frame.size.width-10, (44.0f*([arrOrganization count])));
            
            self.btnOk.frame = CGRectMake(self.btnOk.frame.origin.x,self.tableForOrganization.frame.size.height, self.tableForOrganization.frame.size.width, self.btnOk.frame.size.height);
            
            cell.lblOrganizationName.text = [[arrOrganization objectAtIndex:indexPath.row] valueForKey:@"organization_name"];
            [self.btnOk setHidden:NO];
            [cell.imgCheck setHidden:NO];
        }
        return cell;
    }
    else
    {
        MilesCell *cell=(MilesCell *)[self.tableForOrgaList dequeueReusableCellWithIdentifier:@"orgaListIdentifier"];
        if (cell==nil)
        {
            cell=[[MilesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"orgaListIdentifier"];
        }
        cell.btnForLink.titleLabel.textColor = [UIColor blueColor];
        cell.btnForLink.titleLabel.textAlignment = NSTextAlignmentCenter;
        cell.btnForLink.frame = CGRectMake(cell.btnForLink.frame.origin.x, cell.btnForLink.frame.origin.y, self.viewForRideGiver.frame.size.width, 30);
        cell.btnForLink.tag = indexPath.row;
        NSLog(@"%ld",(long)indexPath.row);
        [cell.btnForLink setTitle:[[arrForSelectedOrga objectAtIndex:indexPath.row] valueForKey:@"organization_name"] forState:UIControlStateNormal];
        [self setSelectedOrganizationListFrame];
        [cell.btnForLink addTarget:self action:@selector(openUrl:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableForCity)
    {
        milesPlacemark=[placeMarkMiles objectAtIndex:indexPath.row];
        self.tableForCity.hidden=YES;
        [self setNewPlaceDataMiles];
    }
    else if(tableView==self.tableForOrganization)
    {
        if(textTag==4)
        {
            self.txtSelectPercentage.text = [NSString stringWithFormat:@"  %@",[arrForPercentage objectAtIndex:indexPath.row]];
            [self.tableForOrganization setHidden:YES];
        }
        else if(textTag==3 || textTag==5)
        {
            MilesCell *cell = (MilesCell *)[self.tableForOrganization cellForRowAtIndexPath:indexPath];
            
            if([cell.imgCheck.image isEqual:[UIImage imageNamed:@"check_icon"]])
            {
                cell.imgCheck.image = [UIImage imageNamed:@"uncheck_icon"];
                [arrForSelectedOrga removeObject:[arrOrganization objectAtIndex:indexPath.row]];
            }
            else
            {
                cell.imgCheck.image = [UIImage imageNamed:@"check_icon"];
                [arrForSelectedOrga addObject:[arrOrganization objectAtIndex:indexPath.row]];
            }
        }
    }
}

#pragma mark - setPlaces

-(void)setNewPlaceDataMiles
{
    if (isTextField==1)
    {
        self.txtGoFrom.text = [NSString stringWithFormat:@"%@",[milesPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtGoFrom];
    }
    if (isTextField==2)
    {
        self.txtGoTo.text = [NSString stringWithFormat:@"%@",[milesPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtGoTo];
    }
    if (isTextField==3)
    {
        self.txtCommingFrom.text = [NSString stringWithFormat:@"%@",[milesPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtCommingFrom];
    }
    if (isTextField==4)
    {
        self.txtCommingTo.text = [NSString stringWithFormat:@"%@",[milesPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtCommingTo];
    }
}

#pragma mark - OpenWebView

- (void)openUrl:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSString *strForLink = [NSString stringWithFormat:@"http://%@",[[arrForSelectedOrga objectAtIndex:btn.tag]valueForKey:@"organization_web"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strForLink]];
}

#pragma mark - 
#pragma mark - textfield Delagate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==self.txtGoStartBtn)
    {
        self.txtGoStartBtn.text = @"";
        [self.view endEditing:YES];
        [self.viewForPicker setHidden:NO];
        [self.datePicker setHidden:NO];
        [self.viewForOrganization setHidden:YES];
        [self.tableForCity setHidden:YES];
        [self.datePicker setBackgroundColor:[UIColor grayColor]];
        [self SetStartTime];
        textTag=1;
        return NO;
    }
    else if(textField==self.txtCommingStartBtn)
    {
        self.txtCommingStartBtn.text = @"";
        [self.view endEditing:YES];
        [self.viewForPicker setHidden:NO];
        [self.datePicker setHidden:NO];
        [self.viewForOrganization setHidden:YES];
        [self.tableForCity setHidden:YES];
        [self.datePicker setBackgroundColor:[UIColor grayColor]];
        [self SetStartTime];
        textTag=2;
        return NO;
    }
    else if(textField==self.txtOrganization)
    {
        [self.view endEditing:YES];
        textTag=3;
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:NO];
        [self.tableForOrganization reloadData];
        intForOrga=1;
        return NO;
    }
    else if(textField==self.txtSelectPercentage)
    {
        [self.view endEditing:YES];
        textTag=4;
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:NO];
        [self.tableForOrganization reloadData];
        return NO;
    }
    else if (textField==self.txtGoFrom)
    {
        self.txtGoFrom.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else if (textField==self.txtGoTo)
    {
        self.txtGoTo.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else if (textField==self.txtCommingFrom)
    {
        self.txtCommingFrom.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else if (textField==self.txtCommingTo)
    {
        self.txtCommingTo.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else if (textField==self.txtBikeNo)
    {
        self.txtBikeNo.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else if (textField==self.txtCarNo)
    {
        self.txtCarNo.text = @"";
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        [self.viewForOrganization setHidden:YES];
        return YES;
    }
    else
    {
        [self.view endEditing:YES];
        textTag=5;
        [self.viewForPicker setHidden:YES];
        [self.viewForOrganization setHidden:NO];
        [self.tableForOrganization reloadData];
        intForTag=textField.tag;
        return YES;
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==self.txtGoFrom)
    {
        isTextField=1;
        CGRect frameRelativeTomainview = [self.txtGoFrom convertRect:self.txtGoFrom.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(self.tableForCity.frame.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(textField==self.txtGoTo)
    {
        isTextField=2;
        CGRect frameRelativeTomainview = [self.txtGoTo convertRect:self.txtGoTo.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(textField==self.txtCommingFrom)
    {
        isTextField=3;
        CGRect frameRelativeTomainview = [self.txtCommingFrom convertRect:self.txtCommingFrom.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if (textField==self.txtCommingTo)
    {
        isTextField=4;
        CGRect frameRelativeTomainview = [self.txtCommingTo convertRect:self.txtCommingTo.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-10,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height+5,frameRelativeTomainview.size.width+10,self.tableForCity.frame.size.height);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.viewForPicker.hidden=YES;
    if(textField==self.txtGoFrom || textField==self.txtGoTo || textField==self.txtCommingFrom || textField==self.txtCommingTo)
    {
        [self getLocationFromString:textField];
    }
    self.tableForCity.hidden=YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.tableForCity.hidden=YES;
    self.viewForPicker.hidden=YES;
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - Action methods

- (IBAction)onClickOk:(id)sender
{
    [self.viewForOrganization setHidden:YES];
    [self.tableForOrgaList reloadData];
    [self.tableForOrgaList setHidden:NO];
    [self setSelectedOrganizationListFrame];
}

- (IBAction)onClickMenu:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickCancelPicker:(id)sender
{
    if(textTag==1)
        self.txtGoStartBtn.text = @"";
    if(textTag==2)
        self.txtCommingStartBtn.text = @"";
    if(textTag==3)
    {
        self.txtOrganization.text = @"";
        [arrForId removeObject:[[arrOrganization valueForKey:@"id"] objectAtIndex:0]];
    }
    if(textTag==4)
        self.txtSelectPercentage.text = @"";
    if(textTag==5)
    {
        UITextField *txt= (UITextField *)[self.view viewWithTag:intForTag];
        txt.text=@"";
        [arrForId removeObject:[[arrOrganization valueForKey:@"id"] objectAtIndex:intForTag]];
    }
    
    [self.viewForPicker setHidden:YES];
}

- (IBAction)onClickDonePicker:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedDateString;
    if(textTag==1)
    {
        formattedDateString = [dateFormatter stringFromDate:self.datePicker.date];
        [self.txtGoStartBtn setText:formattedDateString];
    }
    if(textTag==2)
    {
        formattedDateString = [dateFormatter stringFromDate:self.datePicker.date];
        [self.txtCommingStartBtn setText:formattedDateString];
    }
    if(textTag==5)
    {
        UITextField *txt = (UITextField *)[self.view viewWithTag:intForTag];
        NSLog(@"%@",txt);
        [arrForId addObject:[[arrOrganization valueForKey:@"id"] objectAtIndex:intForTag]];
    }
    
    [self.viewForPicker setHidden:YES];
}

- (IBAction)onClickFeelGoodBtn:(id)sender
{
    if(self.txtGoFrom.text.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_GO_FROM", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtGoTo.text.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_GO_TO", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtCommingFrom.text.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_COMMING_FROM", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtCommingTo.text.length<1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_COMMING_TO", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSString *strforId = @"";
        for (int i=0; i<arrForId.count; i++)
        {
            NSString *strTemp;
            if (i!=(arrForId.count-1))
            {
                strTemp=[NSString stringWithFormat:@"%@,",[arrForId objectAtIndex:i]];
            }
            else
            {
                strTemp=[NSString stringWithFormat:@"%@",[arrForId objectAtIndex:i]];
            }
            strforId = [strforId stringByAppendingString:strTemp];
        }
        
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait"];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strforId forKey:@"pledge_organization"];
            [dictParam setValue:self.txtSelectPercentage.text forKey:@"pledge_percentage"];
            [dictParam setValue:self.txtCarNo.text forKey:@"car_number"];
            [dictParam setValue:self.txtBikeNo.text forKey:@"bike_number"];
            
            [dictParam setValue:goFrom_latitude forKey:@"pattern1_source_lat"];
            [dictParam setValue:goFrom_longitude forKey:@"pattern1_source_long"];
            [dictParam setValue:sLocationGoFrom forKey:@"pattern1_s_location"];
            [dictParam setValue:goTo_latitude forKey:@"pattern1_dest_lat"];
            [dictParam setValue:goTo_longitude forKey:@"pattern1_dest_long"];
            [dictParam setValue:dLocationGoTo forKey:@"pattern1_d_location"];
            [dictParam setValue:self.txtGoStartBtn.text forKey:@"pattern1_start_time"];
            
            [dictParam setValue:commingFrom_latitude forKey:@"pattern2_source_lat"];
            [dictParam setValue:commingFrom_longitude forKey:@"pattern2_source_long"];
            [dictParam setValue:sLocationCommingFrom forKey:@"pattern2_s_location"];
            [dictParam setValue:commingTo_latitude forKey:@"pattern2_dest_lat"];
            [dictParam setValue:commingTo_longitude forKey:@"pattern2_dest_long"];
            [dictParam setValue:dLocationCommingTo forKey:@"pattern2_d_location"];
            [dictParam setValue:self.txtCommingStartBtn.text forKey:@"pattern2_start_time"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getProviderDataFromPath:FILE_SET_PATTERNS withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     {
                         if([[response valueForKey:@"success"]integerValue]==1)
                         {
                             [[AppDelegate sharedAppDelegate]showToastMessage:@"You have successfully added your patterns"];
                             [[NSUserDefaults standardUserDefaults]setValue:sLocationGoFrom forKey:@"SourceLocationGo"];
                             [[NSUserDefaults standardUserDefaults]setValue:dLocationGoTo forKey:@"DestinationLocationGo"];
                             [[NSUserDefaults standardUserDefaults]setValue:sLocationCommingFrom forKey:@"SourceLocationFrom"];
                             [[NSUserDefaults standardUserDefaults]setValue:dLocationCommingTo forKey:@"DestinationLocationFrom"];
                             [self.navigationController popToRootViewControllerAnimated:YES];
                         }
                         else
                         {
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             [alert show];
                         }
                     }
                 }
             }];
        }
    }
}

#pragma mark - 
#pragma mark - Searching Method

- (IBAction)searchingCity:(id)sender
{
    milesPlacemark=nil;
    [placeMarkMiles removeAllObjects];
    self.tableForCity.hidden=YES;
    NSString *str;
    if (isTextField==1)
    {
        str=self.txtGoFrom.text;
        CGRect frameRelativeTomainview = [self.txtGoFrom convertRect:self.txtGoFrom.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(isTextField==2)
    {
        str=self.txtGoTo.text;
        CGRect frameRelativeTomainview = [self.txtGoTo convertRect:self.txtGoTo.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(isTextField==3)
    {
        str=self.txtCommingFrom.text;
        CGRect frameRelativeTomainview = [self.txtCommingFrom convertRect:self.txtCommingFrom.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }
    else if(isTextField==4)
    {
        str=self.txtCommingTo.text;
        CGRect frameRelativeTomainview = [self.txtCommingTo convertRect:self.txtCommingTo.bounds toView:self.view];
        self.tableForCity.frame=CGRectMake(frameRelativeTomainview.origin.x-15,frameRelativeTomainview.origin.y+frameRelativeTomainview.size.height+5,frameRelativeTomainview.size.width,self.tableForCity.frame.size.height);
    }

    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isSource"];
             if ([arrAddress count] > 0)
             {
                 self.tableForCity.hidden=NO;
                 placeMarkMiles=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkMiles addObject:Placemark];
                 [self.tableForCity reloadData];
                 if(arrAddress.count==0)
                 {
                     self.tableForCity.hidden=YES;
                 }
             }
             
         }
         
     }];
}

-(void)getLocationFromString:(UITextField *)textField
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:textField.text forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             if ([arrAddress count] > 0)
             {
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 
                 if (textField==self.txtGoFrom)
                 {
                     goFrom_latitude=[dictLocation valueForKey:@"lat"];
                     goFrom_longitude=[dictLocation valueForKey:@"lng"];
                     sLocationGoFrom = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 }
                 else if(textField==self.txtGoTo)
                 {
                     goTo_latitude=[dictLocation valueForKey:@"lat"];
                     goTo_longitude=[dictLocation valueForKey:@"lng"];
                     dLocationGoTo = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 }
                 if (textField==self.txtCommingFrom)
                 {
                     commingFrom_latitude=[dictLocation valueForKey:@"lat"];
                     commingFrom_longitude=[dictLocation valueForKey:@"lng"];
                     sLocationCommingFrom = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 }
                 else if(textField==self.txtCommingTo)
                 {
                     commingTo_latitude=[dictLocation valueForKey:@"lat"];
                     commingTo_longitude=[dictLocation valueForKey:@"lng"];
                     dLocationCommingTo = [[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 }
            }
         }
     }];
}

@end