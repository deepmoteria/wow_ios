//
//  InviteFriendsCell.h
//  Wow Client
//
//  Created by My Mac on 12/19/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgeCheck;

@end
