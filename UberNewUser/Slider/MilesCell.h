//
//  MilesCell.h
//  Wow Client
//
//  Created by My Mac on 1/19/16.
//  Copyright (c) 2016 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblOrganizationName;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnForLink;

@end
