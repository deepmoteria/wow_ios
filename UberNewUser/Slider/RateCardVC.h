//
//  RateCardVC.h
//  Wow Client
//
//  Created by My Mac on 10/31/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface RateCardVC : BaseVC

@property (weak, nonatomic) IBOutlet UIImageView *imgForCar;
@property (weak, nonatomic) IBOutlet UIImageView *imgForbike;

@property (weak, nonatomic) IBOutlet UILabel *lblCarFare;
@property (weak, nonatomic) IBOutlet UILabel *lblCarPerKm;
@property (weak, nonatomic) IBOutlet UILabel *lblBikeFare;
@property (weak, nonatomic) IBOutlet UILabel *lblBikePerKm;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

- (IBAction)onClickNavigationBtn:(id)sender;

@end
