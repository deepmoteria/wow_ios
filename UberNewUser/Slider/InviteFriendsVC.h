//
//  InviteFriendsVC.h
//  Wow Client
//
//  Created by My Mac on 12/19/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "BaseVC.h"
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <linkedin-sdk/LISDK.h>

@interface InviteFriendsVC : BaseVC <ABPeoplePickerNavigationControllerDelegate,FBSDKSharingDelegate,MFMailComposeViewControllerDelegate>
{
    SLComposeViewController *fb,*mySLComposerSheet;
    GPPSignIn *signIn;
}

@property (weak, nonatomic) IBOutlet UITableView *tableForContacts;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIButton *btnSocialShare;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteSelected;


- (IBAction)onClickConatact:(id)sender;
- (IBAction)onClickSocialBtnPressed:(id)sender;
- (IBAction)onClickBackBtn:(id)sender;
- (IBAction)onClickInviteSelected:(id)sender;

@end