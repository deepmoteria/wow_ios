//
//  ManageBuddiesVC.m
//  Wow Client
//
//  Created by My Mac on 11/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.

#import "ManageBuddiesVC.h"

@interface ManageBuddiesVC ()
{
    NSMutableArray *arrForContacts,*arrForAlert,*arrForSos;
    NSArray *sectionTitleArray;
    NSString *strForUserId,*strForUserToken,*strForFirstNumber,*strForSecondNumber,*strForThirdNumber,*strForFirstName,*strForSecondName,*strForThirdName;
    NSString *strForFirstRide,*strForSecondRide,*strForThirdRide,*strForFirstSos,*strForSecondSos,*strForThirdSos;
    int flagForTxt;
    NSString *strFinalName,*strFinalNumber,*strFinalAlert,*strFinalSos;
}

@end

@implementation ManageBuddiesVC

@synthesize lblFirstRide,lblFirstSos,lblSecondRide,lblSecondSos,lblThirdRide,lblThirdSos;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.txtFirstBuddy.tag=1;
    self.txtSecondBuddy.tag=2;
    self.txtThirdBuddy.tag=3;
    
    arrForContacts = [[NSMutableArray alloc]init];
    arrForAlert = [[NSMutableArray alloc]init];
    arrForSos = [[NSMutableArray alloc]init];
    
    [self hideTableView];
    [self setupBuddies];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getContacts];
    [self getBuddies];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark - setups methods

-(void)setupBuddies
{
    self.viewForExpandFirstBuddy.hidden=YES;
    self.viewForExpandSecondBuddy.hidden=YES;
    self.viewForExpandThirdBuddy.hidden=YES;
    
    strForFirstRide=@"0";
    strForSecondRide=@"0";
    strForThirdRide=@"0";
    strForFirstSos=@"0";
    strForSecondSos=@"0";
    strForThirdSos=@"0";
    
    self.switchForFirstRide.on=NO;
    self.switchForSecondRide.on=NO;
    self.switchForThirdRide.on=NO;
    self.switchForFirstSos.on=NO;
    self.switchForSecondSos.on=NO;
    self.switchForThirdSos.on=NO;
    
    lblFirstRide.text=@"No";
    lblSecondRide.text=@"No";
    lblThirdRide.text=@"No";
    lblFirstSos.text=@"No";
    lblSecondSos.text=@"No";
    lblThirdSos.text=@"No";
}

-(void)hideTableView
{
    [self.tableForContacts setHidden:YES];
    [self.btnClose setHidden:YES];
    [self.btnSave setHidden:NO];
}

-(void)showTableView
{
    [self.tableForContacts setHidden:NO];
    [self.btnClose setHidden:NO];
    [self.btnSave setHidden:YES];
}

#pragma mark - 
#pragma mark - buddies methods

-(void)getBuddies
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait while getting buddies"];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GET_BUDDIES withParamData:dictParam withBlock:^(id response, NSError *error)
        {
            if(response)
            {
                response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                {
                    if([[response valueForKey:@"success"]integerValue]==1)
                    {
                        [self manageBuddies:response];
                    }
                    else
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }
        }];
    }
}

-(void)getContacts
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    __block BOOL accessGranted = NO;
    if (addressBook != NULL)
    {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
        {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else
        accessGranted = YES;
    
    if (accessGranted)
    {
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        
        //CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonPhoneProperty);
        
        //CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        
        CFIndex nPeople =(CFIndex) ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonPhoneProperty);
        
        for (int i=0; i<(CFIndex)allPeople;i++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            ABRecordRef person1 = CFArrayGetValueAtIndex((CFArrayRef)nPeople, i);
            
            if(person==person1)
            {
                ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
                
                NSString *firstName = [NSString stringWithFormat:@"%@",ABRecordCopyValue(person, kABPersonFirstNameProperty)];
                
                NSString *lastName = [NSString stringWithFormat:@"%@",ABRecordCopyValue(person, kABPersonLastNameProperty)];
                
                NSString *phoneNumber = (__bridge NSString *) ABMultiValueCopyValueAtIndex(phones, 0);
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                
                if(![firstName isEqualToString:@"(null)"] || [firstName isKindOfClass:[[NSNull null] class]])
                    [dict setValue:firstName forKey:@"firstname"];
                if(![lastName isEqualToString:@"(null)"] || [lastName isKindOfClass:[[NSNull null] class]])
                    [dict setValue:lastName forKey:@"lastname"];
                if(![phoneNumber isEqualToString:@"(null)"] || [phoneNumber isKindOfClass:[[NSNull null] class]])
                    [dict setValue:phoneNumber forKey:@"number"];
                
                [arrForContacts addObject:dict];
                [self.tableForContacts reloadData];
            }
            else
            {
                break;
            }
        }
    }
    else
    {
        #ifdef DEBUG
        NSLog(@"Cannot fetch Contacts");
        #endif
    }

}

-(void)manageBuddies:(NSDictionary *)dictBuddies
{
    NSLog(@"%@",dictBuddies);
    NSArray *arrBuddies = [dictBuddies valueForKey:@"mobile_number"];
    int conut = (int)arrBuddies.count;
    
    if(conut==3)
    {
        if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:0] isEqual:@"1"])
        {
            self.switchForFirstRide.on=YES;
            lblFirstRide.text=@"Yes";
            strForFirstRide=@"1";
        }
        if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:1] isEqual:@"1"])
        {
            self.switchForSecondRide.on=YES;
            lblSecondRide.text=@"Yes";
            strForSecondRide=@"1";
        }
        if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:2] isEqual:@"1"])
        {
            self.switchForThirdRide.on=YES;
            lblThirdRide.text=@"Yes";
            strForThirdRide=@"1";
        }
        if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:0] isEqual:@"1"])
        {
            self.switchForFirstSos.on=YES;
            lblFirstSos.text=@"Yes";
            strForFirstSos=@"1";
        }
        if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:1] isEqual:@"1"])
        {
            self.switchForSecondSos.on=YES;
            lblSecondSos.text=@"Yes";
            strForSecondSos=@"1";
        }
        if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:2] isEqual:@"1"])
        {
            self.switchForThirdSos.on=YES;
            lblThirdSos.text=@"Yes";
            strForThirdSos=@"1";
        }
        
        self.txtFirstBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:0];
        self.txtSecondBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:1];
        self.txtThirdBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:2];
        
        strForFirstName=[[arrBuddies valueForKey:@"name"] objectAtIndex:0];
        strForFirstNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:0];
        strForSecondName=[[arrBuddies valueForKey:@"name"] objectAtIndex:1];
        strForSecondNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:1];
        strForThirdName=[[arrBuddies valueForKey:@"name"] objectAtIndex:2];
        strForThirdNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:2];
        
        [self.btnAddFirst setSelected:YES];
        [self.btnSecondBuddy setSelected:YES];
        [self.btnThirdBuddy setSelected:YES];
        [self.viewForExpandFirstBuddy setHidden:NO];
        [self.viewForExpandSecondBuddy setHidden:NO];
        [self.viewForExpandThirdBuddy setHidden:NO];
        self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
        self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
        self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.size.height+self.viewForExpandSecondBuddy.frame.origin.y, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
        self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
    }
    else
    {
        if(conut==2)
        {
            if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:0] isEqual:@"1"])
            {
                self.switchForFirstRide.on=YES;
                lblFirstRide.text=@"Yes";
                strForFirstRide=@"1";
            }
            if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:1] isEqual:@"1"])
            {
                self.switchForSecondRide.on=YES;
                lblSecondRide.text=@"Yes";
                strForSecondRide=@"1";
            }
            if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:0] isEqual:@"1"])
            {
                self.switchForFirstSos.on=YES;
                lblFirstSos.text=@"Yes";
                strForFirstSos=@"1";
            }
            if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:1] isEqual:@"1"])
            {
                self.switchForSecondSos.on=YES;
                lblSecondSos.text=@"Yes";
                strForSecondSos=@"1";
            }
            
            self.txtFirstBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:0];
            self.txtSecondBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:1];
            
            strForFirstName=[[arrBuddies valueForKey:@"name"] objectAtIndex:0];
            strForFirstNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:0];
            strForSecondName=[[arrBuddies valueForKey:@"name"] objectAtIndex:1];
            strForSecondNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:1];
            
            [self.btnAddFirst setSelected:YES];
            [self.btnSecondBuddy setSelected:YES];
            [self.viewForExpandFirstBuddy setHidden:NO];
            [self.viewForExpandSecondBuddy setHidden:NO];
            self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
            self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.origin.y+self.viewForSecondBuddy.frame.size.height, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
            self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.size.height+self.viewForExpandSecondBuddy.frame.origin.y, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
            self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
        }
        else
        {
            if(conut==1)
            {
                if([[[arrBuddies valueForKey:@"alert_everytime"] objectAtIndex:0] isEqual:@"1"])
                {
                    self.switchForFirstRide.on=YES;
                    lblFirstRide.text=@"Yes";
                    strForFirstRide=@"1";
                }
                if([[[arrBuddies valueForKey:@"alert_sos"] objectAtIndex:0] isEqual:@"1"])
                {
                    self.switchForFirstSos.on=YES;
                    lblFirstSos.text=@"Yes";
                    strForFirstSos=@"1";
                }
                
                self.txtFirstBuddy.text = [[arrBuddies valueForKey:@"name"] objectAtIndex:0];
                
                strForFirstName=[[arrBuddies valueForKey:@"name"] objectAtIndex:0];
                strForFirstNumber=[[arrBuddies valueForKey:@"number"] objectAtIndex:0];
                
                [self.btnAddFirst setSelected:YES];
                [self.viewForExpandFirstBuddy setHidden:NO];
                self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height+self.viewForSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
            }
        }
    }
    if(self.viewForExpandThirdBuddy.hidden==NO)
    {
        [self.scrollViewForManage setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForExpandThirdBuddy.frame.size.height+self.viewForExpandThirdBuddy.frame.origin.y)];
    }
    else
    {
        [self.scrollViewForManage setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height)];
    }
}

-(BOOL)checkNumberOrString:(NSString *)str
{
    if([str isEqualToString:@""])
    {
        return NO;
    }
    else
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
        NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
        return [str isEqualToString:filtered];
    }
}

-(void)setInputValue
{
    if(self.txtFirstBuddy.text.length>0 && ![self checkNumberOrString:self.txtFirstBuddy.text])
    {
        strFinalName = [NSString stringWithFormat:@"%@",strForFirstName];
        strFinalNumber = [NSString stringWithFormat:@"%@",strForFirstNumber];
        strFinalAlert = [NSString stringWithFormat:@"%@",strForFirstRide];
        strFinalSos = [NSString stringWithFormat:@"%@",strForFirstSos];
        if(self.txtSecondBuddy.text.length>0 && ![self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@,%@",strForFirstName,strForSecondName];
            strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForFirstNumber,strForSecondNumber];
            strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",strForFirstName,strForSecondName,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",strForFirstNumber,strForSecondNumber,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",strForFirstName,strForSecondName,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",strForFirstNumber,strForSecondNumber,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
        }
        else if (self.txtSecondBuddy.text.length>0 && [self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@,%@",strForFirstName,self.txtSecondBuddy.text];
            strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForFirstNumber,self.txtSecondBuddy.text];
            strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",strForFirstName,self.txtSecondBuddy.text,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",strForFirstNumber,self.txtSecondBuddy.text,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",strForFirstName,self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",strForFirstNumber,self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
        }
        else
        {
            if(self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",strForFirstName,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForFirstNumber,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForThirdSos];
            }
            else
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",strForFirstName,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForFirstNumber,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForThirdSos];
            }
        }
    }
    else if (self.txtFirstBuddy.text.length>0 && [self checkNumberOrString:self.txtFirstBuddy.text])
    {
        strFinalName = [NSString stringWithFormat:@"%@",self.txtFirstBuddy.text];
        strFinalNumber = [NSString stringWithFormat:@"%@",self.txtFirstBuddy.text];
        strFinalAlert = [NSString stringWithFormat:@"%@",strForFirstRide];
        strFinalSos = [NSString stringWithFormat:@"%@",strForFirstSos];
        if(self.txtSecondBuddy.text.length>0 && ![self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,strForSecondName];
            strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,strForSecondNumber];
            strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,strForSecondName,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,strForSecondNumber,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,strForSecondName,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,strForSecondNumber,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
        }
        else if (self.txtSecondBuddy.text.length>0 && [self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text];
            strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text];
            strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@,%@",self.txtFirstBuddy.text,self.txtSecondBuddy.text,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@,%@",strForFirstRide,strForThirdRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@,%@",strForFirstSos,strForThirdSos,strForThirdSos];
            }
        }
        else
        {
            if(self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForThirdSos];
            }
            else if(self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtFirstBuddy.text,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForFirstRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForFirstSos,strForThirdSos];
            }
        }
    }
    else
    {
        if(self.txtSecondBuddy.text.length>0 && ![self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@",strForSecondName];
            strFinalNumber = [NSString stringWithFormat:@"%@",strForSecondNumber];
            strFinalAlert = [NSString stringWithFormat:@"%@",strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@",strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",strForSecondName,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForSecondNumber,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",strForSecondName,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",strForSecondNumber,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForSecondSos,strForThirdSos];
            }
        }
        else if (self.txtSecondBuddy.text.length>0 && [self checkNumberOrString:self.txtSecondBuddy.text])
        {
            strFinalName = [NSString stringWithFormat:@"%@",self.txtSecondBuddy.text];
            strFinalNumber = [NSString stringWithFormat:@"%@",self.txtSecondBuddy.text];
            strFinalAlert = [NSString stringWithFormat:@"%@",strForSecondRide];
            strFinalSos = [NSString stringWithFormat:@"%@",strForSecondSos];
            if(self.txtThirdBuddy.text.length>0 && [self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtSecondBuddy.text,self.txtThirdBuddy.text];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForSecondRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForSecondSos,strForThirdSos];
            }
            else if (self.txtThirdBuddy.text.length>0 && ![self checkNumberOrString:self.txtThirdBuddy.text])
            {
                strFinalName = [NSString stringWithFormat:@"%@,%@",self.txtSecondBuddy.text,strForThirdName];
                strFinalNumber = [NSString stringWithFormat:@"%@,%@",self.txtSecondBuddy.text,strForThirdNumber];
                strFinalAlert = [NSString stringWithFormat:@"%@,%@",strForThirdRide,strForThirdRide];
                strFinalSos = [NSString stringWithFormat:@"%@,%@",strForThirdSos,strForThirdSos];
            }
        }
    }
    
    [self saveBuddies];
}

-(void)saveBuddies
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Please wait while saving your buddies"];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        strForUserType = [pref objectForKey:PREF_USER_TYPE];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:@"user_id"];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strForUserType forKey:PARAM_RIDE];
        [dictParam setValue:strFinalName forKey:@"name"];
        [dictParam setValue:strFinalNumber forKey:@"mobile_number"];
        [dictParam setValue:strFinalAlert forKey:@"alert_everytime"];
        [dictParam setValue:strFinalSos forKey:@"alert_sos"];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_MANAGE_BUDDIES withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject] dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 {
                     if([[response valueForKey:@"success"]integerValue]==1)
                     {
                         [[AppDelegate sharedAppDelegate]showToastMessage:@"You have successfully saved your buddies"];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
             }
         }];
    }

}

#pragma mark-
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForContacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    cell = [self.tableForContacts dequeueReusableCellWithIdentifier:@"eventCell"];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"eventCell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableDictionary *pastDict=[arrForContacts objectAtIndex:indexPath.row];
    NSString *strFirstName = [pastDict valueForKey:@"firstname"];
    NSString *strLastName = [pastDict valueForKey:@"lastname"];
    
    if(strFirstName)
        cell.textLabel.text=[NSString stringWithFormat:@"%@",[pastDict valueForKey:@"firstname"]];
    
    if(strLastName)
        cell.textLabel.text=[NSString stringWithFormat:@"%@ %@",[pastDict valueForKey:@"firstname"],[pastDict valueForKey:@"lastname"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrForContacts objectAtIndex:indexPath.row];
    NSString *strFirstName = [dict valueForKey:@"firstname"];
    NSString *strLastName = [dict valueForKey:@"lastname"];
    NSString *strNumber = [dict valueForKey:@"number"];
    
    if(flagForTxt==1)
    {
        if(strFirstName)
        {
            self.txtFirstBuddy.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"firstname"]];
            strForFirstName = [NSString stringWithFormat:@"%@",strFirstName];
        }
        if(strLastName)
        {
            self.txtFirstBuddy.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"firstname"],[dict valueForKey:@"lastname"]];
            strForFirstName = [NSString stringWithFormat:@"%@ %@",strFirstName,strLastName];
        }
        if(strNumber)
            strForFirstNumber = [NSString stringWithFormat:@"%@",strNumber];
        
        if([strForFirstRide isEqualToString:@""])
            strForFirstRide=@"0";
        if([strForFirstSos isEqualToString:@""])
            strForFirstSos=@"0";
    }
    else if (flagForTxt==2)
    {
        if(strFirstName)
        {
            self.txtSecondBuddy.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"firstname"]];
            strForSecondName = [NSString stringWithFormat:@"%@",strFirstName];
        }
        if(strLastName)
        {
            self.txtSecondBuddy.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"firstname"],[dict valueForKey:@"lastname"]];
            strForSecondName = [NSString stringWithFormat:@"%@ %@",strFirstName,strLastName];
        }
        if(strNumber)
            strForSecondNumber = [NSString stringWithFormat:@"%@",strNumber];
        
        if([strForSecondRide isEqualToString:@""])
            strForSecondRide=@"0";
        if([strForSecondSos isEqualToString:@""])
            strForSecondSos=@"0";
    }
    else if (flagForTxt==3)
    {
        if(strFirstName)
        {
            self.txtThirdBuddy.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"firstname"],[dict valueForKey:@"lastname"]];
            strForThirdName = [NSString stringWithFormat:@"%@",strFirstName];
        }
        if(strLastName)
        {
            self.txtThirdBuddy.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"firstname"],[dict valueForKey:@"lastname"]];
            strForThirdName = [NSString stringWithFormat:@"%@ %@",strFirstName,strLastName];
        }
        if(strNumber)
            strForThirdNumber = [NSString stringWithFormat:@"%@",strNumber];
        
        if([strForThirdRide isEqualToString:@""])
            strForThirdRide=@"0";
        if([strForThirdSos isEqualToString:@""])
            strForThirdSos=@"0";
    }
    
    [self hideTableView];
}

#pragma mark
#pragma mark - UItextField Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(![string isEqualToString:@""])
    {
        if(textField.text.length>9)
        {
            return 0;
        }
    }
    if(textField==self.txtFirstBuddy || textField==self.txtSecondBuddy || textField==self.txtThirdBuddy)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag==1)
    {
        if(![self checkNumberOrString:self.txtFirstBuddy.text])
            self.txtFirstBuddy.text = @"";
        if(self.btnAddFirst.isSelected==YES)
            return YES;
        else
            return NO;
    }
    else if(textField.tag==2)
    {
        if(![self checkNumberOrString:self.txtSecondBuddy.text])
            self.txtSecondBuddy.text = @"";
        if(self.btnSecondBuddy.isSelected==YES)
            return YES;
        else
            return NO;
    }
    else if(textField.tag==3)
    {
        if(![self checkNumberOrString:self.txtThirdBuddy.text])
            self.txtThirdBuddy.text = @"";
        if(self.btnThirdBuddy.isSelected==YES)
            return YES;
        else
            return NO;
    }
    else
    {
        return NO;
    }
}

#pragma mark
#pragma mark - Action Methods

- (IBAction)onClickAddBuddy:(id)sender
{
    [self.view endEditing:YES];
    if(sender==self.btnAddFirst)
    {
        if(self.btnAddFirst.isSelected==YES)
        {
            if(self.btnSecondBuddy.isSelected==NO)
            {
                if(self.btnThirdBuddy.isSelected==NO)
                {
                    strForFirstName=@"";
                    strForFirstNumber=@"";
                    strForFirstRide=@"";
                    strForFirstSos=@"";
                    self.txtFirstBuddy.text = @"";
                    self.switchForFirstRide.on=NO;
                    self.switchForFirstSos.on=NO;
                    self.btnAddFirst.selected=NO;
                    self.viewForExpandFirstBuddy.hidden=YES;
                    self.viewForFirstBuddy.hidden=NO;
                    self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForFirstBuddy.frame.size.height+self.viewForSecondBuddy.frame.size.height-29.0f, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                    self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForFirstBuddy.frame.size.height+self.viewForSecondBuddy.frame.size.height+self.viewForThirdBuddy.frame.size.height-29.0f, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                }
                else
                {
                    strForFirstName=@"";
                    strForFirstNumber=@"";
                    strForFirstRide=@"";
                    strForFirstSos=@"";
                    self.txtFirstBuddy.text = @"";
                    self.switchForFirstRide.on=NO;
                    self.switchForFirstSos.on=NO;
                    self.btnAddFirst.selected=NO;
                    self.viewForExpandFirstBuddy.hidden=YES;
                    self.viewForFirstBuddy.hidden=NO;
                    self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForFirstBuddy.frame.size.height+self.viewForSecondBuddy.frame.size.height-29.0f, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                    self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForSecondBuddy.frame.origin.y+self.viewForSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                    self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
                }
            }
            else
            {
                strForFirstName=@"";
                strForFirstNumber=@"";
                strForFirstRide=@"";
                strForFirstSos=@"";
                self.txtFirstBuddy.text = @"";
                self.switchForFirstRide.on=NO;
                self.switchForFirstSos.on=NO;
                self.btnAddFirst.selected=NO;
                self.viewForExpandFirstBuddy.hidden=YES;
                self.viewForFirstBuddy.hidden=NO;
                self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForFirstBuddy.frame.size.height+self.viewForFirstBuddy.frame.origin.y, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.origin.y+self.viewForExpandSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
        }
        else if(self.btnAddFirst.isSelected==NO)
        {
            if(self.btnSecondBuddy.isSelected==NO)
            {
                if(self.btnThirdBuddy.isSelected==NO)
                {
                    self.btnAddFirst.selected=YES;
                    self.viewForExpandFirstBuddy.hidden=NO;
                    self.viewForFirstBuddy.hidden=NO;
                    self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                    self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height+self.viewForSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                }
                else
                {
                    self.btnAddFirst.selected=YES;
                    self.viewForExpandFirstBuddy.hidden=NO;
                    self.viewForFirstBuddy.hidden=NO;
                    self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.size.height+self.viewForExpandFirstBuddy.frame.origin.y, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                    self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                    self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
                }
            }
            else
            {
                self.btnAddFirst.selected=YES;
                self.viewForExpandFirstBuddy.hidden=NO;
                self.viewForFirstBuddy.hidden=NO;
                self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.size.height+self.viewForExpandSecondBuddy.frame.origin.y, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
        }
    }
    else if (sender==self.btnSecondBuddy)
    {
        if(self.btnSecondBuddy.isSelected==YES)
        {
            if(self.btnAddFirst.isSelected==NO)
            {
                strForSecondName=@"";
                strForSecondNumber=@"";
                strForSecondRide=@"";
                strForSecondSos=@"";
                self.txtSecondBuddy.text = @"";
                self.switchForFirstRide.on=NO;
                self.switchForFirstSos.on=NO;
                self.viewForExpandSecondBuddy.hidden=YES;
                self.btnSecondBuddy.selected=NO;
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y,self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
            else
            {
                strForSecondName=@"";
                strForSecondNumber=@"";
                strForSecondRide=@"";
                strForSecondSos=@"";
                self.txtSecondBuddy.text = @"";
                self.switchForSecondRide.on=NO;
                self.switchForSecondSos.on=NO;
                self.viewForExpandSecondBuddy.hidden=YES;
                self.btnSecondBuddy.selected=NO;
                self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForSecondBuddy.frame.origin.y+self.viewForSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
        }
        else if(self.btnSecondBuddy.isSelected==NO)
        {
            if(self.btnAddFirst.isSelected==NO)
            {
                self.btnSecondBuddy.selected=YES;
                self.viewForExpandSecondBuddy.hidden=NO;
                self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.size.height+self.viewForSecondBuddy.frame.origin.y, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.origin.y+self.viewForExpandSecondBuddy.frame.size.height, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
            else
            {
                self.btnSecondBuddy.selected=YES;
                self.viewForExpandSecondBuddy.hidden=NO;
                self.viewForSecondBuddy.frame = CGRectMake(0,self.viewForExpandFirstBuddy.frame.origin.y+self.viewForExpandFirstBuddy.frame.size.height, self.viewForSecondBuddy.frame.size.width, self.viewForSecondBuddy.frame.size.height);
                self.viewForExpandSecondBuddy.frame = CGRectMake(0, self.viewForSecondBuddy.frame.origin.y+self.viewForSecondBuddy.frame.size.height, self.viewForExpandSecondBuddy.frame.size.width, self.viewForExpandSecondBuddy.frame.size.height);
                self.viewForThirdBuddy.frame = CGRectMake(0,self.viewForExpandSecondBuddy.frame.size.height+self.viewForExpandSecondBuddy.frame.origin.y, self.viewForThirdBuddy.frame.size.width, self.viewForThirdBuddy.frame.size.height);
                self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
            }
        }
    }
    else if (sender==self.btnThirdBuddy)
    {
        if(self.btnThirdBuddy.isSelected==YES)
        {
            strForThirdName=@"";
            strForThirdNumber=@"";
            strForThirdRide=@"";
            strForThirdSos=@"";
            self.txtThirdBuddy.text = @"";
            self.switchForThirdRide.on=NO;
            self.switchForThirdSos.on=NO;
            self.btnThirdBuddy.selected=NO;
            self.viewForExpandThirdBuddy.hidden=YES;
        }
        else
        {
            self.btnThirdBuddy.selected=YES;
            self.viewForExpandThirdBuddy.hidden=NO;
            self.viewForExpandThirdBuddy.frame = CGRectMake(0, self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height,self.viewForExpandThirdBuddy.frame.size.width, self.viewForExpandThirdBuddy.frame.size.height);
        }
    }
    if(self.viewForExpandThirdBuddy.hidden==NO)
    {
        [self.scrollViewForManage setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForExpandThirdBuddy.frame.size.height+self.viewForExpandThirdBuddy.frame.origin.y)];
    }
    else
    {
        [self.scrollViewForManage setContentSize:CGSizeMake(self.view.frame.size.width,self.viewForThirdBuddy.frame.origin.y+self.viewForThirdBuddy.frame.size.height)];
    }
}

- (IBAction)onClickEditBuddy:(id)sender
{
    [self.view endEditing:YES];
    if(sender==self.btnEditFirstBuddy)
    {
        flagForTxt=1;
        if(self.viewForExpandFirstBuddy.hidden==YES)
            [self onClickAddBuddy:self.btnAddFirst];
        [self showTableView];
    }
    if(sender==self.btnEditSecondBuddy)
    {
        flagForTxt=2;
        if(self.viewForExpandSecondBuddy.hidden==YES)
            [self onClickAddBuddy:self.btnSecondBuddy];
        [self showTableView];
    }
    if(sender==self.btnEditThirdBuddy)
    {
        flagForTxt=3;
        if(self.viewForExpandThirdBuddy.hidden==YES)
            [self onClickAddBuddy:self.btnThirdBuddy];
        [self showTableView];
    }
}

- (IBAction)onClickNavigationBtn:(id)sender
{
    if(self.tableForContacts.hidden==YES)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self hideTableView];
    }
}
- (IBAction)onClickSaveBtn:(id)sender
{
    if([self.txtFirstBuddy.text isEqual:@""] && [self.txtSecondBuddy.text isEqual:@""] && [self.txtThirdBuddy.text isEqual:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_BUDDY", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if([self checkNumberOrString:self.txtFirstBuddy.text] || [self checkNumberOrString:self.txtSecondBuddy.text] || [self checkNumberOrString:self.txtThirdBuddy.text])
    {
        if(self.txtFirstBuddy.text.length!=10 && [self checkNumberOrString:self.txtFirstBuddy.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_10_digit", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (self.txtSecondBuddy.text.length!=10 && [self checkNumberOrString:self.txtSecondBuddy.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_10_digit", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (self.txtThirdBuddy.text.length!=10 && [self checkNumberOrString:self.txtThirdBuddy.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_10_digit", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            [self setInputValue];
        }
    }
    else
    {
        [self setInputValue];
    }
}

- (IBAction)onClickClosebtn:(id)sender
{
    [self hideTableView];
}

- (IBAction)onClickRideSwitch:(id)sender
{
    if(sender==self.switchForFirstRide)
    {
        if([self.switchForFirstRide isOn]==YES)
        {
            lblFirstRide.text = @"Yes";
            strForFirstRide=@"1";
        }
        else
        {
            lblFirstRide.text = @"No";
            strForFirstRide=@"0";
        }
    }
    if(sender==self.switchForSecondRide)
    {
        if([self.switchForSecondRide isOn]==YES)
        {
            lblSecondRide.text = @"Yes";
            strForSecondRide=@"1";
        }
        else
        {
            lblSecondRide.text = @"No";
            strForSecondRide=@"0";
        }
    }
    if(sender==self.switchForThirdRide)
    {
        if([self.switchForThirdRide isOn]==YES)
        {
            lblThirdRide.text = @"Yes";
            strForThirdRide=@"1";
        }
        else
        {
            lblThirdRide.text = @"No";
            strForThirdRide=@"0";
        }
    }
}

- (IBAction)onClickSos:(id)sender
{
    if(sender==self.switchForFirstSos)
    {
        if([self.switchForFirstSos isOn]==YES)
        {
            lblFirstSos.text = @"Yes";
            strForFirstSos=@"1";
        }
        else
        {
            lblFirstSos.text = @"No";
            strForFirstSos=@"0";
        }
    }
    if(sender==self.switchForSecondSos)
    {
        if([self.switchForSecondSos isOn]==YES)
        {
            lblSecondSos.text = @"Yes";
            strForSecondSos=@"1";
        }
        else
        {
            lblSecondSos.text = @"No";
            strForSecondSos=@"0";
        }
    }
    if(sender==self.switchForThirdSos)
    {
        if([self.switchForThirdSos isOn]==YES)
        {
            lblThirdSos.text = @"Yes";
            strForThirdSos=@"1";
        }
        else
        {
            lblThirdSos.text = @"No";
            strForThirdSos=@"0";
        }
    }

}

@end