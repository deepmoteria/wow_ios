//
//  ProfileVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ProfileVC.h"
#import "UIImageView+Download.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVBase.h>
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "UIView+Utils.h"
#import "UberStyleGuide.h"

@interface ProfileVC ()
{
    NSString *strForUserId,*strForUserToken,*strForType,*strOldOfficialEmail;
    NSMutableArray *arrForType;
    NSMutableDictionary *dictOld,*dictNew;
    BOOL isOtp,isProfileUpdate;
}

@end

@implementation ProfileVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForType = [[NSMutableArray alloc]init];
    dictOld = [[NSMutableDictionary alloc]init];
    dictNew = [[NSMutableDictionary alloc]init];
    isOtp=0;
    isProfileUpdate=0;
    [self SetLocalization];
    [self setDataForUserInfo];
    [self.proPicImgv applyRoundedCornersFull];
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureProfile:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.scroolView addGestureRecognizer:singleTapGestureRecognizer];
    [self.lblOtp setHidden:YES];
    [self.txtOtp setHidden:YES];
    [self.lineForOtp setHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.scroolView setContentSize:CGSizeMake(self.scroolView.frame.size.width,self.txtPhone.frame.origin.y+self.txtPhone.frame.size.height+5.0f)];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btnNavigation setTitle:NSLocalizedString(@"NAV_PROFILE", nil) forState:UIControlStateNormal];
}

#pragma mark - tap gesture

-(void)handleSingleTapGestureProfile:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtFullName resignFirstResponder];
    [self.txtEmail resignFirstResponder];
    [self.txtOfficailMail resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    CGPoint offset;
    offset=CGPointMake(0,0);
    [self.scroolView setContentOffset:offset animated:YES];
}

#pragma mark- Custom methods

-(void)customFont
{
    self.txtFullName.font=[UberStyleGuide fontSemiBold:18.0f];
    self.txtPhone.font=[UberStyleGuide fontRegular];
    self.txtEmail.font=[UberStyleGuide fontRegular];
    self.btnUpdate.titleLabel.font=[UberStyleGuide fontRegularBold];
}

-(void)showServive
{
    self.lblLikeToGiveRide.hidden=NO;
    self.btnCar.hidden=NO;
    self.btnBike.hidden=NO;
    self.lblCar.hidden=NO;
    self.lblBike.hidden=NO;
}

-(void)hideServive
{
    self.lblLikeToGiveRide.hidden=YES;
    self.btnCar.hidden=YES;
    self.btnBike.hidden=YES;
    self.lblCar.hidden=YES;
    self.lblBike.hidden=YES;
}

-(void)setFrameForOtp
{
    isOtp=1;
    [self.lblOtp setHidden:NO];
    [self.txtOtp setHidden:NO];
    [self.lineForOtp setHidden:NO];
    
    self.lblOfficeId.frame = CGRectMake(self.lblOfficeId.frame.origin.x, self.lblOfficeId.frame.origin.y+60,self.lblOfficeId.frame.size.width, self.lblOfficeId.frame.size.height);
    
    self.txtOfficailMail.frame = CGRectMake(self.txtOfficailMail.frame.origin.x, self.txtOfficailMail.frame.origin.y+60,self.txtOfficailMail.frame.size.width, self.txtOfficailMail.frame.size.height);
    
    self.lineForOfficeId.frame = CGRectMake(self.lineForOfficeId.frame.origin.x, self.lineForOfficeId.frame.origin.y+60,self.lineForOfficeId.frame.size.width, self.lineForOfficeId.frame.size.height);
    
    self.lblILikeTo.frame = CGRectMake(self.lblILikeTo.frame.origin.x, self.lblILikeTo.frame.origin.y+60,self.lblILikeTo.frame.size.width, self.lblILikeTo.frame.size.height);
    
    self.lblRideGiver.frame = CGRectMake(self.lblRideGiver.frame.origin.x, self.lblRideGiver.frame.origin.y+60,self.lblRideGiver.frame.size.width, self.lblRideGiver.frame.size.height);
    
    self.lblRideTaker.frame = CGRectMake(self.lblRideTaker.frame.origin.x, self.lblRideTaker.frame.origin.y+60,self.lblRideTaker.frame.size.width, self.lblRideTaker.frame.size.height);
    
    self.lblRideTakerGiver.frame = CGRectMake(self.lblRideTakerGiver.frame.origin.x, self.lblRideTakerGiver.frame.origin.y+60,self.lblRideTakerGiver.frame.size.width, self.lblRideTakerGiver.frame.size.height);
    
    self.btnGiveRide.frame = CGRectMake(self.btnGiveRide.frame.origin.x, self.btnGiveRide.frame.origin.y+60,self.btnGiveRide.frame.size.width, self.btnGiveRide.frame.size.height);
    
    self.btnTakeRide.frame = CGRectMake(self.btnTakeRide.frame.origin.x, self.btnTakeRide.frame.origin.y+60,self.btnTakeRide.frame.size.width, self.btnTakeRide.frame.size.height);
    
    self.btnDoBoth.frame = CGRectMake(self.btnDoBoth.frame.origin.x, self.btnDoBoth.frame.origin.y+60,self.btnDoBoth.frame.size.width, self.btnDoBoth.frame.size.height);
    
    self.lblLikeToGiveRide.frame = CGRectMake(self.lblLikeToGiveRide.frame.origin.x, self.lblLikeToGiveRide.frame.origin.y+60,self.lblLikeToGiveRide.frame.size.width, self.lblLikeToGiveRide.frame.size.height);
    
    self.btnCar.frame = CGRectMake(self.btnCar.frame.origin.x, self.btnCar.frame.origin.y+60,self.btnCar.frame.size.width, self.btnCar.frame.size.height);
    
    self.lblCar.frame = CGRectMake(self.lblCar.frame.origin.x, self.lblCar.frame.origin.y+60,self.lblCar.frame.size.width, self.lblCar.frame.size.height);
    
    self.lblBike.frame = CGRectMake(self.lblBike.frame.origin.x, self.lblBike.frame.origin.y+60,self.lblBike.frame.size.width, self.lblBike.frame.size.height);
    
    self.btnBike.frame = CGRectMake(self.btnBike.frame.origin.x, self.btnBike.frame.origin.y+60,self.btnBike.frame.size.width, self.btnBike.frame.size.height);
    
    [self.scroolView setContentSize:CGSizeMake(self.view.frame.size.width, self.btnBike.frame.size.height+self.btnBike.frame.origin.y+20)];
}

-(void)getOtp
{
    [APPDELEGATE showLoadingWithTitle:@"Please Wait while getting Otp"];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:self.txtPhone.text forKey:@"mobile_number"];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_OTP withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Get otp= %@",response);
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}

-(void)SetLocalization
{
    self.txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    self.txtPhone.placeholder=NSLocalizedString(@"PHONE", nil);
    [self.btnUpdate setTitle:NSLocalizedString(@"UPDATE PROFILE", nil) forState:UIControlStateNormal];
}

-(void)setDataForUserInfo
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    dictOld = [pref objectForKey:PREF_LOGIN_OBJECT];
    dictNew = [pref objectForKey:PREF_LOGIN_OBJECT];
    strForUserType=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"user_type"]];
    [self.proPicImgv downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    self.txtFullName.text = [NSString stringWithFormat:@"%@ %@",[dictInfo valueForKey:@"first_name"],[dictInfo valueForKey:@"last_name"]];
    self.txtEmail.text=[dictInfo valueForKey:@"personal_email"];
    self.txtPhone.text=[dictInfo valueForKey:@"mobile"];
    self.txtOfficailMail.text=[dictInfo valueForKey:@"official_email"];
    strOldOfficialEmail = [dictInfo valueForKey:@"official_email"];
    self.lblRating.text=[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"rating"]];
    if([strForUserType isEqualToString:@"0"])
    {
        [self.btnTakeRide setSelected:YES];
        self.lblRideTaker.textColor = [UIColor blackColor];
        [self hideServive];
    }
    else if([strForUserType isEqualToString:@"1"])
    {
        [self.btnGiveRide setSelected:YES];
        self.lblRideGiver.textColor = [UIColor blackColor];
    }
    else if([strForUserType isEqualToString:@"2"])
    {
        [self.btnDoBoth setSelected:YES];
        self.lblRideTakerGiver.textColor = [UIColor blackColor];
    }
    if([[dictInfo valueForKey:@"type"]isEqualToString:@"1"])
    {
        [self.btnCar setSelected:YES];
        self.lblCar.textColor = [UIColor blackColor];
        strForType=@"1";
    }
    else if ([[dictInfo valueForKey:@"type"]isEqualToString:@"2"])
    {
        [self.btnBike setSelected:YES];
        self.lblBike.textColor = [UIColor blackColor];
        strForType=@"2";
    }
    else
    {
        [self.btnCar setSelected:YES];
        [self.btnBike setSelected:YES];
        self.lblCar.textColor = [UIColor blackColor];
        self.lblBike.textColor = [UIColor blackColor];
        strForType=@"1,2";
    }
}

-(void)updateProfile
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if([[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"EDITING", nil)];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId = [pref objectForKey:PREF_USER_ID];
            strForUserToken = [pref objectForKey:PREF_USER_TOKEN];
            NSArray *arrForName = [self.txtFullName.text componentsSeparatedByString:@" "];
            NSString *strForNumber = [NSString stringWithFormat:@"%@",self.txtPhone.text];
            
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:[arrForName objectAtIndex:0] forKey:PARAM_FIRST_NAME];
            [dictParam setValue:[arrForName objectAtIndex:1] forKey:PARAM_LAST_NAME];
            [dictParam setValue:self.txtOfficailMail.text forKey:PARAM_OFFCIAL_EMAIL];
            [dictParam setValue:strForUserType forKey:PARAM_RIDE];
            [dictParam setValue:strForNumber forKey:PARAM_PHONE];
            [dictParam setValue:strForType forKey:PARAM_TYPE];
            if(isOtp==1)
                [dictParam setValue:self.txtOtp.text forKey:PARAM_OTP];
            
            UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.proPicImgv.image];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_UPADTE withParamDataImage:dictParam andImage:imgUpload withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"] boolValue])
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                         dictNew = [pref objectForKey:PREF_LOGIN_OBJECT];
                         [pref setValue:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                         strForUserType=[response valueForKey:@"user_type"];
                         [pref synchronize];
                         [self setDataForUserInfo];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"PROFILE_EDIT_SUCESS", nil)];
                         self.btnUpdate.hidden=YES;
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 
                 NSLog(@"PROFILE --> %@",response);
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1000)
    {
        if (buttonIndex == 0)
        {
            [self updateProfile];
        }
        else
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark-
#pragma mark - UIButton Action

- (IBAction)onClickRideChange:(id)sender
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *str = [pref valueForKey:PREF_REQ_ID];
    
    if(str==nil || [str integerValue]==-1)
    {
        self.btnGiveRide.userInteractionEnabled=YES;
        self.btnTakeRide.userInteractionEnabled=YES;
        self.btnDoBoth.userInteractionEnabled=YES;
        
        UIButton *btn=(UIButton *)sender;
        if (btn==self.btnTakeRide)
        {
            self.btnGiveRide.selected=NO;
            self.btnTakeRide.selected=YES;
            self.btnDoBoth.selected=NO;
            self.lblRideGiver.textColor = [UIColor lightGrayColor];
            self.lblRideTaker.textColor = [UIColor blackColor];
            self.lblRideTakerGiver.textColor = [UIColor lightGrayColor];
            strForUserType=@"0";
            strForType=@"";
            [self hideServive];
            NSLog(@"str for type = %@",strForUserType);
        }
        else if(btn==self.btnGiveRide)
        {
            self.btnGiveRide.selected=YES;
            self.btnTakeRide.selected=NO;
            self.btnDoBoth.selected=NO;
            self.lblRideGiver.textColor = [UIColor blackColor];
            self.lblRideTaker.textColor = [UIColor lightGrayColor];
            self.lblRideTakerGiver.textColor = [UIColor lightGrayColor];
            strForUserType=@"1";
            NSLog(@"str for type = %@",strForUserType);
            [self showServive];
        }
        else
        {
            self.btnGiveRide.selected=NO;
            self.btnTakeRide.selected=NO;
            self.btnDoBoth.selected=YES;
            self.lblRideGiver.textColor = [UIColor lightGrayColor];
            self.lblRideTaker.textColor = [UIColor lightGrayColor];
            self.lblRideTakerGiver.textColor = [UIColor blackColor];
            strForUserType=@"2";
            NSLog(@"str for type = %@",strForUserType);
            [self showServive];
        }
    }
}

- (IBAction)onClickServiceTypeBtn:(id)sender
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *str = [pref valueForKey:PREF_REQ_ID];
    if(str==nil || [str integerValue]==-1)
    {
        self.btnCar.userInteractionEnabled=YES;
        self.btnBike.userInteractionEnabled=YES;
        
        UIButton *btn=(UIButton *)sender;
        if (btn==self.btnCar)
        {
            if(self.btnCar.selected==YES)
            {
                self.btnCar.selected=NO;
                self.lblCar.textColor=[UIColor lightGrayColor];
                if(self.btnBike.selected==YES)
                    strForType=@"2";
                else
                    strForType=@"";
                
                NSLog(@"arr for type = %@",strForType);
            }
            else
            {
                self.btnCar.selected=YES;
                self.lblCar.textColor=[UIColor darkGrayColor];
                if(self.btnBike.selected==YES)
                    strForType = [NSString stringWithFormat:@"1,%@",strForType];
                else
                    strForType=@"1";
                
                NSLog(@"arr for type = %@",strForType);
            }
        }
        else if (btn==self.btnBike)
        {
            if(self.btnBike.selected==YES)
            {
                self.btnBike.selected=NO;
                self.lblBike.textColor=[UIColor lightGrayColor];
                if(self.btnCar.selected==YES)
                    strForType=@"1";
                else
                    strForType=@"";
                
                NSLog(@"arr for type = %@",strForType);
            }
            else
            {
                self.btnBike.selected=YES;
                self.lblBike.textColor=[UIColor darkGrayColor];
                if(self.btnCar.selected==YES)
                    strForType = [NSString stringWithFormat:@"%@,2",strForType];
                else
                    strForType=@"2";
                
                NSLog(@"arr for type = %@",strForType);
            }
        }
    }
}

- (IBAction)selectPhotoBtnPressed:(id)sender
{
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    UIActionSheet *actionpass;
    
    actionpass = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"SELECT_PHOTO", @""),NSLocalizedString(@"TAKE_PHOTO", @""),nil];
    actionpass.delegate=self;
    [actionpass showInView:window];
}

- (IBAction)onClickNavigationBtm:(id)sender
{
    if(isOtp==1)
    {
        if(self.txtOtp.text.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_OTP", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"SAVE_CHANGES", nil) delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"Cancel", nil];
            alert.tag=1000;
            [alert show];
        }
    }
    else if(isProfileUpdate==1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"SAVE_CHANGES", nil) delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"Cancel", nil];
        alert.tag=1000;
        [alert show];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)updateBtnPressed:(id)sender
{
    if (self.txtFullName.text.length < 1 || self.txtOfficailMail.text.length < 1 || self.txtPhone.text.length < 1)
    {
        if(self.txtFullName.text.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_FIRST_NAME", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (self.txtOfficailMail.text.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtOfficailMail.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if (self.txtPhone.text.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_NUMBER", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if(strForType.length<1)
    {
        if(self.btnTakeRide.selected==YES)
        {
            [self updateProfile];
        }
        else
        {
            if(self.btnCar.selected==YES || self.btnBike.selected==YES)
            {
                [self updateProfile];
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_TYPE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    else if (isProfileUpdate==1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"SAVE_CHANGES", nil) delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"Cancel", nil];
        alert.tag=1000;
        [alert show];
    }
    else if (isOtp==1)
    {
        if(self.txtOtp.text.length<1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_OTP", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        [self updateProfile];
    }
}

#pragma mark
#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 1:
        {
            [self takePhoto];
        }
            break;
        case 0:
        {
            [self selectPhotos];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark
#pragma mark - Action to Share


- (void)selectPhotos
{
    // Set up the image picker controller and add it to the view
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

-(void)takePhoto
{
    // Set up the image picker controller and add it to the view
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
    imagePickerController.allowsEditing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

#pragma mark
#pragma mark - ImagePickerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if([info valueForKey:UIImagePickerControllerEditedImage]==nil)
    {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:[info valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc(rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//
            UIImage *img=[UIImage imageWithData:data];
            [self setImage:img];
        } failureBlock:^(NSError *err) {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
    else
    {
        [self setImage:[info valueForKey:UIImagePickerControllerEditedImage]];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)setImage:(UIImage *)image
{
    self.proPicImgv.image=image;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark - UItextField Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    isProfileUpdate=1;
    if(textField==self.txtPhone)
    {
        if(isOtp==0)
            [self setFrameForOtp];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self.txtFullName)
    {
        offset=CGPointMake(0, 0);
        [self.scroolView setContentOffset:offset animated:YES];
    }
    else if(textField==self.txtPhone)
    {
        offset=CGPointMake(0, 40);
        [self.scroolView setContentOffset:offset animated:YES];
    }
    else if(textField==self.txtOfficailMail)
    {
        if(isOtp==1)
        {
            offset=CGPointMake(0, 155);
            [self.scroolView setContentOffset:offset animated:YES];
        }
        else
        {
            offset=CGPointMake(0, 95);
            [self.scroolView setContentOffset:offset animated:YES];
        }
    }
    else if(textField==self.txtOtp)
    {
        offset=CGPointMake(0, 115);
        [self.scroolView setContentOffset:offset animated:YES];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.txtFullName)
    {
        [self.txtPhone becomeFirstResponder];
    }
    else if (textField==self.txtPhone)
    {
        if(isOtp==1)
            [self getOtp];
    }
    else if (textField==self.txtOtp)
    {
        [self.txtOfficailMail becomeFirstResponder];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.scroolView setContentOffset:offset animated:YES];
        
        } completion:^(BOOL finished)
        {
        }];
    }
    [textField resignFirstResponder];
    return YES;
}

@end