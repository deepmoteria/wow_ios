//
//  ProfileVC.h
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface ProfileVC : BaseVC <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scroolView;

@property (weak, nonatomic) IBOutlet UITextField *txtOtp;
@property (weak, nonatomic) IBOutlet UITextField *txtOfficailMail;
@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;

@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UILabel *lblRideGiver;
@property (weak, nonatomic) IBOutlet UILabel *lblRideTaker;
@property (weak, nonatomic) IBOutlet UILabel *lblRideTakerGiver;
@property (weak, nonatomic) IBOutlet UILabel *lblOfficeId;
@property (weak, nonatomic) IBOutlet UILabel *lblCar;
@property (weak, nonatomic) IBOutlet UILabel *lblBike;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeToGiveRide;
@property (weak, nonatomic) IBOutlet UILabel *lblOtp;
@property (weak, nonatomic) IBOutlet UILabel *lblILikeTo;

@property (weak, nonatomic) IBOutlet UIImageView *lineForOtp;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv;
@property (weak, nonatomic) IBOutlet UIImageView *lineForOfficeId;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnProPic;
@property (weak, nonatomic) IBOutlet UIButton *btnCar;
@property (weak, nonatomic) IBOutlet UIButton *btnBike;
@property (weak, nonatomic) IBOutlet UIButton *btnTakeRide;
@property (weak, nonatomic) IBOutlet UIButton *btnGiveRide;
@property (weak, nonatomic) IBOutlet UIButton *btnDoBoth;


- (IBAction)onClickNavigationBtm:(id)sender;
- (IBAction)selectPhotoBtnPressed:(id)sender;
- (IBAction)updateBtnPressed:(id)sender;
- (IBAction)onClickRideChange:(id)sender;
- (IBAction)onClickServiceTypeBtn:(id)sender;

@end